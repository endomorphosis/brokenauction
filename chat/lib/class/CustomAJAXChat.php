<?php
/*
 * @package AJAX_Chat
 * @author Sebastian Tschan
 * @copyright (c) Sebastian Tschan
 * @license GNU Affero General Public License
 * @link https://blueimp.net/ajax/
 */

class CustomAJAXChat extends AJAXChat {

/*
   function __construct($init_parameter) {
//      $this->setRequestVar('channelID',$init_parameter);
//      $this->setRequestVar('channelName',$init_parameter);

    //    $this->some_parameter = $init_parameter;
    }
*/

// Returns an associative array containing userName, userID and userRole
	// Returns null if login is invalid
	function getValidLoginUserData()
    {
    		// debug message:
//		$text = 'Channel:'.$_SESSION['_chanID'];
//		$text = 'Channel:'.$this->_requestVars['channelID'];


		$customUsers = $this->getCustomUsers();
		if($this->getRequestVar('password'))
      {
			// Check if we have a valid registered user:
			$userName = $this->getRequestVar('userName');
//			$userName = $this->convertEncoding($userName, $this->getConfig('contentEncoding'), $this->getConfig('sourceEncoding'));
			$password = $this->getRequestVar('password');
//			$password = $this->convertEncoding($password, $this->getConfig('contentEncoding'), $this->getConfig('sourceEncoding'));
      include($_SERVER['DOCUMENT_ROOT'].'/admin/mc_db/db_config.inc.php');
			$db_conn = mysql_connect(DB_SERVER, DB_USER, DB_PASS);
      $db_result = mysql_selectdb(DEFAULT_DB,$db_conn);
//if(U_ID!='') {
//  		$userQuery="select * from `auction_users` where user_id='".U_ID."' and active='1' limit 1 ";
//}
//else
{
  		$userQuery="select * from `auction_users` where username='".$userName."' and password='".$password."' and active='1' limit 1 ";
}
//      $r1=mysql_query($userQuery,$link);
      $r1=mysql_query($userQuery);


if (!isset($_uid)) $_uid='';
$myFile = "erro_dump.txt";
$fh = fopen($myFile, 'w') or die("can't open file");
$stringData = 'Query: '.$userQuery."\n";
//fwrite($fh, $stringData);
//$stringData = 'Results: '.$results."\n";
fwrite($fh, $stringData);
$stringData = 'UID: '.$_uid."\n";
fwrite($fh, $stringData);
fclose($fh);
if(mysql_num_rows($r1)!=1)
        {
    		$userQuery="select * from `auction_users` where user_id='".$_SESSION['m_info']['user_id']."' and active='1' limit 1 ";
        $r1=mysql_query($userQuery);
        }
      if(mysql_num_rows($r1)==1)
        {
        $results=mysql_fetch_assoc($r1);
        if($results!=false)
          {
  				$userData = array();
  				$userData['userID'] = $results['user_id']; //$key;
  				$userData['userName'] = $results['username']; //$this->trimUserName($value['userName']);
  				$userData['userRole'] = 'AJAX_CHAT_USER'; //$value['userRole'];
  				return $userData;
          }
  			}
        else
        {
  			// Guest users:
//  			return $this->getGuestUser();
    		}  
			} 
			return null;
		} //end of function


//	}

//		$rarr=$cms->getresults($userQuery);
/*
			foreach($customUsers as $key=>$value) {
				if(($value['userName'] == $userName) && ($value['password'] == $password)) {
					$userData = array();
					$userData['userID'] = $key;
					$userData['userName'] = $this->trimUserName($value['userName']);
					$userData['userRole'] = $value['userRole'];
					return $userData;
*/
//				}
//---end of function----------------------------------------------------------------------------------------------------
                                                                                                                                                                                                                                                                                   

	// Store the channels the current user has access to
	// Make sure channel names don't contain any whitespace
	function &getChannels() {
		if($this->_channels === null) {
			$this->_channels = array();
			
			$customUsers = $this->getCustomUsers();
			
			// Get the channels, the user has access to:
//			if($this->getUserRole() == AJAX_CHAT_GUEST) {
//				$validChannels = $customUsers[0]['channels'];
//			} else {
				$validChannels = $customUsers[$this->getUserID()]['channels'];
//			}
			
			// Add the valid channels to the channel list (the defaultChannelID is always valid):
			foreach($this->getAllChannels() as $key=>$value) {
				// Check if we have to limit the available channels:
				if($this->getConfig('limitChannelList') && !in_array($value, $this->getConfig('limitChannelList'))) {
					continue;
				}
				if(in_array($value, $validChannels) || $value == $this->getConfig('defaultChannelID')) {
					$this->_channels[$key] = $value;
				}
			}
			//$this->_channels = array_merge($this->_channels, array('chan'=>1, 'chan'=>3));
		}
		return $this->_channels;
	}

	// Store all existing channels
	// Make sure channel names don't contain any whitespace
	function &getAllChannels() {
		if($this->_allChannels === null) {
			// Get all existing channels:
			$customChannels = $this->getCustomChannels();
			
			$defaultChannelFound = false;
			
			foreach($customChannels as $key=>$value) {
				$forumName = $this->trimChannelName($value);
				
				$this->_allChannels[$forumName] = $key;
				
				if($key == $this->getConfig('defaultChannelID')) {
					$defaultChannelFound = true;
				}
			}
			
			if(!$defaultChannelFound) {
				// Add the default channel as first array element to the channel list:
				$this->_allChannels = array_merge(
					array(
						$this->trimChannelName($this->getConfig('defaultChannelName'))=>$this->getConfig('defaultChannelID')
					), 
					$this->_allChannels
				);
			}
			//$this->_allChannels = array_merge($this->_allChannels, array('chan'=>1, 'chan'=>3));
		}
		return $this->_allChannels;
	}

	function &getCustomUsers() {
		// List containing the registered chat users:
		$users = null;

$users = array();

// Default guest user (don't delete this one):
$users[0] = array();
$users[0]['userRole'] = AJAX_CHAT_GUEST;
$users[0]['userName'] = null;
$users[0]['password'] = null;
$users[0]['channels'] = array();

/*
$cntr=1;
while (  $_auth_info = mysql_fetch_assoc($result_q1) )
  {
  $users[$cntr] = array();
  $users[$cntr]['userName'] = $_auth_info['username'];
  $users[$cntr]['password'] = $_auth_info['password'];
  if ($_auth_info['access_lvl']=='1337')
    {
    $users[$cntr]['userRole'] = AJAX_CHAT_ADMIN;
    $users[$cntr]['channels'] = array(0,1,2);
    }
  if ($_auth_info['access_lvl']=='1')
    {
    $users[$cntr]['userRole'] = AJAX_CHAT_USER;
    $users[$cntr]['channels'] = array(0);
    }
  if ($_auth_info['access_lvl']=='10')
    {
    $users[$cntr]['userRole'] = AJAX_CHAT_USER;
    $users[$cntr]['channels'] = array(0,1,2);
    }
  if ($_auth_info['access_lvl']=='5')
    {
    $users[$cntr]['userRole'] = AJAX_CHAT_USER;
    $users[$cntr]['channels'] = array(0,2);
    }
  
  $cntr++;
  }
*/
		
		require(AJAX_CHAT_PATH.'lib/data/users.php');
		return $users;
	}
	
	function &getCustomChannels() {
		// List containing the custom channels:
		$channels = null;
		require(AJAX_CHAT_PATH.'lib/data/channels.php');
		return $channels;
	}

}
?>