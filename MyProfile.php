<?
 include('header.php');
  $pathIMG='userImages';
  $User=$cms->getMemberDetails($cms->loggedUserId);
  $officeHours=$cms->getresults("select * from office_hours where	user_id='".$cms->loggedUserId."'");
  $imgsrc=($User['image']!="")? $pathIMG.'/'.$User['image']:'images/profile-pic.gif';
?>
<div id="main-column">  
  <div class="clear">&nbsp;
  </div>  
  <div>    
    <div class="h3">      
      <div style="float:left; color:#307ea0;"> 
        <a href="MyProfile.php">My Profile</a> 
      </div>      
      <div style="float:right; padding-right:10px; font-size:12px;">  	  
        <span style="display:block; float:left; padding-right:30px;">
          <a href="paymentauctionopia.php">	  Payment To Admin </a> 
        </span>	  
        <span style="display:block; float:left; padding-right:30px;">
          <a href="paypalacinformation.php">Paypal E-mail</a> 
        </span>	  
        <span style="display:block; float:left; padding-right:30px;"> 
          <a href="EditProfile.php">Edit My Profile</a> 
        </span> 
        <span style="display:block; float:left; padding-right:30px;"> 
          <a href="ViewPublicProfile.php">View Public Profile</a> 
        </span> 
        <span style="display:none; float:left; padding-right:20px;"> 
          <a href="mysummary.php">Return to My Summary</a> 
        </span> 
      </div>    
    </div>    
    <div style="padding:14px 10px; position:relative;">      
      <div class="user-register">        
        <form action="" method="post" style="width:100%; border:0;">          
          <div style="width:440px; float:left;">    
            <h3>Personal Information</h3>            
            <div> 
              <span>              
                <label for="txtusername*">Salutation
                </label>              
              </span> 
              <span class="myprofile-text">              
                <?=$User['Salutation']?>              
              </span>
            </div>            
            <div> 
              <span>              
                <label for="txtpassword*">First Name 
                </label>              
              </span> 
              <span class="myprofile-text">              
                <?=$User['name']?>              
              </span>
            </div>            
            <div> 
              <span>              
                <label for="txtconfirmpassword">Last Name 
                </label>              
              </span> 
              <span class="myprofile-text">              
                <?=$User['Last_Name']?>              
              </span> 
            </div>            <br style="clear: both;" />            
            <div> 
              <span>              
                <label for="txtsecretquestion*">Address 
                </label>              
              </span>
              <span class="myprofile-text">              
                <?=$User['address']?>              
              </span>
            </div>            
            <div> 
              <span>              
                <label for="txtanswer*">Address
                </label>              
              </span>
              <span class="myprofile-text">              
                <?=$User['Address_2']?>              
              </span>
            </div>            
            <div> 
              <span>              
                <label >Country 
                </label>              
              </span>
              <span class="myprofile-text">              
                <?=$User['country']?>              
              </span>
            </div>            
            <div> 
              <span>              
                <label >City 
                </label>              
              </span>
              <span class="myprofile-text">              
                <?=$User['city']?>              
              </span>
            </div>            
            <div> 
              <span>              
                <label >State / Province 
                </label>              
              </span>
              <span class="myprofile-text">              
                <?=$User['state']?>              
              </span>
            </div>            
            <div> 
              <span>              
                <label >Zip / Postal Code 
                </label>              
              </span>
              <span class="myprofile-text">              
                <?=$User['zip_code']?>              
              </span>
            </div>            
            <br style="clear: both;" />            
            <h3>Additional Information</h3>            
            <div> 
              <span>              
                <label for="txtemailaddress*"> 
                  <a href="feedback.php"  style="color:#307ea0;">Feedback</a> 
                </label>              
              </span>
              <span class="myprofile-text">&nbsp; 
              </span>
            </div>            
            <div> 
              <span>              
                <label for="txtconfirmemailadd">Member Since
                </label>              
              </span>
              <span class="myprofile-text">
                <?=date("d / m / y" ,$User['reg_date'])?>
              </span>
            </div>            
            <div> 
              <span>              
                <label for="txtconfirmemailadd">Gender
                </label>              
              </span>
              <span class="myprofile-text">              
                <?=$User['Gender']?>              
              </span>
            </div>            
            <div> 
              <span>              
                <label for="txtconfirmemailadd">Hometown
                </label>              
              </span>
              <span class="myprofile-text">              
                <?=$User['Hometown']?>              
              </span>
            </div>            
            <div> 
              <span>              
                <label for="txtconfirmemailadd">Birthday
                </label>              
              </span>
              <span class="myprofile-text">              
                <?=$User['birthdate']?>              
              </span>
            </div>            
            <div> 
              <span>              
                <label for="txtconfirmemailadd">Interests
                </label>              
              </span>
              <span class="myprofile-text">              
                <?=$User['Interests']?>              
              </span>
            </div>            
            <div> 
              <span>              
                <label for="txtconfirmemailadd">Favorite Music
                </label>              
              </span>
              <span class="myprofile-text">              
                <?=$User['FavoriteMusic']?>              
              </span>
            </div>            
            <div> 
              <span>              
                <label for="txtconfirmemailadd">Favorite Quotes
                </label>              
              </span>
              <span>
                <span class="myprofile-text">              
                  <?=$User['FavoriteQuotes']?>              
                </span>
              </span>
            </div>            
            <div> 
              <span>              
                <label for="txtconfirmemailadd">Occupation(s)
                </label>              
              </span>
              <span class="myprofile-text">              
                <?=$User['Occupation']?>              
              </span>
            </div>            
            <div> 
              <span>              
                <label for="txtconfirmemailadd">College Attended / Attending
                </label>              
              </span>
              <span class="myprofile-text">              
                <?=$User['CollegeAttended']?>              
              </span>
            </div>            
            <div> 
              <span>              
                <label for="txtconfirmemailadd">High School Attended / Attending
                </label>              
              </span>
              <span class="myprofile-text">              
                <?=$User['HighSchoolAttended']?>              
              </span>
            </div>            
            <div> 
              <span>              
                <label for="txtconfirmemailadd" style="color:#307ea0; font-size:10px; font-weight: normal;">* Required Fields
                </label>              
              </span> 
            </div>          
          </div>          
          <div style="width:440px; float:left;">          
            <h3>Personal Picture</h3>          
            <div style="padding-left:35px;"> 
              <span>
                <img src="<?=$imgsrc?>" height="150" width="150" />
              </span> 
            </div>          
            <br style="clear: both;" />          
            <div> 
              <span>            
              <h3>Office Hours</h3>            
              </span>
            </div>          
            <div> 
              <span>            
                <label style="width:120px;">Day of Week
                </label>            
              </span> 
              <span class="startend-time">Start Time
              </span>
              <span class="startend-time">End Time
              </span> 
            </div>          
          <? if(count($officeHours)>0){
					 foreach($officeHours as $val){
            		  ?>          
            <div style="clear:both;"> 
              <span>            
                <label style="width:120px;">            
                  <?=$val['day_o']?>            
                </label>            
              </span> 
              <span class="startend-value">            
                <?=$val['from_o']?>            
              </span> 
              <span class="startend-value">            
                <?=$val['to_o']?>            
              </span> 
            </div>          
            <? }} ?>          
            <div style="clear:both;" style="display:none;">                           
              <div style="display:none;"> 
                <span>              
                  <label for="txtconfirmanswer">Username
                  </label>              
                </span>              
                <input type="text" name="textfield4" />            
              </div>            
              <div style="display:none;"> 
                <span>              
                  <label for="txtconfirmanswer">Password
                  </label>              
                </span>              
                <input type="text" name="textfield3" />            
              </div>            
              <div style="display:none;"> 
                <span>              
                  <label >Secret Question
                  </label>              
                </span>
                <span>              
                  <input type="text" name="textfield" />              
                </span>
              </div>            
              <div style="display:none;"> 
                <span>              
                  <label >Secret Answer
                  </label>              
                </span>              
                <input type="text" name="textfield2" />            
              </div>            
              <br style="clear: both;" />          
            </div>		  
          </div>          
          <br style="clear: both;" />        
        </form>      
      </div>    
    </div>  
  </div>
</div>              
<? //John Harre 9/13/2010 - disabled due to FF bombing. The file is backed up and all contents have been cleared out.
  include('footer.php');?>