<? 
  include('header.php');
  include('leftLinks.php');
  if(!isset($_POST['batchOperatin'])){
  $_POST['batchOperatin']='';
  $action=$_POST['batchOperatin'];
  }
  if(!isset($_POST['msgids'])){ 
  $_POST['msgids']='';
  $act_ids=$_POST['msgids'];
  }
  if(count($act_ids)>0){
  	if($action=="archive"){
  	foreach($act_ids as $val)
  		{
  		$cms->SetMessageArchive($val);
  		}
  	}
  	if($action=="delete"){
  	foreach($act_ids as $val)
    	{
    	   $cms->DeleteMessage($val);
    	}
  	}
  }
  $recmsglist=$cms->archivedMessage($cms->loggedUserId);
  if(!isset($recmsglist['lists'])) $recmsglist['lists']=array();
  $recmsglist=$recmsglist['lists'];
?>
  <div id="center-column" class="single">
    <div style="font: bold 14px Arial;">
      Archived Messages
    </div>
    <div style="clear:both;">
      <div class="subtitle" style="font:bold 14px Arial; padding-top:20px; padding-bottom:5px;"></div>
        <div id="table-row" class="table_header">
          <div>
            <div style="position:relative; height:25px;" >
              <div class="med" style="width:150px; font-size:11px; color:#000; font-weight:bold;">
                From/To
              </div>
              <div class="big" style="width:330px; font-size:11px; color:#000; font-weight:bold;">
                Subject
              </div>
              <div class="small" style="width:140px;font-size:11px; font-weight:bold;">
                <a href="#" style="color:#000;">Date / Time</a>
              </div>
              <div class="small" style="width:80px;font-size:11px; color:#000; font-weight:bold;">
                <a href="javascript: checkedAll('fm',true);" style="color:#000;">Select All</a>
              </div>
            </div>
            <script language="javascript">
        		var checkedSubmit=function(operation){
        		SetVal('batchOperatin',operation);
        		document.fm.submit();
        		}
        		function checkedAll(id, checked){
          		var el = document.getElementById(id);
          		for (var i = 0; i < el.elements.length; i++){
          		el.elements[i].checked = checked;
          		}
        		}
        		</script>
            <form action="archivedmsg.php" name="fm" id="fm" method="post">
            <input type="hidden" name="batchOperatin" id="batchOperatin" value="true" />
            <?
          		if(count($recmsglist)>0)
          		foreach($recmsglist as $val)
          		{
          		$detailslink='MsgDetails.php?'.$val['detailid'].'='.$val['msgid'];
            ?>
            <div>
              <div class="med" style="width:150px;">
                <?=$val['tofrom']?>
              </div>
              <div class="big" style="width:330px;">
                <a href="<?=$detailslink?>">
                <?=$val['subject']?></a>
              </div>
              <div class="small" style="width:140px;">
                <?=$val['datetime']?>
              </div>
              <div class="small" style="text-align:center;">
                <input type="checkbox" name="msgids[]" value="<?=$val['msgid']?>" />
              </div>
            </div>
            <? } ?>
            </form>
          </div>
          <div class="td-clear"></div>
          <div style="text-align:right; padding-right:40px; padding-top:20px;">&nbsp;&nbsp;&nbsp;
            <img src="/images/delete.gif" onclick="javascript:checkedSubmit('delete');" style="cursor:pointer" />
          </div>
        </div>
    </div>	  	
    <br style="clear:both;" />
  </div>
<? //John Harre 9/13/2010 - disabled due to FF bombing. The file is backed up and all contents have been cleared out.
  include('footer.php');?>