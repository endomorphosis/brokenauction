<? include('header.php');
//select content
$contentsql="select * from content where `contentid`='15' limit 1";
$content=$cms->getresults($contentsql);
?>
<div id="main-column">	
  <div class="clear">&nbsp;
  </div>		 	
  <div>		
    <div class="h3">		
      <div style="float:left; color:#000;">Privacy Policy
      </div>		
      <div style="float:right; padding-right:10px;">	
      </div>		 
    </div>				     
    <div style="padding:14px 10px; position:relative;">        
      <div class="user-register">            
        <div>
          <span>		  	
            <label style="width:700px; font:14px Arial, Helvetica, sans-serif; color:#307ea0; display:block; padding:10px 80px;">			
              <span style="display:block; width:100%; text-align:center; font:bold 20px Arial, Helvetica, sans-serif; color:#307ea0;">Privacy Policy
              </span>			
              <br/>
              <br/>Broken Buy has established this Privacy Policy to explain to you how your information is protected, collected and used, which may be updated by Broken Buy from time to time. Broken Buy will provide notice of materially significant changes to this privacy policy by posting notice on the Broken Buy site. Your use of the Broken Buy website or the Services signifies acknowledgement of and agreement to this Privacy Policy.
              <br/>
              <br/>1. Protecting your privacy. 
              <br/>
              <br/>Broken Buy does not: 
              <br/>&bull;	Share your information with third parties for marketing purposes. 
              <br/>&bull;	Engage in cross-marketing or link-referral programs with other sites. 
              <br/>&bull;	Employ tracking devices for marketing purposes (�cookies,� �web beacons,� etc.) 
              <br/>&bull;	Send you unsolicited communications for marketing purposes. 
              <br/>&bull;	Knowingly collect any information from persons under the age of 13. If we learn that a posting is by a person under the age of 13, we will remove that post.
              <br/>
              <br/>  Third Party Content
              <br/>&bull;	Broken Buy, or people who post on Broken Buy, may provide links to third party websites, which may have different privacy practices. We are not responsible for, nor have any control over, the privacy policies of those third party websites, and encourage all users to read the privacy policies of each and every website visited.  
              <br/>
              <br/>2. Data we may collect:
              <br/>&bull;	Your email address. 
              <br/>&bull;	Your telephone number for account authentication purposes, and may transmit it to a third party service for tele-robotic verification. 
              <br/>  &bull;	Personal information. 
              <br/>&bull;	Our web logs collect standard web log entries for each page served, including your IP address, page URL, and timestamp. Web logs help us to diagnose problems with our server, to administer the Broken Buy site, and to otherwise provide our service to you. 
              <br/>
              <br/>3.  Why we may collect data:
              <br/>&bull;	Registration for services.
              <br/>&bull;	Sending self-publishing and confirmation emails, providing subscription email services.
              <br/>&bull;	Authenticating user accounts.
              <br/>&bull;	Billing purposes
              <br/>&bull;	You provided such data in feedback, comments or otherwise posted it on our classifieds or interactive forums 
              <br/>
              <br/>Data we store
              <br/>&bull;	All classified and forum postings are stored in our database, even after "deletion," and may be archived elsewhere.
              <br/>  &bull;	Our web logs and other records are stored indefinitely. 
              <br/>&bull;	Registered job posters can access and update their account information through the account homepage.
              <br/>  &bull;	Although we make good faith efforts to store the information in a secure operating environment that is not available to the public, we cannot guarantee complete security. 
              <br/>
              <br/>4. Archiving and display of Broken Buy postings by search engines and other sites Some search engines and other sites not affiliated with Broken Buy may archive or otherwise make available Broken Buy postings, including resumes.
              <br/>
              <br/>5. Circumstances in which Broken Buy may disclose information about its users:
              <br/>&bull;	If you expressly authorize Broken Buy to such information.
              <br/>&bull;	If required to do so by law or in the good faith belief that such disclosure is reasonably necessary to respond to subpoenas, court orders, or other legal process.  &bull;	If Broken Buy has a good faith belief that such disclosure is reasonably necessary to: enforce our Terms of Use; respond to claims that any posting or other content violates the rights of third-parties; or protect the rights, property, or personal safety of Broken Buy, its users or the general public. 
              <br/>6. International Users
              <br/>
              <br/>By visiting our web site and providing us with data, you acknowledge and agree that due to the international dimension of Broken Buy we may use the data collected in the course of our relationship for the purposes identified in this policy or in our other communications with you, including the transmission of information outside your resident jurisdiction. In addition, please understand that such data may be stored on servers located in the United States. By providing us with your data, you consent to the transfer of such data.  
            </label>
          </span>
        </div>      
      </div>    
    </div>	
  </div>
</div>
<? //John Harre 9/13/2010 - disabled due to FF bombing. The file is backed up and all contents have been cleared out.
  include('footer.php');?>