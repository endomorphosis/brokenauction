<?
include('header.php');
?>
<ul class="breadcrumb"></ul>
<?
include('leftLinks.php');
?>
<div id="center-column" class="single transfer">
<?
if (isset($_GET['id'])){
	$id = $_GET['id'];
	mysql_query('DELETE FROM `ebay_rating` WHERE `user_id` = ' . $id);
	mysql_query('DELETE FROM `ebay_feedback` WHERE `user_id` = ' . $id);
	echo '<div align="center" style="padding: 10px;">Your eBay information was deleted.</div>';
}
if(isset($_GET['message'])){
	echo '<div align="center" style="padding: 10px;">Your eBay information was saved.</div>';
}
?>
<?
	require_once('ebay/ebay.php');
	$ebay = new eBay();
	$result= $ebay->callAutorize();
	if(!isset($_GET['username'])){
	?>
		<div align="center">
			<button type="button">
				<a style="color: black" href="<?php echo $result;?>">Transfer your ratings from eBay</a>
			</button>
			<br />
			<button type="button">
				<a style="color: black" href="transferEbayPage.php?id=<?php echo $_SESSION['m_info']['user_id']; ?>">
					Delete all information from eBay
				</a>
			</button>
		</div>
	<?php
	}
	if (isset($_GET['username']) && !empty($_GET['username'])){
		$username = $_GET['username'];
		$token = $_GET['ebaytkn'];
		$eBay = new eBay();
		$requestResult = $ebay->getFeedback($username, $token);
		$ratings = $eBay->fetchFeedBack($requestResult);
	?>
		<style type="text/css">
			table.sellerRating tr td{
				text-align: center;
				padding: 5px;
			}
			table.sellerRating{
				margin: 0 auto;
			}
		</style>
	<table  class="sellerRating" border="1">
		<tr>
			<th colspan="3">Feedbacks</th>
		</tr>
		<tr>
			<td>UserID</td>
			<td>Comment</td>
			<td>Type</td>
		</tr>
		<?php for($i=0, $n=count($ratings['FeedBacks']); $i<$n; $i++):?>
		<tr>
			<td><?php echo $ratings['FeedBacks'][$i][0] ?></td>
			<td><?php echo $ratings['FeedBacks'][$i][1] ?></td>
			<td><?php echo $ratings['FeedBacks'][$i][2] ?></td>
		</tr>
		<?php endfor; ?>
	</table>
	<br />
	<table class="sellerRating" border="1">
		<tr>
			<th colspan="3">
				Deteils seller ratings
			</th>
		</tr>
		<tr>
			<td>Criterial</td>
			<td>Average rating</td>
			<td>Number of ratings</td>
		</tr>
		<?php for($i=1, $n=count($ratings['Ratings'])+1; $i<$n; $i++):?>
		<tr>
			<td><?php if ($i==1){ echo 'Item as described';}elseif($i==4){ echo 'Shipping and handling charges';} else {echo $ratings['Ratings'][$i]['RatingDetail'];} ?></td>
			<td><?php echo $ratings['Ratings'][$i]['Rating']; ?></td>
			<td><?php echo $ratings['Ratings'][$i]['RatingCount']; ?></td>
		</tr>
		<?php endfor;?>
	</table>
	<div style="text-align: center; margin-top: 10px;">
		<button type="button">
			<a style="color: black" href="transferEbayPage.php?message=save">
				Accept and save transfered information from eBay
			</a>
		</button> 

		<button type="button">
			<a style="color: black" href="transferEbayPage.php?id=<?php echo $_SESSION['m_info']['user_id']; ?>">
				Decline and delete transfered information from eBay
			</a>
		</button>
	</div>
</div>
<? }
//John Harre 9/13/2010 - disabled due to FF bombing. The file is backed up and all contents have been cleared out.
  include('footer.php');?>