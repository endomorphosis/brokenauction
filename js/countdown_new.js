/*

Author: Robert Hashemian

http://www.hashemian.com/



You can use this code in any manner so long as the author's

name, Web address and this disclaimer is kept intact.

********************************************************

Usage Sample:



<script language="JavaScript">

TargetDate = "12/31/2020 5:00 AM";

BackColor = "palegreen";

ForeColor = "navy";

CountActive = true;

CountStepper = -1;

LeadingZero = true;

DisplayFormat = "%%D%% Days, %%H%% Hours, %%M%% Minutes, %%S%% Seconds.";

FinishMessage = "It is finally here!";

</script>

<script language="JavaScript" src="http://scripts.hashemian.com/js/countdown.js"></script>

*/
var SetTimeOutPeriod;


function calcage(secs, num1, num2) {

  s = ((Math.floor(secs/num1))%num2).toString();

  if (LeadingZero && s.length < 2)

    s = "0" + s;

  return "<b>" + s + "</b>";

}



function CountBack0(secs,div_no) {
  if (secs < 0) {

    document.getElementById("cntdwn"+div_no).innerHTML = FinishMessage;

    return;

  }

  DisplayStr = DisplayFormat.replace(/%%D%%/g, calcage(secs,86400,100000));

  DisplayStr = DisplayStr.replace(/%%H%%/g, calcage(secs,3600,24));

  DisplayStr = DisplayStr.replace(/%%M%%/g, calcage(secs,60,60));

  DisplayStr = DisplayStr.replace(/%%S%%/g, calcage(secs,1,60));



  document.getElementById("cntdwn"+div_no).innerHTML = DisplayStr;
//alert(div_no);
if (CountActive)
	setTimeout("CountBack0(" + (secs+CountStepper) + "," + (div_no) + ")", SetTimeOutPeriod);

}
function CountBack1(secs,div_no) {
  if (secs < 0) {

    document.getElementById("cntdwn"+div_no).innerHTML = FinishMessage;

    return;

  }

  DisplayStr = DisplayFormat.replace(/%%D%%/g, calcage(secs,86400,100000));

  DisplayStr = DisplayStr.replace(/%%H%%/g, calcage(secs,3600,24));

  DisplayStr = DisplayStr.replace(/%%M%%/g, calcage(secs,60,60));

  DisplayStr = DisplayStr.replace(/%%S%%/g, calcage(secs,1,60));



  document.getElementById("cntdwn"+div_no).innerHTML = DisplayStr;
//alert(div_no);
if (CountActive)
{
   // alert(div_no);
	setTimeout("CountBack1(" + (secs+CountStepper) + "," + (div_no) + ")", SetTimeOutPeriod);
}

}
function CountBack2(secs,div_no) {
  if (secs < 0) {

    document.getElementById("cntdwn"+div_no).innerHTML = FinishMessage;

    return;

  }

  DisplayStr = DisplayFormat.replace(/%%D%%/g, calcage(secs,86400,100000));

  DisplayStr = DisplayStr.replace(/%%H%%/g, calcage(secs,3600,24));

  DisplayStr = DisplayStr.replace(/%%M%%/g, calcage(secs,60,60));

  DisplayStr = DisplayStr.replace(/%%S%%/g, calcage(secs,1,60));



  document.getElementById("cntdwn"+div_no).innerHTML = DisplayStr;
//alert(div_no);
if (CountActive)
{
   // alert(div_no);
	setTimeout("CountBack2(" + (secs+CountStepper) + "," + (div_no) + ")", SetTimeOutPeriod);
}

}
function CountBack3(secs,div_no) {
  if (secs < 0) {

    document.getElementById("cntdwn"+div_no).innerHTML = FinishMessage;

    return;

  }

  DisplayStr = DisplayFormat.replace(/%%D%%/g, calcage(secs,86400,100000));

  DisplayStr = DisplayStr.replace(/%%H%%/g, calcage(secs,3600,24));

  DisplayStr = DisplayStr.replace(/%%M%%/g, calcage(secs,60,60));

  DisplayStr = DisplayStr.replace(/%%S%%/g, calcage(secs,1,60));



  document.getElementById("cntdwn"+div_no).innerHTML = DisplayStr;
//alert(div_no);
if (CountActive)
{
   // alert(div_no);
	setTimeout("CountBack3(" + (secs+CountStepper) + "," + (div_no) + ")", SetTimeOutPeriod);
}

}
function CountBack4(secs,div_no) {
  if (secs < 0) {

    document.getElementById("cntdwn"+div_no).innerHTML = FinishMessage;

    return;

  }

  DisplayStr = DisplayFormat.replace(/%%D%%/g, calcage(secs,86400,100000));

  DisplayStr = DisplayStr.replace(/%%H%%/g, calcage(secs,3600,24));

  DisplayStr = DisplayStr.replace(/%%M%%/g, calcage(secs,60,60));

  DisplayStr = DisplayStr.replace(/%%S%%/g, calcage(secs,1,60));



  document.getElementById("cntdwn"+div_no).innerHTML = DisplayStr;
//alert(div_no);
if (CountActive)
{
   // alert(div_no);
	setTimeout("CountBack4(" + (secs+CountStepper) + "," + (div_no) + ")", SetTimeOutPeriod);
}

}
function CountBack5(secs,div_no) {
  if (secs < 0) {

    document.getElementById("cntdwn"+div_no).innerHTML = FinishMessage;

    return;

  }

  DisplayStr = DisplayFormat.replace(/%%D%%/g, calcage(secs,86400,100000));

  DisplayStr = DisplayStr.replace(/%%H%%/g, calcage(secs,3600,24));

  DisplayStr = DisplayStr.replace(/%%M%%/g, calcage(secs,60,60));

  DisplayStr = DisplayStr.replace(/%%S%%/g, calcage(secs,1,60));



  document.getElementById("cntdwn"+div_no).innerHTML = DisplayStr;
//alert(div_no);
if (CountActive)
{
   // alert(div_no);
	setTimeout("CountBack5(" + (secs+CountStepper) + "," + (div_no) + ")", SetTimeOutPeriod);
}

}
function CountBack6(secs,div_no) {
  if (secs < 0) {

    document.getElementById("cntdwn"+div_no).innerHTML = FinishMessage;

    return;

  }

  DisplayStr = DisplayFormat.replace(/%%D%%/g, calcage(secs,86400,100000));

  DisplayStr = DisplayStr.replace(/%%H%%/g, calcage(secs,3600,24));

  DisplayStr = DisplayStr.replace(/%%M%%/g, calcage(secs,60,60));

  DisplayStr = DisplayStr.replace(/%%S%%/g, calcage(secs,1,60));



  document.getElementById("cntdwn"+div_no).innerHTML = DisplayStr;
//alert(div_no);
if (CountActive)
{
   // alert(div_no);
	setTimeout("CountBack6(" + (secs+CountStepper) + "," + (div_no) + ")", SetTimeOutPeriod);
}

}
function CountBack7(secs,div_no) {
  if (secs < 0) {

    document.getElementById("cntdwn"+div_no).innerHTML = FinishMessage;

    return;

  }

  DisplayStr = DisplayFormat.replace(/%%D%%/g, calcage(secs,86400,100000));

  DisplayStr = DisplayStr.replace(/%%H%%/g, calcage(secs,3600,24));

  DisplayStr = DisplayStr.replace(/%%M%%/g, calcage(secs,60,60));

  DisplayStr = DisplayStr.replace(/%%S%%/g, calcage(secs,1,60));



  document.getElementById("cntdwn"+div_no).innerHTML = DisplayStr;
//alert(div_no);
if (CountActive)
{
   // alert(div_no);
	setTimeout("CountBack7(" + (secs+CountStepper) + "," + (div_no) + ")", SetTimeOutPeriod);
}

}
function CountBack8(secs,div_no) {
  if (secs < 0) {

    document.getElementById("cntdwn"+div_no).innerHTML = FinishMessage;

    return;

  }

  DisplayStr = DisplayFormat.replace(/%%D%%/g, calcage(secs,86400,100000));

  DisplayStr = DisplayStr.replace(/%%H%%/g, calcage(secs,3600,24));

  DisplayStr = DisplayStr.replace(/%%M%%/g, calcage(secs,60,60));

  DisplayStr = DisplayStr.replace(/%%S%%/g, calcage(secs,1,60));



  document.getElementById("cntdwn"+div_no).innerHTML = DisplayStr;
//alert(div_no);
if (CountActive)
{
   // alert(div_no);
	setTimeout("CountBack8(" + (secs+CountStepper) + "," + (div_no) + ")", SetTimeOutPeriod);
}

}
function CountBack9(secs,div_no) {
  if (secs < 0) {

    document.getElementById("cntdwn"+div_no).innerHTML = FinishMessage;

    return;

  }

  DisplayStr = DisplayFormat.replace(/%%D%%/g, calcage(secs,86400,100000));

  DisplayStr = DisplayStr.replace(/%%H%%/g, calcage(secs,3600,24));

  DisplayStr = DisplayStr.replace(/%%M%%/g, calcage(secs,60,60));

  DisplayStr = DisplayStr.replace(/%%S%%/g, calcage(secs,1,60));



  document.getElementById("cntdwn"+div_no).innerHTML = DisplayStr;
//alert(div_no);
if (CountActive)
{
   // alert(div_no);
	setTimeout("CountBack9(" + (secs+CountStepper) + "," + (div_no) + ")", SetTimeOutPeriod);
}

}
function CountBack10(secs,div_no) {
  if (secs < 0) {

    document.getElementById("cntdwn"+div_no).innerHTML = FinishMessage;

    return;

  }

  DisplayStr = DisplayFormat.replace(/%%D%%/g, calcage(secs,86400,100000));

  DisplayStr = DisplayStr.replace(/%%H%%/g, calcage(secs,3600,24));

  DisplayStr = DisplayStr.replace(/%%M%%/g, calcage(secs,60,60));

  DisplayStr = DisplayStr.replace(/%%S%%/g, calcage(secs,1,60));



  document.getElementById("cntdwn"+div_no).innerHTML = DisplayStr;
//alert(div_no);
if (CountActive)
{
   // alert(div_no);
	setTimeout("CountBack10(" + (secs+CountStepper) + "," + (div_no) + ")", SetTimeOutPeriod);
}

}
function CountBack11(secs,div_no) {
  if (secs < 0) {

    document.getElementById("cntdwn"+div_no).innerHTML = FinishMessage;

    return;

  }

  DisplayStr = DisplayFormat.replace(/%%D%%/g, calcage(secs,86400,100000));

  DisplayStr = DisplayStr.replace(/%%H%%/g, calcage(secs,3600,24));

  DisplayStr = DisplayStr.replace(/%%M%%/g, calcage(secs,60,60));

  DisplayStr = DisplayStr.replace(/%%S%%/g, calcage(secs,1,60));



  document.getElementById("cntdwn"+div_no).innerHTML = DisplayStr;
//alert(div_no);
if (CountActive)
{
   // alert(div_no);
	setTimeout("CountBack11(" + (secs+CountStepper) + "," + (div_no) + ")", SetTimeOutPeriod);
}

}
function CountBack12(secs,div_no) {
  if (secs < 0) {

    document.getElementById("cntdwn"+div_no).innerHTML = FinishMessage;

    return;

  }

  DisplayStr = DisplayFormat.replace(/%%D%%/g, calcage(secs,86400,100000));

  DisplayStr = DisplayStr.replace(/%%H%%/g, calcage(secs,3600,24));

  DisplayStr = DisplayStr.replace(/%%M%%/g, calcage(secs,60,60));

  DisplayStr = DisplayStr.replace(/%%S%%/g, calcage(secs,1,60));



  document.getElementById("cntdwn"+div_no).innerHTML = DisplayStr;
//alert(div_no);
if (CountActive)
{
   // alert(div_no);
	setTimeout("CountBack16(" + (secs+CountStepper) + "," + (div_no) + ")", SetTimeOutPeriod);
}

}
function CountBack13(secs,div_no) {
  if (secs < 0) {

    document.getElementById("cntdwn"+div_no).innerHTML = FinishMessage;

    return;

  }

  DisplayStr = DisplayFormat.replace(/%%D%%/g, calcage(secs,86400,100000));

  DisplayStr = DisplayStr.replace(/%%H%%/g, calcage(secs,3600,24));

  DisplayStr = DisplayStr.replace(/%%M%%/g, calcage(secs,60,60));

  DisplayStr = DisplayStr.replace(/%%S%%/g, calcage(secs,1,60));



  document.getElementById("cntdwn"+div_no).innerHTML = DisplayStr;
//alert(div_no);
if (CountActive)
{
   // alert(div_no);
	setTimeout("CountBack13(" + (secs+CountStepper) + "," + (div_no) + ")", SetTimeOutPeriod);
}

}
function CountBack14(secs,div_no) {
  if (secs < 0) {

    document.getElementById("cntdwn"+div_no).innerHTML = FinishMessage;

    return;

  }

  DisplayStr = DisplayFormat.replace(/%%D%%/g, calcage(secs,86400,100000));

  DisplayStr = DisplayStr.replace(/%%H%%/g, calcage(secs,3600,24));

  DisplayStr = DisplayStr.replace(/%%M%%/g, calcage(secs,60,60));

  DisplayStr = DisplayStr.replace(/%%S%%/g, calcage(secs,1,60));



  document.getElementById("cntdwn"+div_no).innerHTML = DisplayStr;
//alert(div_no);
if (CountActive)
{
   // alert(div_no);
	setTimeout("CountBack14(" + (secs+CountStepper) + "," + (div_no) + ")", SetTimeOutPeriod);
}

}
function CountBack15(secs,div_no) {
  if (secs < 0) {

    document.getElementById("cntdwn"+div_no).innerHTML = FinishMessage;

    return;

  }

  DisplayStr = DisplayFormat.replace(/%%D%%/g, calcage(secs,86400,100000));

  DisplayStr = DisplayStr.replace(/%%H%%/g, calcage(secs,3600,24));

  DisplayStr = DisplayStr.replace(/%%M%%/g, calcage(secs,60,60));

  DisplayStr = DisplayStr.replace(/%%S%%/g, calcage(secs,1,60));



  document.getElementById("cntdwn"+div_no).innerHTML = DisplayStr;
//alert(div_no);
if (CountActive)
{
   // alert(div_no);
	setTimeout("CountBack15(" + (secs+CountStepper) + "," + (div_no) + ")", SetTimeOutPeriod);
}

}
function CountBack16(secs,div_no) {
  if (secs < 0) {

    document.getElementById("cntdwn"+div_no).innerHTML = FinishMessage;

    return;

  }

  DisplayStr = DisplayFormat.replace(/%%D%%/g, calcage(secs,86400,100000));

  DisplayStr = DisplayStr.replace(/%%H%%/g, calcage(secs,3600,24));

  DisplayStr = DisplayStr.replace(/%%M%%/g, calcage(secs,60,60));

  DisplayStr = DisplayStr.replace(/%%S%%/g, calcage(secs,1,60));



  document.getElementById("cntdwn"+div_no).innerHTML = DisplayStr;
//alert(div_no);
if (CountActive)
{
   // alert(div_no);
	setTimeout("CountBack16(" + (secs+CountStepper) + "," + (div_no) + ")", SetTimeOutPeriod);
}

}
function CountBack17(secs,div_no) {
  if (secs < 0) {

    document.getElementById("cntdwn"+div_no).innerHTML = FinishMessage;

    return;

  }

  DisplayStr = DisplayFormat.replace(/%%D%%/g, calcage(secs,86400,100000));

  DisplayStr = DisplayStr.replace(/%%H%%/g, calcage(secs,3600,24));

  DisplayStr = DisplayStr.replace(/%%M%%/g, calcage(secs,60,60));

  DisplayStr = DisplayStr.replace(/%%S%%/g, calcage(secs,1,60));



  document.getElementById("cntdwn"+div_no).innerHTML = DisplayStr;
//alert(div_no);
if (CountActive)
{
   // alert(div_no);
	setTimeout("CountBack17(" + (secs+CountStepper) + "," + (div_no) + ")", SetTimeOutPeriod);
}

}
function CountBack18(secs,div_no) {
  if (secs < 0) {

    document.getElementById("cntdwn"+div_no).innerHTML = FinishMessage;

    return;

  }

  DisplayStr = DisplayFormat.replace(/%%D%%/g, calcage(secs,86400,100000));

  DisplayStr = DisplayStr.replace(/%%H%%/g, calcage(secs,3600,24));

  DisplayStr = DisplayStr.replace(/%%M%%/g, calcage(secs,60,60));

  DisplayStr = DisplayStr.replace(/%%S%%/g, calcage(secs,1,60));



  document.getElementById("cntdwn"+div_no).innerHTML = DisplayStr;
//alert(div_no);
if (CountActive)
{
   // alert(div_no);
	setTimeout("CountBack18(" + (secs+CountStepper) + "," + (div_no) + ")", SetTimeOutPeriod);
}

}
function CountBack19(secs,div_no) {
  if (secs < 0) {

    document.getElementById("cntdwn"+div_no).innerHTML = FinishMessage;

    return;

  }

  DisplayStr = DisplayFormat.replace(/%%D%%/g, calcage(secs,86400,100000));

  DisplayStr = DisplayStr.replace(/%%H%%/g, calcage(secs,3600,24));

  DisplayStr = DisplayStr.replace(/%%M%%/g, calcage(secs,60,60));

  DisplayStr = DisplayStr.replace(/%%S%%/g, calcage(secs,1,60));



  document.getElementById("cntdwn"+div_no).innerHTML = DisplayStr;
//alert(div_no);
if (CountActive)
{
   // alert(div_no);
	setTimeout("CountBack19(" + (secs+CountStepper) + "," + (div_no) + ")", SetTimeOutPeriod);
}

}
function CountBack20(secs,div_no) {
  if (secs < 0) {

    document.getElementById("cntdwn"+div_no).innerHTML = FinishMessage;

    return;

  }

  DisplayStr = DisplayFormat.replace(/%%D%%/g, calcage(secs,86400,100000));

  DisplayStr = DisplayStr.replace(/%%H%%/g, calcage(secs,3600,24));

  DisplayStr = DisplayStr.replace(/%%M%%/g, calcage(secs,60,60));

  DisplayStr = DisplayStr.replace(/%%S%%/g, calcage(secs,1,60));



  document.getElementById("cntdwn"+div_no).innerHTML = DisplayStr;
//alert(div_no);
if (CountActive)
{
   // alert(div_no);
	setTimeout("CountBack20(" + (secs+CountStepper) + "," + (div_no) + ")", SetTimeOutPeriod);
}

}

function putspan(backcolor, forecolor) {

 document.write("<span id='cntdwn' style='background-color:" + backcolor + 

                "; color:" + forecolor + "'></span>");

}


function test(TargetDate,ToDate,DisplayFormat,FinishMessage,div_no)
{
if (typeof(BackColor)=="undefined")

  BackColor = "";

if (typeof(ForeColor)=="undefined")

  ForeColor= "";

if (typeof(TargetDate)=="undefined")

  TargetDate = "12/31/2020 5:00 AM";

if (typeof(DisplayFormat)=="undefined")

  DisplayFormat = "%%D%% Days, %%H%% Hours, %%M%% Minutes, %%S%% Seconds.";

if (typeof(CountActive)=="undefined")

  CountActive = true;

if (typeof(FinishMessage)=="undefined")

  FinishMessage = "";

if (typeof(CountStepper)!="number")

  CountStepper = -1;

if (typeof(LeadingZero)=="undefined")

  LeadingZero = true;





CountStepper = Math.ceil(CountStepper);

if (CountStepper == 0)

  CountActive = false;

SetTimeOutPeriod = (Math.abs(CountStepper)-1)*1000 + 990;
//putspan(BackColor, ForeColor);

var dthen = new Date(TargetDate);

var dnow = new Date(ToDate);

if(CountStepper>0)

  ddiff = new Date(dnow-dthen);

else

  ddiff = new Date(dthen-dnow);

gsecs = Math.floor(ddiff.valueOf()/1000);
if(div_no == 0)
CountBack0(gsecs,div_no);
if(div_no == 1)
CountBack1(gsecs,div_no);
if(div_no == 2)
CountBack2(gsecs,div_no);
if(div_no == 3)
CountBack3(gsecs,div_no);
if(div_no == 4)
CountBack4(gsecs,div_no);
if(div_no == 5)
CountBack5(gsecs,div_no);
if(div_no == 6)
CountBack6(gsecs,div_no);
if(div_no == 7)
CountBack7(gsecs,div_no);
if(div_no == 8)
CountBack8(gsecs,div_no);
if(div_no == 9)
CountBack9(gsecs,div_no);
if(div_no == 10)
CountBack10(gsecs,div_no);
if(div_no == 11)
CountBack11(gsecs,div_no);
if(div_no == 12)
CountBack12(gsecs,div_no);
if(div_no == 13)
CountBack13(gsecs,div_no);
if(div_no == 14)
CountBack14(gsecs,div_no);
if(div_no == 15)
CountBack15(gsecs,div_no);
if(div_no == 16)
CountBack16(gsecs,div_no);
if(div_no == 17)
CountBack17(gsecs,div_no);
if(div_no == 18)
CountBack18(gsecs,div_no);
if(div_no == 19)
CountBack19(gsecs,div_no);
if(div_no == 20)
CountBack20(gsecs,div_no);

}
