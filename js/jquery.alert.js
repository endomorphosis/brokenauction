/*
 * jQuery Alert Animations
 * Copyright 2009 Ted Sczelecki and Radio Active Media (http://www.radio4ctive.com)
 * Released under the MIT and GPL licenses.
 */

(function($){
	
	$.fn.chat_alert = function(options){

		var defaults = {startColor: '#ffffff',
						endColor : "#ef7e00",
						opacity : false,
						opacityMax : 0.5};
						
		var opts = $.extend(defaults , options);

		return this.each(function(){
			var $this = $(this);
			var o = opts;
			
			function startLoop(){
				if ( o.opacity == false ){
					$($this)
					.animate({color : o.endColor}, 1000)
					.animate({color : o.startColor}, 1000, 
					null, function(){
						startLoop()
					});
				} else {
					$($this)
						.animate({opacity: o.opacityMax}, 1000)
						.animate({opacity:'0'}, 1000, null, function(){
							startLoop();
						});
				}
			}
			
			$($this).click(function(){
				$($this).stop(true);
				if ( o.opacity == false ){
					$($this).css('color', o.startColor);
				} else {
					$($this).css('opacity', '0');
				}
			})
			
			startLoop();
		});
	}
	
})(jQuery)