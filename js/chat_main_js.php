<?
//======================================================================================================================
// CHAT_MAIN_JS.PHP module
//======================================================================================================================
?>
<script type='text/javascript'>
$(document).ready(function(){
	var beepSound;
	soundManager.url = '/swf/';
	soundManager.onready(function(oStatus){
		beepSound = soundManager.createSound({
			id: 'beep',
			url: 'mp3/cool_bubble.mp3'
		});
	});

	function ping(user_id){
/*
		$.ajax({
			type: 'GET',
			url: 'mc_pinged.php',
			data: {
				'user_id': user_id
			},
			dataType: 'script',
			success: function(data){
//				if(ret == false){
//					beepSound.play();
//				}
			}
		});
*/
	}


               /*
				 * var name = prompt("Enter your chat name:", "tomp");
				 *  // default name is 'Guest' if (!name || name === ' ') { name =
				 * "tomp"; }
				 */
				var name = '<?php echo $_SESSION['usrname']; ?>';
                var user_id;
                var currentChatId;
                var currentProductId;
                var messageTimeout;
                var chatTimeout;
                var currentChat;
                var currentTab;
                var sendingChat = false;
                var liveChatOpened = false;
				var textColor = '#FFFFFF';
				
                getUserInfo(name);
                /*
				 * CORE FUNCTIONALITY START
				 */
                var targetItem;
                var processing = false;

				
                $('#toolbar_nav li').click(function(e){

                    if (targetItem != undefined) {

                        if (('#' + $(this).attr('class')) != targetItem) {
                            closePopup();
							if ($(this).attr('id') == 'toolbar_live_chat'){
								liveChatOpened = true;
								startMessageUpdate();
							}
							if ($(this).attr('id') == 'toolbar_contacts')getContacts(true);
                            openPopup($(this).attr('class'));
                        }
                        else {
                            closePopup();
                            targetItem = undefined;
                        }
                        
                        
                    }
                    else {
						if ($(this).attr('id') == 'toolbar_live_chat'){
							liveChatOpened = true;
							startMessageUpdate();
						}
						if ($(this).attr('id') == 'toolbar_contacts')getContacts(true);
                        openPopup($(this).attr('class'));
                    }
                });
                
                function closePopup(){
					clearInterval(messageTimeout);
					messageTimeout = null;
                    liveChatOpened = false;
                    $(targetItem).stop().animate({
                        top: 100
                    }, 'slow')     
                }
                function openPopup(_ti){
			    // getChatInfo();
                    if (_ti == 'live_chat_popup'){
						 liveChatOpened = true;
						updateChat('updateStatus');
					}
                    targetItem = '#' + _ti;
                    
                    if(targetItem == "#sell_popup"){
                    	//Populate #sell_popup
                    	getSellingProducts();
                    }
                    
                    $(targetItem).stop().animate({
                        top: ($(targetItem).height() - 5) * -1
                    }, 'slow')

                	set_live_chat_color("off");
                }
                
                function setupVars(){
					
                    if (($('#tool_bar_tabs > ul > li').length) > 0) {
                        if (currentChat == undefined) currentChat = $('#opened_chats > div:eq(0)');
						
                        var chatId = $(currentChat).attr('id');
                        var chatArr = chatId.split('chat_');
                        currentTab = '#tab_' + chatArr[1];
                        
                        $(currentTab).addClass('current_chat');
                        $(currentChat).css('display', 'block');
                        // sets the currentChatId
                        var getChatId = $('#tool_bar_tabs > ul > li').attr('id');
                        var chatIdArr = getChatId.split('-');
                        currentChatId = chatIdArr[1];
                        // sets current product ID
                        var classes = $('#tool_bar_tabs > ul > li').attr('class');
                        var classArr = classes.split(' ');
                        currentProductId = classArr[0];
                        // resets the chat scroll bar	
                        var targetOffset = $(currentChat).find('.current_messages').offset().top + 100;
                        $(currentChat).find('.current_messages').animate({
                            scrollTop: targetOffset
                        }, 500);
                        startChatUpdate();
                        startMessageUpdate();
                    }
                }
                /*
				 * CORE FUNCTIONALITY END
				 */
                /*
				 * TABS START
				 */
                // Init selected tabs
                var selectedChat = '#chat_television';
                var selectedTab = $('li.first');
                
                $(selectedChat).css('display', 'block');
                $(selectedTab).addClass('current_chat');
                
                addListeners();

                /*
				 * TABS END
				 */
                /*
				 * CONTACTS START
				 */
				

				
function addSellingListeners() {
	$('#sell_popup > ul > li').click(function(){
		addCheckTab($(this));
	});
}

function addBuyingListeners(){
	$('#buy_popup > ul > li').click(function(){
		addCheckTab($(this));
	});
}

				
function addCheckTab(_this) {
	var _class = $(_this).attr("class");
	var arrClass = _class.split(" ");
	var _status = arrClass[0];					
	var _id = $(_this).attr('id');
	var checkDiv;
	var arrId = _id.split('-');
	var productId = arrId[1];
	currentProductId = productId;

	if ( productId != 0) {
		checkDiv = $('li[class*=' + productId + ']').attr('class');
	  } else {
	  $('#tool_bar_tabs > ul > li').each(function() {
	    if ($(_this).html() == $(this).find('.tab_title_hit').html()){
  		 // checkDiv = 'something';
  	    }
      });
  	}

  // CHECK IF CHAT EXISTS
  // IF THERES NOTHING
  if (checkDiv == undefined) {
    var contactId = arrId[0];
    addTab(contactId, productId, _status);
    closePopup();
    openPopup('live_chat_popup');
    // IF THERES SOMETHING
    } else {
    closePopup();
    switchCurrentChats(productId);
    openPopup('live_chat_popup');
    }
} // end function
                
                function switchCurrentChats(_pId){
                    $(currentTab).removeClass('current_chat');
                    $(currentChat).css('display', 'none');
                    
                    var tabname = '.' + _pId;
                    currentTab = $(tabname);
                    var chatname = $(tabname).attr('id');
                    var chatNameArr = chatname.split('tab_')
                    var sChat = '#chat_' + chatNameArr[1];
                    currentChat = $(sChat);
                    // need to add the chat as a parameter
                    
                    
                    $(currentTab).addClass('current_chat');
                    $(currentChat).css('display', 'block');
                    
                }
                /*
				 * CONTACTS END
				 */
                /*
				 * GENERIC FUNCTIONS START
				 */
                function liveChatTabClicked(_this){
                    $(_this).stop
                    var requestedId = $(_this).attr('id');
                    var requestedClean = requestedId.split('tab_');
                    var requestedChat = '#chat_' + requestedClean[1];
                    var requestedTab = '#' + $(_this).attr('id');
                    if (requestedChat != selectedChat) {
                        // RESET VARS
                        var getChatId = $(_this).attr('id');
                        var chatIdArr = getChatId.split('-');
                        currentChatId = chatIdArr[1];
                        
                        var classes = $(_this).attr('class');
                        var classArr = classes.split(' ');
                        currentProductId = classArr[0];
                        
                        updateStatus();
                        
                        $(currentTab).removeClass('current_chat');
                        $(_this).addClass('current_chat');
                        $(currentChat).css('display', 'none');
                        $(requestedChat).css('display', 'block');
                        currentChat = requestedChat;
                        currentTab = $(_this);
						processing = false;
						
						  var chat_a=document.getElementById("chat_a");
  chat_a.innerHTML="<iframe src='/chat/index.php?id=" + user_id + "&chan="+currentProductId+"' scrolling='no' width='600' height='300' style='overflow:hidden;' frameborder='0'>You do not have iframes.</iframe>";
//  switchCurrentChats(productId);

  switchCurrentChats(currentProductId);

//						updateChat();
                    }
                    
                }
// =========================
// ADD LISTENERS
                function addListeners(){

						$('.click_here_button').click(function(){
							addCheckTab($(this));
						})




						$('.color_picker_bidding > div > div').css('backgroundColor', textColor);
						$('.color_picker_bidding').ColorPicker({
							color: textColor,
							onShow: function (colpkr) {
								$(colpkr).fadeIn(500);
								return false;
							},
							onHide: function (colpkr) {
								$(colpkr).fadeOut(500);
								saveColorInfo();
								return false;
							},
							onChange: function (hsb, hex, rgb) {
								textColor = '#' + hex;
								$('.color_picker_bidding > div > div').css('backgroundColor', textColor);
								
							}
						});
						
						
						$('.send_button').click(function(){
							var area = $(currentChat).find('textarea');
							var trimmed = jQuery.trim($(area).val());
							
							if ( trimmed != '' || trimmed != ' '){
								sendChat(trimmed);
								$(area).val('');
							}
						});

						$('input.sell_submit').click(function(){
							var reg = /[0-9]+./g;
							var bid = $(currentChat).find('.sell_amount').val();
							var bidClean = bid.match(reg);
							if (bidClean == null){
								$(currentChat).find('.sellBid_errors > span').show();
								$(currentChat).find('.sellBid_errors > span').css('opacity',1);
								$(currentChat).find('.sellBid_errors > span').stop().animate({
									opacity:0
								}, 2500, null, function(){
									$(this).hide();
								})
							} else {
								var numberBid = '';
								for ( var i = 0; i < bidClean.length; i +=1){
									numberBid += bidClean[i]
								}
								var message = 'Are you sure you want to change the starting price to $' + numberBid;
								var con = confirm(message);
								
								if (con){
								//John Harre using chat_a to see if if(con) is used 10-9-10
								//chat_a.innerHTML='chatId:' + numberBid;
									submitStartPrice(numberBid);
									var bid = $(currentChat).find('.sell_amount').val('');
								} else {								
								}
							}
						});
						
						$('input.bid_submit').click(function(){
							var reg = /[0-9]+./g;
							var bid = $(currentChat).find('.bid_amount').val();
							var bidClean = bid.match(reg);
							if (bidClean == null ){
								$(currentChat).find('.bid_errors > span').show();
								$(currentChat).find('.bid_errors > span').css('opacity',1);
								$(currentChat).find('.bid_errors > span').stop().animate({
									opacity:0
								}, 2500, null, function(){
									$(this).hide();
								})
							} else {
								var numberBid = '';
								for ( var i = 0; i < bidClean.length; i +=1){
									numberBid += bidClean[i]
								}
								var message = 'Are you sure you want to place a bid of $' + numberBid;
								var con = confirm(message);
								
								if ( con ){
									submitBid(numberBid);
									var bid = $(currentChat).find('.bid_amount').val('');
								} else {
									
								}
								
								
							}
							
						})
						
                    $('#tool_bar_tabs > ul > li > .tab_title_hit').click(function(){
                        liveChatTabClicked($(this).parent());
                    });
					
					$('.tab_close_btn').click(function(){
						
						var chatIdRemove;
						
						if ($(this).parent().next().attr('id') != undefined ){
							
							var chatRemove = $(this).parent().attr('id');
							var chatRemoveArr = chatRemove.split('-');
							chatIdRemove = chatRemoveArr[1];
							$(currentChat).css('display', 'none');
							
							var newChatInfo = $(this).parent().next().attr('id');
							var newChatInfoClass = $(this).parent().next().attr('class');
							var currentChatArr = newChatInfo.split('tab_');
							
							var currentChatIdArr = newChatInfo.split('-');
							$(currentTab).removeClass('current_chat');
							currentChatId = currentChatIdArr[1];
							currentTab = '#' + newChatInfo;
							currentChat = '#chat_' + currentChatArr[1];
							currentProductId = newChatInfoClass;
							
							$(currentTab).addClass('current_chat');
	                        $(currentChat).css('display', 'block');

							removeChatDivs($(this).parent());
							
						} else if ($(this).parent().prev().attr('id') != undefined ){
							
							var chatRemove = $(this).parent().attr('id');
							var chatRemoveArr = chatRemove.split('-');
							chatIdRemove = chatRemoveArr[1];bar
							$(currentChat).css('display', 'none');

							var newChatInfo = $(this).parent().prev().attr('id');
							var newChatInfoClass = $(this).parent().prev().attr('class');
							var currentChatArr = newChatInfo.split('tab_');
							
							var currentChatIdArr = newChatInfo.split('-');
							
							currentChatId = currentChatIdArr[1];
							currentTab = '#' + newChatInfo;
							currentChat = '#chat_' + currentChatArr[1];
							currentProductId = newChatInfoClass;
							
							$(currentTab).addClass('current_chat');
	                        $(currentChat).css('display', 'block');
							
							removeChatDivs($(this).parent());
							
						} else {
							var chatRemove = $(this).parent().attr('id');
							var chatRemoveArr = chatRemove.split('-');
							chatIdRemove = chatRemoveArr[1];
							removeChatDivs($(this).parent());
							closePopup();
						}
						$.ajax({
                            type: 'POST',
                            url: 'gateway.php',
                            data: {
                                'action': 'remove_chat',
                                'username': name,
                                'user_id': user_id,
                                'chat_id': chatIdRemove
                            },
                            dataType: 'html',
                            success: function(data){
                                // startChatUpdate();
                            }
                        });

			  $('.chat_tab').each( function(index)
			  { 
			  //alert(index + ': ' + $(this).css('width'));
			  if($(this).css('display') == 'block'){
			  var px_width = $(this).css('width').replace('px', '')			  
			  tab_width = (Math.abs(px_width) + tab_width );
			  }
			    if(tab_width > 880){
			    $(this).css('display', 'none');
			    }
			  });
		//	  alert(tab_width);
			    if(tab_width > 880){
			    $('<div id="tab_left"> &lt; <div>').insertBefore('#current_tabs');
			    $('<div id="tab_right"> &gt; <div>').insertAfter('#current_tabs');
			    $('#current_tabs').attr('name', '0')
			    }
					});
					
				
                    
                    $(currentChat).find('textarea').click(function(){
                        $(this).keyup(function(e){
                            // alert(e.keyCode);
                            if (e.keyCode == 13) {
                                var message = jQuery.trim($(this).val());
                                sendChat(message);
                                $(this).val('')
                            }
                        });
                    })
                }


				function removeChatDivs (_target) {
					var removeChatSource = $(_target).attr('id');
					var removeChatArr = removeChatSource.split('tab_');
					removeChat = '#chat_' + removeChatArr[1];
					
					var chatInfo = removeChat.split("-");
					var chatId = chatInfo[1];

					<?php $date = date('U'); ?>
					var tabDelete = 'tab_' + <?= $date ?>;
					var chatDelete = 'chat_' + <?= $date ?>;
					var chatDeleteString = '#chat_' + <?= $date ?>;
					$(_target).attr('id', tabDelete );
					$(removeChat).attr('id', chatDelete);
					$(_target).remove();
					$(chatDeleteString).remove();
					$.ajax({
						type: 'POST',
						url: 'mc_chat_logout.php',
						data: {
							'chat_id': chatId
						}
					});
				}
				
				function resetChatTabs () {
					
					var _top = (($('#tool_bar_tabs').height()) * -1) - 10;
					$('#tool_bar_tabs').css('top', _top)
				}


				function str_replace (search, replace, subject){
					var result = "";
					var  oldi = 0;
					for (i = subject.indexOf (search); i > -1; i = subject.indexOf (search, i)){
						result += subject.substring (oldi, i);
						result += replace;
						i += search.length;
						oldi = i;
					}
					return result + subject.substring (oldi, subject.length);
				}
//---function addTab(_conId, _pId, _userStatus)------------------------b
function addTab(_conId, _pId, _userStatus) {
    if (!processing) {
        processing = true;
        $.ajax({
            type: 'POST',
            url: 'gateway.php',
            data: {
                'action': 'get_contact_product',
                'product_id': _pId,
                'contact_id': _conId,
                'user_id': user_id,
                'username': name,
                'user_status': _userStatus
            },
            dataType: 'json',
            success: function(data){

if ( data.type != undefined ){
	//ONE ON ONE CHAT
	var name = data.tab_name;
	var chatId = data.chat_id;
	var message = data.message;
	var image;
	var _tabs_ = new Array();
	var exists = false;
	$('#tool_bar_tabs > ul').children('li').each(function(){ _tabs_.push($(this).attr('id')); });

	for(var a in _tabs_){ 

	var tab_arr = _tabs_[a].substr(4).split("-");
	if(tab_arr[0] == name){ exists = true;}
	}
	if(exists == false){

        if ( data.image != '' ){	
		image = '/userImages/' + data.image;
	} else {
		image = '/images/profile-pic.gif';
	}
	var tabName = 'tab_' + name + '-' + data.chat_id;
	
	var chatName = 'chat_' + name + '-' + data.chat_id;
	
	var jChatName = '#' + chatName;
	$('#tool_bar_tabs > ul').append("<li id='" + tabName +"' class='" + data.chat_id + "'><span class='tab_title_hit'>" + name + "</span><span class='tab_close_btn'>X</span></li>");
	$('#opened_chats').append("<div id='" + chatName + "'></div>");
	$(jChatName).append("<div class='product_description'></div>");
	$(jChatName + ' > .product_description').append("<img class='product_image' src='" + image +"' height='80px' />");
	$(jChatName).append("<div class='user_area'></div>");
	$(jChatName + ' > .user_area').append("<div class='current_users'></div>");
	$(jChatName + ' > .user_area > .current_users').append('<ul></ul>');
	//$(jChatName + ' > .user_area > .current_users').append("<div class='color_picker_bidding'><div><div></div></div></div>");
	$(jChatName).append("<div id='chat_a' class='chat_area'>");
	var chat_a=document.getElementById("chat_a");
	var priv_chan = data.chat_id ;
	//  chat_a.innerHTML="<iframe src='/chat/index.php?id=" + user_id + "&chan=" + _pId + 500000000 + "' scrolling='no' width='600' height='300' style='overflow:hidden;' frameborder='0'>You do not have iframes.</iframe>";
	$(jChatName).append("<iframe src='/chat/index.php?id=" + user_id + "&chan=" + priv_chan + "' scrolling='no' width='600' height='300' style='overflow:hidden;' frameborder='0'>You do not have iframes.</iframe></div>");
	switchCurrentChats(priv_chan);
  
        $.ajax({
            type: 'POST',
            url: 'gateway.php',
            data: {
                'action': 'notify',
                'user_id': _conId,
		'type': 'join',
		'room': priv_chan,
		'con_id': user_id
            },
            dataType: 'json',
            success: function(data){}
	    });



//									$(jChatName).append('<div class="chat_area"></div>');
//									$(jChatName + ' > .chat_area').append('<iframe src ="/chat/index.php?id=' + user_id + ' width="600" height="300" style="overflow:hidden;" frameborder="0">You do not have iframes.</iframe>');
//									$(jChatName + ' > .chat_area').append("test");

//									$(jChatName + ' > .chat_area').append("<div class='current_messages'></div>");
//									$(jChatName + ' > .chat_area > .current_messages').append("<ul></ul>");
//	                                $(jChatName + ' >.chat_area').append("<div class='message_type'></div>");
//	                                $(jChatName + ' > .chat_area > .message_type').append("<textarea></textarea>");
//									$(jChatName + ' > .chat_area > .message_type').append("<div class='send_button'>SEND</div>");

	/*$(currentTab).removeClass('current_chat');
	currentTab = '#' + tabName;
	$(currentTab).addClass('current_chat');*/

	$(currentTab).removeClass('current_chat');
            $(currentChat).css('display', 'none');
            
            var tabname = '#' + tabName;
            currentTab = $(tabname);
            
            currentChat = $(jChatName);
            // need to add the chat as a parameter
            
            
            $(currentTab).addClass('current_chat');
            $(currentChat).css('display', 'block');
}																			
} else {
//BUY SELL CHAT
  var cleanTitle = data.title.replace(' ', '_');
  var cleanerTitle = cleanTitle.split(' ');

  var tabName = 'tab_' + cleanerTitle[0];
  var chatName = 'chat_' + cleanerTitle[0];

  var startPrice = data.start_price;
  var auctionEnd = data.auction_duration_end * 1000;
  var auctionType = data.auction_type;
  var _d = new Date(auctionEnd);
  var productImage
  if (data.image != undefined) {
    productImage = "<img class='product_image' height='80' src='/productImages/" + data.image + "' />";
    }
    else {
    productImage = '';
    }

  var bidAmount
  if ( data.bidamount != ''){
  	bidAmount = data.bidamount
    } else {
  	bidAmount = 'NO BIDS PLACED';
    }
  var productId = data.product_id;
  var chatId = data.chat_id;
  var jChatName = '#' + chatName + '-' + chatId;
  $('#tool_bar_tabs > ul').append("<li id='" + tabName + '-' + chatId + "' class='" + productId + "'><span class='tab_title_hit'>" + cleanerTitle[0] + "</span><span class='tab_close_btn'>X</span></li>");
  $('#opened_chats').append("<div id='" + chatName + '-' + chatId + "' class='chat_" + productId + "'></div>");
  $(jChatName).append("<div class='product_description'></div>");
  $(jChatName + ' > .product_description').append(productImage + "<p class='product_title'>" + data.title + "</p>") 
  $(jChatName + ' > .product_description').append("<p>STARTING PRICE:<br /> <span class='product_number_bids starting_price'>" + startPrice + "</span></p>");
	$(jChatName + ' > .product_description').append("<p>CURRENT BID:<br /> <span class='product_number_bids'>" + bidAmount + "</span></p>");
  $(jChatName + ' > .product_description').append("<p>TIME REMAINING:<br/><span class='time_left'></span></p>");
  $(jChatName).find('.time_left').countdown( {
  	until: _d,
  	labels : ['Y', 'M', 'W', 'D', 'H', 'M', 'S']
    })// NEEDS DYNAMIC COUNTDOWN
  if ( auctionType == 'instant') {
  	if(data.is_seller == 1) {
  	$(jChatName + ' > .product_description').append("<input type='text' class='sell_amount' /><input id='sell_" + _pId + "' type='submit' class='sell_submit' value='Change Start Price' /><span class='auction_type' style='display:none'>" + auctionType + "</span>");
     //   alert(data);
  	} else {
  		$(jChatName + ' > .product_description').append("<input type='text' class='bid_amount' /><input id='bid_" + _pId + "' type='submit' class='bid_submit' value='Place Bid' /><span class='auction_type' style='display:none'>" + auctionType + "</span>");
  	/*
  		$(jChatName + ' > .product_description').append("<form style='width:80px;' action='javascript:instantbid();' method='GET' name='bidplacemine'><input type='hidden' value='"+_pId+"' id='mypro'><input id='bidval' type='hidden' value='2' /><input id='val' type='text' value='' /><input type='submit' class='bid_submit' value='Place Bid' /></form><span id='msgmebid'></span>");
  	*/
    }


  } else {
  	$(jChatName + ' > .product_description').append("<a href='/itemDetails.php?itemid="+_pId+"' class='bid_submit'></a>");
    }
  $(jChatName).append("<div class='user_area'></div>");
  $(jChatName + ' > .user_area').append("<div class='current_users'></div>");
  $(jChatName + ' > .user_area > .current_users').append('<ul></ul>');
	$(jChatName + ' > .user_area').append("<div class='color_picker_bidding'><div><div></div></div></div>");
  $(jChatName).append("<div id='chat_a' class='chat_area'></div>");
  var chat_a=document.getElementById("chat_a");
  chat_a.innerHTML="<iframe src='/chat/index.php?id=" + user_id + "&chan="+_pId+"' scrolling='no' width='600' height='300' style='overflow:hidden;' frameborder='0'>You do not have iframes.</iframe>";
  switchCurrentChats(productId);
}


currentChatId = chatId;
           
            openPopup('live_chat_popup');
            currentChat = $(jChatName);
            processing = false;
            updateChat();
            addListeners();
            getChatContacts(chatId, jChatName);
            }
        });
        
    } 
		var tab_width = 0;
		$('.chat_tab').each( function(index)
			  { 
			  //alert(index + ': ' + $(this).css('width'));
			  if($(this).css('display') == 'block'){
			  var px_width = $(this).css('width').replace('px', '')			  
			  tab_width = (Math.abs(px_width) + tab_width );
			  }
			    if(tab_width > 880){
			    $(this).css('display', 'none');
			    }
			  });
		//	  alert(tab_width);
			    if(tab_width < 880){
			    $('#tab_right').remove();
			    $('#tab_left').remove();
			    $('#current_tabs').attr('name', '0')
			    }

} // end function
//---function addTab(_conId, _pId, _userStatus)------------------------e

				function submitBid(_bidAmount){
					var chatType = $(currentChat).find('.auction_type').html();
                        $.ajax({
                            type: 'POST',
                            url: 'gateway.php',
                            data: {
                                'action': 'submit_bid',
                                'user_id': user_id,
                                'product_id': currentProductId,
								'bid_amount' : _bidAmount,
								'username' : name,
								'chat_id' : currentChatId,
								'chat_type' : chatType
                            },
                            dataType: 'html',
                            success: function(){
					    getChatInfo();
								updateChat();
                                
                            }
                        });
                }




				function submitStartPrice(_bidAmount){
                        $.ajax({
                            type: 'POST',
                            url: 'gateway.php',
                            data: {
                              'action': 'start_price',
                              'product_id': currentProductId,
					'bid_amount' : _bidAmount,
					'chat_id' : currentChatId,
					'username' : name
                            },  
                            dataType: 'json',
                            success: function(data){  
					getChatInfo();
					updateChat();
                                
                            }
                        });
                }
		    
		    function addClickHereListener(){
			$('.click_here').click(function(){
				checkAddTab($(this));
			})    

			
		    }

				function saveColorInfo(){
                        $.ajax({
                            type: 'POST',
                            url: 'gateway.php',
                            data: {
                                'action': 'save_color_info',
                                'user_id': user_id,
                                'text_color' : textColor
                            },
                            dataType: 'html',
                            success: function(){
                                
                            }
                        });
                }
                
                function updateChat(_update){
                    if (currentChatId == undefined ) setupVars();
                    var trimValue = $(currentChat).find('.current_messages').find('ul > li').length;
					resetChatTabs();
                    if (!processing) {
                        processing = true;
                        $.ajax({
                            type: 'POST',
                            url: 'include/process.php',
                            data: {
                                'chat_action' : 'update',
								'update' : _update,
                                'username': name,
                                'user_id': user_id,
                                'chat_id': currentChatId,
                                'product_id': currentProductId,
                                'trim': trimValue
                            },
                            dataType: 'html',
                            success: function(data){
								getChatInfo();
                                if (data != '') {

                                    $(currentChat).find('.current_messages').find('ul').append(data);

                                    var targetOffset = $(currentChat).find('.current_messages').offset().top + 100;
                                    $(currentChat).find('.current_messages').animate({
                                        scrollTop: targetOffset
                                    }, 500);
                                }
                                processing = false;
                            }
                        });
                    }
                }
                
                function sendChat(message){

			var cleaned = message.replace(/['"]/g, '');

                    if (!sendingChat) {
                        sendingChat = true;
                        $.ajax({
                            type: 'POST',
                            url: 'include/process.php',
                            data: {
                                'chat_action': 'send',
                                'message': cleaned,
                                'user_id': user_id,
                                'chat_id': currentChatId,
                                'product_id': currentProductId,
								'text_color' : textColor,
                                'username': name
                            },
                            dataType: 'html',
                            success: function(data){
                                sendingChat = false;
                                updateChat();

                            }
                        });
                    }
                }
                
                function getUserInfo(_name){
                    if (!processing) {
                        processing = true;
                        $.ajax({
                            type: 'POST',
                            url: 'gateway.php',
                            data: {
                                'action': 'user_info',
                                'user_id': <?php echo $_SESSION['m_info']['user_id']; ?>,
                                'username': name
                            },
                            dataType: 'json',
                            success: function(data){
                                processing = false;
                                user_id = data.userId;
								textColor = data.chatColor;
                                getContacts();
                                
                                
                            }
                        });
                    }
                }

                function updateStatus(){
                    if (!processing) {
                        processing = true;
                        $.ajax({
                            type: 'POST',
                            url: 'gateway.php',
                            data: {
                                'action': 'update_status',
                                'user_id': <?php echo $_SESSION['m_info']['user_id']; ?>,
                                'chat_id': currentChatId
                            },
                            dataType: 'html',
                            success: function(){
                                processing = false;
                                startChatUpdate();
                            }
                        });
                        
                    }
                }

				function removeChat(){
                        $.ajax({
                            type: 'POST',
                            url: 'gateway.php',
                            data: {
                                'action': 'remove_chat',
                                'user_id': <?php echo $_SESSION['m_info']['user_id']; ?>,
                                'username' : name,
                                'chat_id': currentChatId
                            },
                            dataType: 'html',
                            success: function(data){
                            }
                        });
                }
                /*
				 * GENERIC FUNCTIONS END
				 */
                /*
				 * UPDATE FUNCTIONS START
				 */
				 
                function checkForChatUpdate(){
                    if (!processing) {
                        processing = true;
                        $.ajax({
                            type: 'POST',
                            url: 'gateway.php',
                            data: {
                                'action': 'check_for_chat_updates',
                                'user_id': <?php echo $_SESSION['m_info']['user_id']; ?>
                            },
                            dataType: 'json',
                            success: function(data){
                                processing = false;


			    
      for(var a in data.notify){
 //     alert(a);
	  if(data.notify[a].join != undefined){
	  var newtab = '';
	  newtab.id = data.notify[a].join + "-0";
	  addTab(data.notify[a].join, null , 'user_online');
//	  alert(data.notify[a].join);
	  closePopup();
	  openPopup('live_chat_popup');
	  }
      }


								if(data['chat_action'] == "logout"){
									alert("You have been logged out due to inactivity.");
									location = "?actionLogout=logout";
								} else {
									for(var i in data){
										if(data[i] != "nothing"){
											var mc_chat_id = data[i];

											if(!liveChatOpened){
												$('#toolbar_live_chat').chat_alert({
													opacity: false
												});   

//													set_live_chat_color("on");
												ping(user_id);
											} else {
												ctab = currentTab.split("-");
												if(ctab[1] != mc_chat_id){
													$("*").each(function(){
														tid = $(this).attr("id");
														if(tid.indexOf("tab_") > -1){
															tinfo = tid.split("-");
															if(tinfo[1] == mc_chat_id){
																$(this).css("background", "orange");
																ping(user_id);
															}
														}
													});
												} else {
													$(currentTab).css("background", "#2e2e2e");
												}
											}
										}
									}	
								}



                            }
                        });
                    }
                }
                
				function set_live_chat_color(n){
					if(n == "off"){
						$(".live_chat_text").css("color", "white");
					} else {
						$(".live_chat_text").css("color", "orange");
					}
				}
		    
                function updateChatStatus(){
                    $.ajax({
                        type: 'POST',
                        url: 'gateway.php',
                        data: {
                            'action': 'update_chat_status',
                            'user_id': user_id
                        },
                        dataType: 'html',
                        success: function(data){
                        }
                    });
            }
		    
function getChatInfo() {
var chatToUpdate = currentChat;
var chatType = $(currentChat).find('.auction_type').html();              
  $.ajax({
      type: 'POST',
      url: 'gateway.php',
      data: {
          'action': 'get_chat_info',
          'user_id': <?php echo $_SESSION['m_info']['user_id']; ?>,
          'chat_id': currentChatId,
          'product_id': currentProductId,
          'username': name
      },
      dataType: 'json',
      success: function(data) {
	   // reset time




			$(currentChat).find('.starting_price').html(data.start_price);
			$(chatToUpdate).find('.time_left').countdown('destroy'); 
				if (data.time != 'expired'){
					// $(chatToUpdate).find('.time_left').countdown('destroy');
	  				var timeLeft = data.time * 1000;
	  				var _date = new Date(timeLeft);
	  				$(chatToUpdate).find('.time_left').countdown({
						until : _date,
						labels : ['Y', 'M', 'W', 'D', 'H', 'M', 'S']
					});
				} else {
					$(chatToUpdate).find('.time_left').html('<p style="color:#FF0000; font-size:px;">AUCTION HAS BEEN COMPLETED</p>')
				}
	  // reset users
	    if(data.bid != null || data.type == 'Traditional'|| data.time == 'expired' )
	      {
	      $(chatToUpdate).find('.chat-changetxt').css('display', 'none');
	      $(chatToUpdate).find('#changeme').css('display', 'none');
	      $(chatToUpdate).find('.chat-bidinput').css('display', 'none');
	      $(chatToUpdate).find('.time_left').html('<p style="color:#FF0000; font-size:px;">AUCTION HAS BEEN COMPLETED</p>');
	      var iframe =  $(chatToUpdate).find('iframe')
	      iframe.contents().find('#inputFieldContainer').css('display', 'none');
	      }


	  $(chatToUpdate).find('.current_users > ul').html(data.users);
	  var bidAmount = (data.bid == null)?'NO BIDS PLACED' : "$"+  data.bid;
	  $(chatToUpdate).find('.current_bid').html(bidAmount);
	  if(data.owner == <?php echo $_SESSION['m_info']['user_id']; ?>)
	    {
	    if(data.bid != null || data.type == 'Traditional'|| data.time == 'expired' )
	      {
	      $(chatToUpdate).find('.chat-changetxt').css('display', 'none');
	      $(chatToUpdate).find('#changeme').css('display', 'none');
	      }
	    }
	  else
	    {
	    $(chatToUpdate).find('.display_starting_price').css('display', 'none');    
	    }

	    $('.username').click(function(){
                       addCheckTab($(this));  
	     })
      }
  });
} // end function

/////////////////////////Check for chat/message updates
                function startChatUpdate(){
                    chatTimeout = setInterval(checkForChatUpdate, 15000);
                }
                
                function startMessageUpdate(){
                   messageTimeout = setInterval(updateChat, 5000);
					
                }                
 /////////////////////////                 
                // liveTabAlert();
                /*
				 * UPDATE FUNCTIONS END
				 */
                /*
				 * $('#input_text').keyup(function(e) { if(e.keyCode == 13) {
				 * alert('Enter key was pressed.'); } });
				 */
                /*
				 * DELETABLE METHODS
				 */
				
				function getContacts(_update){
                        $.ajax({
                            type: 'POST',
                            url: 'gateway.php',
                            data: {
                                'action': 'get_contacts',
                                'user_id': <?php echo $_SESSION['m_info']['user_id']; ?>
                            },
                            dataType: 'html',
                            success: function(data){ 
                                $('#contacts_popup').html(data);

                    $('.user_online').click(function(){
                       addCheckTab($(this));
		      });

 
								if (_update != true )getChats();
                            }
                        });
                }

function getChatContacts(_chatId, _jChatName) {
  var user_list;
  $.ajax( {
    type: 'POST',
    url: 'gateway.php',
    data: {
      'action': 'get_chat_contacts',
      'chat_id': _chatId
    },
    dataType: 'html',
    success: function(data) {
      processing = false;
      user_list = data;
      $(_jChatName + ' > .user_area > .current_users > ul').append(user_list);
    }
  });
}

   
                function getChatTabs(){
                    $.ajax({
                        type: 'POST',
                        url: 'gateway.php',
                        data: {
                            'action': 'get_chat_tabs',
                            'user_id': user_id
                        },
                        dataType: 'html',
                        success: function(data){
                            processing = false;
                            var user_list = data;
                            $('#tool_bar_tabs > ul').append(user_list);
							/*
							 * $('#tool_bar_tabs > ul > li').each(function(){ })
							 */
							getSellingProducts();
							getBuyingProducts();
							addListeners();
							setupVars();
							var tab_width = 0;
		$('.chat_tab').each( function(index)
			  { 
			  //alert(index + ': ' + $(this).css('width'));
			  if($(this).css('display') == 'block'){
			  var px_width = $(this).css('width').replace('px', '')			  
			  tab_width = (Math.abs(px_width) + tab_width );
			  }
			    if(tab_width > 880){
			    $(this).css('display', 'none');
			    }
			  });
		//	  alert(tab_width);
			    if(tab_width > 880){
			    $('<div id="tab_left"> &lt; <div>').insertBefore('#current_tabs');
			    $('<div id="tab_right"> &gt; <div>').insertAfter('#current_tabs');
			    $('#current_tabs').attr('name', '0')
			    }
var tab_width = 0;
			  		$('#tab_left').click(function(){
		var current_index = (Math.abs($('#current_tabs').attr('name')) - 1); 
		tab_width = 0;
		$('.chat_tab').each( function(index)
			  { 
			  if($('#current_tabs').attr('name') != '0' && $('#current_tabs').attr('class') !=  'pause_left'){
			  if(index < current_index){ $(this).css('display', 'none');}
			  else{
			  if($(this).css('display') == 'block'){ 
 			  var px_width = $(this).css('width').replace('px', '');			  
			  tab_width = (Math.abs(px_width) + tab_width );
			    if(tab_width > 880){
			    $(this).css('display', 'none');
			    }
			    else{ 
			    $(this).css('display', 'block' );
			    }
			  }
			  else{
			    if(tab_width > 880){
			    $(this).css('display', 'none');
			    }
			    else{ 
			    $(this).css('display', 'block' );
			    var px_width = $(this).css('width').replace('px', '');			  
			    tab_width = (Math.abs(px_width) + tab_width );
			    if(tab_width > 880){
			    $(this).css('display', 'none');
			      }
			    }
			   }
			  }
			  }
		  
			  });
			  if($('#current_tabs').attr('name') == '0' || $('#current_tabs').attr('name') == '-1'){ $('#current_tabs').addClass('pause_left'); }
			  else{ $('#current_tabs').attr('class', '');		$('#current_tabs').attr('name', current_index);	 }
			  $('#current_tabs').removeClass('pause_right');
		});

		$('#tab_right').click(function(){
		var pause_right = '';
		if($('#current_tabs').attr('class') != 'pause_right'){
		var current_index = (Math.abs($('#current_tabs').attr('name')) + 1); 
		tab_width = 0;
		$('.chat_tab').each( function(index)
			  { 
			  if(index < current_index){ $(this).css('display', 'none');}
			  else{
			  if($(this).css('display') == 'block'){ 
 			  var px_width = $(this).css('width').replace('px', '');			  
			  tab_width = (Math.abs(px_width) + tab_width );
			    if(tab_width > 880){
			    $(this).css('display', 'none');
			    }
			    else{ 
			    $(this).css('display', 'block' );
			    

			    
			    }
			  }
			  else{
			    if(tab_width > 880){
			    $(this).css('display', 'none');
			    pause_right = 'no';
			      }
			    else{ 
			    $(this).css('display', 'block' );
			    var px_width = $(this).css('width').replace('px', '');			  
			    tab_width = (Math.abs(px_width) + tab_width );
			    if(tab_width > 880){
			    $(this).css('display', 'none');
			    }
			    pause_right = 'yes';
			    }
			   }
			  }
			  });
		$('#current_tabs').attr('name', current_index);
		if(tab_width < 880 || pause_right == 'yes'){  $('#current_tabs').addClass('pause_right');}
		else{ $('#current_tabs').attr('class', '');}
		$('#current_tabs').removeClass('pause_left');
		}});


                        }
                    });
                }
                
                
                
                function getChats(){
                    $.ajax({
                        type: 'POST',
                        url: 'gateway.php',
                        data: {
                            'action': 'get_chats',
                            'user_id': user_id
                        },
                        dataType: 'json',
                        success: function(data){
							
                            processing = false;
							$('#opened_chats').append(data.html);
							var tl = data.timeleft;
							for ( var i in tl){
								var id = '#'+tl[i][0];
								var timestamp = tl[i][1] * 1000;
								var dt = new Date(timestamp);
							
								$(id).find('.time_left').countdown({
									until: dt,
									labels : ['Y', 'M', 'W', 'D', 'H', 'M', 'S']
								});
							}
                            
							getChatTabs();
                        }
                    });
                }
                
				function getSellingProducts(){
					$("#sell_popup > ul").empty();
					$.ajax({
                        type: 'POST',
                        url: 'gateway.php',
                        data: {
                            'action': 'get_products_selling',
                            'user_id': <?php echo $_SESSION['m_info']['user_id']; ?>
                        },
                        dataType: 'html',
                        success: function(data){ 
							$('#sell_popup > ul').append(data);
							addSellingListeners();
                        }
                    });
				}
				
				function getBuyingProducts(){
					$.ajax({
                        type: 'POST',
                        url: 'gateway.php',
                        data: {
                            'action': 'get_products_buying',
                            'user_id': <?php echo $_SESSION['m_info']['user_id']; ?>
                        },
                        dataType: 'html',
                        success: function(data){ 
							$('#buy_popup > ul').append(data);
							addBuyingListeners();
                        }
                    });
				}
	   

		
		startChatUpdate();
                startMessageUpdate();

            });            
</script>
<?
//======================================================================================================================
// CHAT_MAIN_JS.PHP module
//======================================================================================================================
?>