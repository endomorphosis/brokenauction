function emailvalidation(enteredd, alertbox)
{


// E-mail-Validation (c) Henrik Petersen / NetKontoret


// Explained at www.netkontoret.dk/jsforms.htm


// Please do not remove the this line and the two lines above.

 fobj=document.getElementById(enteredd);


apos=fobj.value.indexOf("@");


dotpos=fobj.value.lastIndexOf(".");


lastpos=fobj.value.length-1;


if (apos<1 || dotpos-apos<2 || lastpos-dotpos>4 || lastpos-dotpos<2) 


{if (alertbox) {alert(alertbox);} return false;}


else {return true;}




}





function valuevalidation(entered, min, max, alertbox, datatype)


{


// Value-Validation (c) Henrik Petersen / NetKontoret


// Explained at www.netkontoret.dk/jsforms.htm


// Please do not remove the this line and the two lines above.


with (entered)


{


checkvalue=parseFloat(value);


if (datatype)


  {smalldatatype=datatype.toLowerCase();


   if (smalldatatype.charAt(0)=="i") {checkvalue=parseInt(value)};


  }


if ((parseFloat(min)==min && checkvalue<min) || (parseFloat(max)==max && checkvalue>max) || value!=checkvalue)


{if (alertbox!="") {alert(alertbox);} return false;}


else {return true;}


}


}





function digitvalidation(entered, min, max, alertbox, datatype)


{


// Digit-Validation (c) Henrik Petersen / NetKontoret
// Explained at www.netkontoret.dk/jsforms.htm
// Please do not remove the this line and the two lines above.
with (entered)
{
	checkvalue=parseFloat(value);
	if (datatype)
	  {smalldatatype=datatype.toLowerCase();
   if (smalldatatype.charAt(0)=="i") {checkvalue=parseInt(value); if (value.indexOf(".")!=-1) {checkvalue=checkvalue+1}};
  }
if ((parseFloat(min)==min && value.length<min) || (parseFloat(max)==max && value.length>max) || value!=checkvalue)
{if (alertbox!="") {alert(alertbox);} return false;}
else {return true;}
}


}

function strtrim(text) {
//Match all spaces or tabs at beginning and end of text and replace
//with null strings
/* \s Matches a "whitespace" character. Depending on the regular expression engine, this can include carriage returns,
 line feeds, horizontal tabs and vertical tabs, as well as spaces. Note that this is case-sensitive: 
 '\S' matches non-whitespace characters
*/
return text.replace(/^\s+/,'').replace(/\s+$/,'');
}




function emptyvalidation(entered, alertbox)
{
// Emptyfield-Validation (c) Henrik Petersen / NetKontoret
// Explained at www.netkontoret.dk/jsforms.htm
// Please do not remove the this line and the two lines above.
var fobj=document.getElementById(entered);

if (fobj.value==null || fobj.value=="" || strtrim(fobj.value)=="")
{if (alertbox!="") {alert(alertbox);} return false;}
else {return true;}
}


function emptytextareavalidation(entered, alertbox)
{
	with (entered)
	{
		if (innerText==null || innerText=="" || strtrim(innerText)=="")
		{
			if (alertbox!="") {alert(alertbox);} 
			return false;
		}
		else {return true;}
	}
}




function checkboxvalidation(entered, alertbox)



{



with (entered)



{



if (checked)

{return true;}

else

{if (alertbox!="") {alert(alertbox);} return false;}



}



}



function lengthvalidation(entered, min, max, alertbox)


{


// Digit-Validation (c) Henrik Petersen / NetKontoret


// Explained at www.netkontoret.dk/jsforms.htm


// Please do not remove the this line and the two lines above.


with (entered)


{


if ((value.length<min) || (value.length>max))


{if (alertbox!="") {alert(alertbox);} return false;}


else {return true;}


}


}





function detectbrowser()


{


Browsername=navigator.appName;       





browser="0";


if (navigator.appVersion.indexOf("2.0")) {browser="2"};


if (navigator.appVersion.indexOf("3.0")) {browser="3"};


if (navigator.appVersion.indexOf("4.0")) {browser="4"};      





}





function confirmaction()


{


    if(confirm('Are you sure you want to proceed?'))


    {


        document.form.submit();


    }


}

function alfanumericvalidation(entered,alertbox)
{
	str=entered.value;
	reg=/^([a-zA-Z0-9_\-]+)$/;
	if (!reg.test(str))
	{
		alert(alertbox);
		 return false;
	}
	else {return true;}
}

function dateformatvalidation(entered)
{
	str=entered;
	//str like "yyyy-mm-dd hh:MM:ss"
	reg=/^\d{4,4}-\d{2,2}-\d{2,2} \d{2,2}:\d{2,2}:\d{2,2}$/;
	if ( !reg.test(entered) ) {
		alert ("You have to type the right format: yyyy-mm-dd HH:MM:SS");
		return false;
	}
	reg2=/-| |:/g;
	valori=entered.split(reg2);
	year=new Number(valori[0]);
	month=new Number(valori[1]);
	day=new Number(valori[2]);
	hour=new Number(valori[3]);
	minute=new Number(valori[4]);
	second=new Number(valori[5]);
	if (intvalidation(year, 1900, 3000, "You haven't typed a valid time of expiring (year)!")==false) return false;
	if (intvalidation(month, 1, 12, "You haven't typed a valid time of expiring (month)!")==false) return false;
	if (intvalidation(day, 1, 31, "You haven't typed a valid time of expiring (day)!")==false) return false;
	if (intvalidation(hour, 0, 23, "You haven't typed a valid time of expiring (hour)!")==false) return false;
	if (intvalidation(minute, 0, 59, "You haven't typed a valid time of expiring (minute)!")==false) return false;
	if (intvalidation(second, 0, 59, "You haven't typed a valid time of expiring (second)!")==false) return false;
	//verify if the day in month is correct (eg: February)
	data = new Date(year, month-1);//, date, hour, minute, second) )
	data.setDate(day);
	if ( month!=data.getMonth()+1)
	{
		alert("You haven't typed the a valid time of expiring (day)!");
		return false;
	}
	return true;
}

function intvalidation( number, minval, maxval, errormessage)
{
	if ( number<minval || number>maxval ) {
		alert( errormessage);
		return false;
	};
	return true;
}


function getRadioValue(radio) 
{
	if (typeof(radio)=="undefined" || radio==null)
		return null;
	if (radio.length>=1)
	{
		for (index=0; index<radio.length; index++)
		{ 
			if (radio[index].checked) 
				return radio[index].value; 
		}
		return null;
	}
	else
	{
		if (radio.checked) 
			return radio.value; 
		return null;
	}
}
