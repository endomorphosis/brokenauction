<? 
  include('header.php');
  $contentsql="select * from content where `contentid`='15' limit 1";
  $content=$cms->getresults($contentsql);
?>
<div id="main-column">	 		
  <div class="clear">&nbsp;
  </div>		 		
  <div>			
    <div class="h3">				
      <div style="float:left; color:#dc983a;">Seller's Agreement
      </div>	
      <div style="float:right; padding-right:10px;">	
      </div>			
    </div>			     
    <div style="padding:14px 10px; position:relative;">        
      <div class="user-register">            
        <div>
          <span>
            <label style="width:700px; font:14px Arial, Helvetica, sans-serif; color:#fff; display:block; padding:10px 80px;">		  
              <span style="display:block; width:100%; text-align:center; font:bold 20px Arial, Helvetica, sans-serif; color:#00ff00;">SELLER�S AGREEMENT
              </span>		  <br /><br />
              <p>This Seller&#8217;s Agreement (the &#8220;Agreement&#8221;) is a binding contract    between you and Broken Buy, LLC, a Oregon limited liability company and applies    to your use of our online classified and auction services (the &#8220;Services&#8221;)    as posted on our website, www.brokenbuy.com (the Website&#8221;). The terms    &#8220;you&#8221; and &#8220;your&#8221; shall mean you the seller and the terms    &#8220;we&#8221;, &#8220;our&#8221;, &#8220;us&#8221; or &#8220;Broken Buy&#8221;    shall mean Broken Buy, LLC.
              </p>
              <p>1. Acknowledgement - You hereby acknowledge that by listing any goods or services    for sale on our Website, you agree to be bound by the terms of this Agreement.    You further acknowledge that you shall be obligated, to the fullest extent of    the law, to sell and deliver any goods or services that you sell through our    Website to the buyer pursuant to the terms of the sale (i.e. item as listed,    the sale price and the delivery method). If you wish to cancel a sale, you may    do so only with the express consent of the buyer. For the purpose of this Agreement    &#8220;buyer&#8221; means the person submitting the highest bid at the close    of your auction.
              </p>
              <p>2. Representations and Warranties - 
              </p>
              <blockquote>  
                <p> 2.1 As to Goods - You hereby represent and warrant that: 
                </p>  
                <blockquote>    
                  <p> 2.1.1 You are the lawful owner of any goods that you list on our Website,        or that you otherwise have permission from the lawful owner to do so.
                  </p>    
                  <p> 2.1.2 The goods are not prohibited under our Terms of Use.
                  </p>  
                </blockquote>  
                <p> 2.2 As to Services - You hereby represent and warrant that: 
                </p>  
                <blockquote>    
                  <p> 2.2.1 You possess all licenses necessary, if any, to perform the service        in the jurisdiction where the service is to be performed.
                  </p>    
                  <p> 2.2.2 The service is not prohibited under our Terms of Use.
                  </p>  
                </blockquote>
              </blockquote>
              <p>3. Fees - You agree to pay all fees for any auction enhancements (i.e. Featured    Product Package, Highlight Package, Bold Package, Border Package, etc. that    you elect to purchase as more fully described on our 
                <a href="/begin1.php">&#8220;How    To&#8221; page</a>. Fees for any auction enhancements shall be due and payable    at the time such auction enhancements are purchased.
              </p>
              <p>4. Invoices - On or about the fifteenth (15th) day of each month we will send    you an invoice containing details for the last payment made, discounts, credits,    and fees that were applied to your account during the preceding month. All invoices    will be sent to your email address as it appears in our records. IT IS YOUR    RESPONSIBILITY TO NOTIFY US OF ANY CHANGE IN YOUR EMAIL ADDRESS. Notice - Any    notice required or permitted under this Agreement shall be made by electronic    transmission. 
              </p>
              <blockquote>  
                <p>4.1 Notice to You - Notice to you shall be sent to your email or other electronic      address as it appears in our records. IT IS YOUR RESPONSIBILITY TO NOTIFY      US OF ANY CHANGE IN YOUR EMAIL ADDRESS 
                </p>  
                <p>4.2 Notice to Us - Notice to us shall be sent to 
                  <a href="mailto:contact@brokenbuy.com">contact@brokenbuy.com</a>
                </p>
              </blockquote>
              <p>5. Non-limitation of Remedies - Nothing in this Agreement shall limit our rights    and remedies at law or equity.
              </p>
              <p>6. Entire Agreement - This Agreement contains the entire agreement between    you and us.
              </p>
              <p>7. Choice of Law and Consent to Jurisdiction - This Agreement shall be construed    and enforced under the laws of the State of Oregon without regard to any conflict    or choice of law rule.&nbsp; By using the Broken Buy Website and our services,    you hereby consent to the jurisdiction and venue of the courts of Portland, Oregon, or the United States District Court for the District of Oregon    (Northern Division), if diversity of citizenship exists, in any action relating    to this Agreement.&nbsp; You agree to stipulate in any future proceeding that    this Agreement is to be considered for all purposes to have been executed and    delivered within the State of Oregon. 
              </p>
              <p>8. Severability Clause - In case any provision (or any part of any provision)    contained in this Agreement shall for any reason be held to be invalid, illegal    or unenforceable in any respect, such invalidity, illegality or unenforceability    shall not affect any other provision (or remaining part of the affected provision)    of this Agreement, but this Agreement shall be construed as if such invalid,    illegal or unenforceable provision (or part thereof) had never been contained    herein but only to the extent it is invalid, illegal or unenforceable.
              </p>
              <p>9. Headings - The headings of the various sections of this Agreement have been    inserted for convenient reference only and shall not in any manner be construed    as modifying, amending or affecting the express terms and provisions hereof.  
              </p>
              <p>10. Benefit - The terms and conditions herein shall inure to the benefit of    and shall be binding upon you and us and each of our respective heirs, personal    representatives, successors and assigns, as the case may be.
              </p>
              <p>11. Compliance - You must agree to comply with all of the terms and conditions    contained in this Agreement. Failure to do so may result in the termination    of your account. In addition, you must read and agree to comply with the terms    and conditions contained on our 
                <a href="/terms%20of%20use.php" style="text-decoration: link;">Terms of Use</a>    and 
                <a href="/privacy%20policy.php">Privacy Policy</a>.
              </p>
              <p>12. Amendment - We reserve the right to amend this Agreement at any time by    posting the amended Agreement on our Website. THE AMENDED AGREEMENT WILL BE    EFFECTIVE IMMEDIATELY UPON POSTING; HOWEVER, IF THE AMENDED AGREEMENT INCLUDES    A SUBSTANTIAL CHANGE, INCLUDING BUT NOT LIMITED TO AN INCREASE IN FEES, WE WILL    PROVIDE YOU WITH FIFTEEN (15) DAYS&#8217; PRIOR NOTICE.
              </p>
              <p>This Agreement was last modified on May 1, 2011.
              </p>
            </label>
          </span>
        </div>      
      </div>    
    </div>		
  </div>	
</div>
<? //John Harre 9/13/2010 - disabled due to FF bombing. The file is backed up and all contents have been cleared out.
  include('footer.php');?>