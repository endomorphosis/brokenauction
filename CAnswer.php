<? 
  $localpath=$_SERVER['DOCUMENT_ROOT'];
  session_start();
  include($localpath.'/inc/cms.php');
?>
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <?
  $cms->Script_GetOBJ();
  $cms->initAjaxFunction();
  ?>
  <script language="javascript">
  loginsubmit=function()
    {
    document.loginform.question.value=GetOBJ('question').value;
    document.loginform.answer.value=GetOBJ('answer').value;
    document.loginform.submit();
    }
  </script>
    </head>
    <body>
      <form name="loginform" method="post" style="display:none" action="">
        <input type="hidden" name="question" />
        <input type="hidden" name="answer" />
        <input type="hidden" name="achange" value="1" />
      </form>
      <div >	 		
        <div class="clear">&nbsp;
        </div>		 		
        <div>			 		
          <div style="padding:0px 0px; position:relative; padding-left:0px;">		
            <div style="width:300px; float:left;">		 		
              <div style="font:bold 12px Arial; color:#FF6600; clear:both; padding-left:0px;">
                <?=$msg?>
              </div>		
              <div style="padding: 0px 0px 0px;">		 			
                <div class="window-item" style="color:#000000;">Question
                </div>			
                <div>			 			
                  <select name="question" id="question" >
                    <option>-- select --
                    </option>
                    <option <? if($User['question']=="What street did you grow up on?")echo "selected"; ?>>																What street did you grow up on?
                    </option>
                    <option <? if($User['question']=="What was your favorite place to visit as a child?")echo "selected"; ?>>What was your favorite place to visit as a child?
                    </option>
                    <option <? if($User['question']=="What is the first name of your first boyfriend or girlfriend?")echo "selected"; ?>>What is the first name of your first boyfriend or girlfriend?
                    </option>
                    <option <? if($User['question']=="Which phone number do you remember most from your childhood?")echo "selected"; ?>>Which phone number do you remember most from your childhood?
                    </option>
                    <option <? if($User['question']=="Who is your favorite actor, musician or artist?")echo "selected"; ?>>Who is your favorite actor, musician or artist?
                    </option>
                  </select>			 			
                </div>			
                <div class="clear">
                </div>			 			
                <div class="window-item" style="color:#000000;">Answer
                </div>			
                <div>
                  <input name="answer" id="answer"  type="text" class="win-input" />
                </div>			
                <div class="clear">
                </div>			 			 			 			
                <div>
                  <span>
                    <input name="submit" type="button" value="submit"  onclick="javascript:loginsubmit()" />
                  </span>
                </div>			
                <div class="clear">
                </div>			 			 	 			 		
              </div>	
            </div>	    
          </div>		
        </div>	
      </div>
  </body>
  </html>