<? 
  $localpath=$_SERVER['DOCUMENT_ROOT'];
  session_start();
  include($localpath.'/inc/cms.php');
?>
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
  <?
    $cms->Script_GetOBJ();
    $cms->initAjaxFunction();
  ?>
  <script language="javascript">
  loginsubmit=function(){
  document.loginform.oldpass.value=GetOBJ('oldpass').value;
  document.loginform.newpass.value=GetOBJ('newpass').value;
  var npass=GetOBJ('newpass').value;
  var opass=GetOBJ('cnewpass').value;
  if(npass!=opass){
  	alert('password missmatch');
  }
  else
    {
    document.loginform.submit();
    }
  }
  </script>
  </head>
  <body>
  <form name="loginform" method="post" style="display:none" action="">
    <input type="hidden" name="oldpass" />
    <input type="hidden" name="newpass" />
    <input type="hidden" name="pchange" value="1" />
  </form>
  <div>	 		
    <div class="clear">&nbsp;
    </div>		 		
    <div>			 		
      <div style="padding:0px 0px; position:relative; padding-left:0px;">		
        <div style="width:300px; float:left;">		 		
          <div style="font:bold 12px Arial; color:#FF6600; clear:both; padding-left:0px;">
            <?=$msg?>
          </div>		
          <div style="padding: 0px 0px 0px;">		 			
            <div class="window-item" style="color:#000000;">Old Password
            </div>			
            <div>
              <input name="oldpass" id="oldpass"  type="password" class="win-input" />
            </div>			
            <div class="clear">
            </div>			 			
            <div class="window-item" style="color:#000000;">New Password
            </div>			
            <div>
              <input name="newpass" id="newpass"  type="password" class="win-input" />
            </div>			
            <div class="clear">
            </div>			 			
            <div class="window-item" style="color:#000000;">Confirm New Password
            </div>			
            <div>
              <input name="cnewpass" id="cnewpass"  type="password" class="win-input" />
            </div>			 			
            <div style="clear:both;"><br />
            </div>			 			
            <div>
              <span >
                <input name="submit" type="button" value="submit"  onclick="javascript:loginsubmit()" />
              </span>
            </div>			
            <div class="clear">
            </div>				
          </div>	
        </div>	    
      </div>		
    </div>	
  </div>
  </body>
  </html>