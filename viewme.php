<? 
  include('header.php');
?>
  <ul class="breadcrumb">  </ul>
  <? 
    $feddbacklist=$cms->getFeedback($_GET['id'],false); // not archieved
    $FeedCP=$cms->getFeedbackPercent($_GET['id']);
    $feddbacklistResp=$cms->getResponse($_GET['id'],'false');
  ?>
  <div id="center-column" class="single" style="width:100%;"> 
    <div style="padding:5px; font: bold 13px Arial; border:1px solid #ff8e2f; background: #ff8e2f url(/images/inner-title-bg.gif) repeat-x top;">Feedback Center
    </div>  
    <div style="clear:both;  padding:10px;">    
      <div class="subtitle" style="padding-top:10px; color:#FF0000;">	
        <?=$_REQUEST['msg'] ?>	
      </div>    
      <div id="table-row" style="width:99%; ">      
        <div class="med1">Positive Feedback
        </div>      
        <div class="big">
          <? echo  $FeedCP['positivepercentage']."%"; ?>	     	  
        </div>      
        <div class="td-clear">
        </div>    
      </div>    
      <div id="table-row" style="width:99%;">      
        <div class="med1" >Total Number Of Feedback: 
        </div>      
        <div class="big" >[ 
          <? echo  $FeedCP['totalfeedback']; ?> ]
        </div>      
        <div class="big">        
          <!--Archived Feedback-->      
        </div>      
        <div class="td-clear">
        </div>    
      </div>    <br style="clear:both;" />    <br style="clear:both;" />    
      <div id="table-row" style="background-color:#99CC00; width:99%; padding:10px;">     
        <div class="small" style="width:150px;"><b>Auction Name</b>
        </div>      
        <div class="med1"><b>From</b>
        </div>      
        <div class="med"><b>Recommend</b>
        </div>      
        <div class="med1" style="width:250px; overflow:hidden;"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Feedback</b>
        </div>      
        <div class="med"><b>Date / Time</b>
        </div>      
        <div class="med" ><b>Rate</b>
        </div>      
        <div class="td-clear">
        </div>    
      </div>    
      <? 
        if(count($feddbacklist)>0){
    		foreach($feddbacklist as $val){
  		?>    
      <div id="table-row" style="width:99%;  padding:10px;">        
        <div class="small" style="width:150px;">        
          <?=$val['title']?>      
        </div>      
        <div class="med1">        
          <?=$val['from_name']?>      
        </div>      
        <div class="med">        
          <?=$val['class']?>      
        </div>      
        <div class="med1" style="width:250px; overflow:hidden;">        <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>        
          <?=$val['feedback']?>        
        </div>      
        <div class="med">        
          <?= date("j, n, Y", $val['datetime'])?>      
        </div>      
        <div class="small">        
        <? 
      		if($val[rate] == 0)
      		echo '<img src="/images/palm-0.gif">';
      		else if($val['rate'] ==1)
      		echo '<img src="/images/palm-1.gif">';
      		else if($val['rate'] ==2)
      		echo '<img src="/images/palm-2.gif">';
      		else if($val['rate'] ==3)
      		echo '<img src="/images/palm-3.gif">';
      		else if($val['rate'] ==4)
      		echo '<img src="/images/palm-4.gif">';
      		else if($val['rate'] ==5)
      		echo '<img src="/images/palm-5.gif">';				
        ?>      
        </div>    
      </div>    
      <?  }} ?>    
      <br style="clear:both;" />    
      <br style="clear:both;" />      
    </div>  
    <? if(count($feddbacklistResp)>0){?>  
    <!-- rsponds  -->  
    <div style="clear:both; display:none;">    
      <div class="subtitle" style="padding-top:10px;">
      </div>    
      <div id="table-row">      
        <div class="med1">Response
        </div>      
        <div class="big">[ 
          <? echo  count($feddbacklistResp); ?> ]
        </div>      
        <div class="big">
        </div>      
        <div class="td-clear">
        </div>    
      </div>    
      <br style="clear:both;" />    
      <br style="clear:both;" />    
      <div id="table-row">      >      
        <div class="small">Auction ID
        </div>      
        <div class="med1">From
        </div>      
        <div class="med">Classification
        </div>      
        <div class="med1">Feedback
        </div>      
        <div class="med">D/T
        </div>      
        <div class="med1">Resp. Comment
        </div>      
        <div class="small">R. D/T
        </div>         
        <div class="td-clear">
        </div>    
      </div>    
      <? 
        foreach($feddbacklistResp as $val){
      ?>    
      <div id="table-row">   
        <div class="small">        
          <?=$val['auction_id']?>      
        </div>      
        <div class="med1">        
          <?=$from?>      
        </div>      
        <div class="med">        
          <?=$val['class']?>      
        </div>      
        <div class="med1">          
          <?=substr($val['feedback'],0,50)?>       
        </div>      
        <div class="med">        
          <?=$val['datetime']?>      
        </div>      
        <div class="med1">        
          <?=substr($val['response_text'],0,20)?>      
        </div>      
        <div class="small">        
          <?= date("F j, Y ",$val['datetime_r']) ?>      
        </div>        
      </div>    
      <?  }?>    
      <br style="clear:both;" />    
      <br style="clear:both;" />    
      <div id="table-row">      
        <div class="med1">&nbsp;
        </div>      
        <div class="small">&nbsp;
        </div>      
        <div class="med1">&nbsp;
        </div>      
        <div class="med">&nbsp;
        </div>      
        <div class="med1">&nbsp;
        </div>      
        <div class="med">        
          <!--Archive -->      
        </div>      
        <div class="small">        
          <!--Respond -->      
        </div>    
      </div>  
    </div>  
    <!--end responds -->    
    <?  } ?>	 	
    <div class="med">
      <a href="#" onClick="history.go(-1)">Back</a>
    </div>   
  </div>
<? //John Harre 9/13/2010 - disabled due to FF bombing. The file is backed up and all contents have been cleared out.
  include('footer.php');?>