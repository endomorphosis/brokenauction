<? 
  include('header.php');
?>
  <ul class="breadcrumb"></ul>
<? 
  include('leftLinks.php');
  if(!isset($_POST['themsgid'])) 
  $_POST['themsgid']=0;
  if($_POST['themsgid']>0){
  if($_POST['theaction']=='del')
  $cms->DeleteMessage($_POST['themsgid']);
  if($_POST['theaction']=='archive')
  $cms->SetMessageArchive($_POST['themsgid']);
  }
  ## Batch Operation
  if(!isset($_POST['batchOperatin'])) $_POST['batchOperatin']=0;
  if(!isset($_POST['msgids'])) $_POST['msgids']=array();
  $action=$_POST['batchOperatin'];
  $act_ids=$_POST['msgids'];
  if(count($act_ids)>0){
    if($action=="archive"){
  		foreach($act_ids as $val){
  		$cms->SetMessageArchive($val);
  		}
		}
		if($action=="delete"){
      foreach($act_ids as $val){
			$cms->DeleteMessage($val);
			}
		}
  }
  $recmsglist=$cms->receivedMessage($cms->loggedUserId);
  $recmsglist=$recmsglist['lists'];
?>
  <script language="javascript">
  function checkedAll (id, checked){
  var el = document.getElementById(id);
  for (var i = 0; i < el.elements.length; i++){
    el.elements[i].checked = checked;
  }
  }
  </script>
  <div id="center-column" class="single">  
    <div style="font: bold 14px Arial; ">Inbox  
    </div>    
    <div style="clear:both;">    
      <div class="subtitle" style="font:bold 14px Arial; padding-top:20px; padding-bottom:5px;">        
      </div>    
      <div id="table-row" >      
        <div style="margin-left:0px;">              
          <div class="table_header" >          
            <div class="med" style="width:150px; font-size:11px; color:#000; font-weight:bold;">From           
            </div>          
            <div class="big" style="width:330px; font-size:11px; color:#000; font-weight:bold;">Subject           
            </div>          
            <div class="small" style="width:140px;font-size:11px; font-weight:bold;">            
              <a href="#" style="color:#000;">Date / Time</a>          
            </div>          
            <div class="small" onclick="checkedAll('fm',true);" style=" cursor:pointer;width:80px;font-size:11px; color:#000; font-weight:bold;">Select All           
            </div>        
          </div>
  <script language="javascript">
	var checkedSubmit=function(operation){
	var flag=window.confirm("Are you sure you want to " + operation + " this message?");
	if(flag==true){
	SetVal('batchOperatin',operation);
	document.fm.submit();
	}
	}
	</script>        
        <form action="" name="fm" method="post" id="fm">          
        <input type="hidden" name="batchOperatin" id="batchOperatin" value="true" />
        <?
        	if(count($recmsglist)>0)
        	foreach($recmsglist as $val){
        	$detailslink='MsgDetails.php?msgID='.$val['msgid'];
        ?>          
        <div>            
          <div class="med" style="width:150px;">              
            <a href="<?=$detailslink?>">                
              <?=$val['username']?></a>            
          </div>            
          <div class="big" style="width:330px;">              
            <a href="<?=$detailslink?>">                
              <?=$val['subject']?></a>            
          </div>            
          <div class="small" style="width:140px;">              
            <?=$val['datetime']?>            
          </div>            
          <div class="small" style="text-align:center;">              
            <input type="checkbox" name="msgids[]" value="<?=$val['msgid']?>" />            
          </div>          
        </div>          
        <? 
          } 
        ?>        
        </form>      
        </div>      
        <div class="td-clear"></div>      
        <div style="text-align:right; padding-right:40px; padding-top:20px;">        
        <img src="/images/archive.gif" onclick="javascript:checkedSubmit('archive');" style="cursor:pointer" />&nbsp;&nbsp;&nbsp;         
        <img src="/images/delete.gif" onclick="javascript:checkedSubmit('delete');" style="cursor:pointer" />      
        </div>    
      </div>  
    </div>	  	
    <br style="clear:both;" />
  </div>
<? //John Harre 9/13/2010 - disabled due to FF bombing. The file is backed up and all contents have been cleared out.
  include('footer.php');?>