<? include('header.php');
  if($_POST['valueOfsteps']=='step2'){
  $_SESSION['register'][$_POST['valueOfsteps']]=$_POST;
  }
  $pageFormsValue=$_SESSION['register']['step3'];
?>
  <div id="main-column">	 		
    <div class="clear">&nbsp;</div>		 		
    <div>			
      <div class="h3">				
        <div style="float:left; color:#dc983a;">Terms &amp; Conditions
        </div>				
        <div style="float:right; padding-right:10px;">
          <img src="/images/spacer.gif" class="progress-bar" style="background-position:0 -60px;" />
        </div>			
      </div>			
      <div style="padding:14px 10px; position:relative;">				
        <div class="user-register">				 				
          <script language="javascript">
  				function formsubmit(){
  				 document.step.submit();
  				}
  				</script> 				 				 				           
          <div>
            <span>
              <label style="width:700px; font:14px Arial, Helvetica, sans-serif; color:#fff; display:block; padding:10px 80px;">		  
                <span style="display:block; width:100%; text-align:center; font:bold 20px Arial, Helvetica, sans-serif; color:#00ff00;">TERMS OF USE
                </span>		  
                <br/>
                <br/>1. ACCEPTANCE OF TERMS
                <br/>Auctionopia hosts a website (the �Auctionopia Website�) which provides users with a forum to bid on and sell goods and services in online auctions (the �Services�) as posted on the Auctionopia Website, subject to the following terms and conditions and in some instances, guidelines located on the Auctionopia website (collectively, the �Terms of Use�). By using the Auctionopia Website or the Services in any manner, you are agreeing to comply with these Terms of Use.  Should you object to these Terms of Use, or any subsequent modifications thereto or become dissatisfied with Auctionopia in any way, your sole recourse is to immediately discontinue use of Auctionopia.  
                <br/>
                <br/>2. MODIFICATIONS TO THIS AGREEMENT
                <br/>Auctionopia, in its sole and absolute discretion, reserves the right to change, modify or otherwise alter these Terms of Use at any time.  Such modifications shall become effective immediately upon the posting thereof. It is your obligation to review these Terms of Use on a regular basis to keep yourself apprised of any changes.  
                <br/>
                <br/>3.  INTELLECTUAL PROPERTY 
                <br/>All past, present and future content, including, without limitation, words, pictures, graphs, charts, and other matters presented or made available on the Auctionopia Website, along with trademarks, logos, domain names, trade names, service marks and trade identities; any and all copyrightable material (including source and object code); and all other materials related to the Auctionopia Website, including, without limitation, the �look and feel� of the Auctionopia Website (the �Information�) are protected by applicable copyrights and other proprietary rights (including, without limitation, intellectual property rights) and are the property of Auctionopia. By accessing the Auctionopia Website or using Auctionopia�s Services, you agree that Auctionopia is not offering for sale or selling the Information, but instead is granting a limited, royalty free, non-exclusive, revocable, non-commercial, non-assignable, non-transferable license for you to use the Information for your own personal use. However, the license is granted only if you retain all trademark, copyright and other proprietary notices contained in the original Information or any copy you may make of the Information pursuant to these Terms. Unless otherwise set forth in a separate written agreement signed by Auctionopia, you agree that this license does not permit you to: (i) download Information from the Auctionopia Website for use in competing or assisting another person to compete in any manner with Auctionopia; (ii) download and store any graphic or image file on this Auctionopia Website except to the extent that your Browser may temporarily cache graphics or images through its standard features; (iii) sell the Information downloaded (or copied in another form) for money, barter, exchange, or other consideration; (iv) redistribute the Information for free to anyone; (v) make any more than one print copy of the Information; (vi) otherwise publish the Information for any purpose, including commercial gain; or (vii) make any alterations, additions or other modifications to the Information. All rights not expressly granted above are expressly reserved.
                <br/>All trademarks, service marks, trade names or other identifying marks displayed on the Auctionopia Website (the �Marks�) are owned by us. Except as applicable law may provide otherwise or as consented to in advance in writing by Auctionopia, Auctionopia does not consent to any use of the Marks by any person and does not grant you any right to use the Marks displayed on the Auctionopia Website. 
                <br/>
                <br/>4.  CONTENT OF AUCTIONOPIA WEBSITE
                <br/>The Auctionopia Website includes information and functionality for you, including various communications tools and auction services. You acknowledge that Auctionopia does not pre-screen or approve advertisements, audio or other files, images, messages, photos, text, video, or other material (�Content�), but Auctionopia shall have the right (but not the obligation), in its sole and absolute discretion, to refuse, delete or move any Content available through the Services for violating the letter or spirit of these Terms of Use or for any other reason. Any person who makes Content available through the Services is solely responsible for such Content. Auctionopia does not control, and is not responsible for, Content available through the Services, and you understand that by using the Services you may be exposed to Content that is offensive, indecent, inaccurate, misleading, or otherwise objectionable. Auctionopia makes no representation or warranty as to the accuracy, completeness or authenticity of any Content available through the Services. It is solely your obligation to evaluate the risk associated with the use of the Services and the reliance on any Content contained therein. Under no circumstances will Auctionopia be liable in any way for any Content or for any loss or damage of any kind incurred as a result of the use of any Content available through the Services.
                <br/>
                <br/>5. THIRD PARTY CONTENT, SITES, AND SERVICES
                <br/>The Auctionopia website and Content available through the Services may contain links which   provide you with access to third party content which is completely independent of Auctionopia, including web sites, directories, servers, networks, systems, information and databases, applications, software, programs, products or services, and the Internet as a whole. Auctionopia makes no representation or warranty as to the accuracy, completeness or authenticity of any information contained in any such third party content.  Linking to any third party content shall be performed �at your own risk.� It is solely your obligation to evaluate the risk associated with the use or reliance on any third party content, and under no circumstances will Auctionopia be liable in any way for any loss or damage of any kind incurred as a result of the use or reliance on any third party content.
                <br/>
                <br/>6.  DEALINGS WITH OTHERS
                <br/>Your dealings with individuals or organizations found on or through the Services, including payment and delivery of goods or services, and any other terms, conditions, warranties or representations associated with such dealings, are solely between you and such individuals or organizations.  You should take whatever precautions you feel necessary or appropriate before proceeding with any online or offline (including in-person) transaction with any third party. 
                <br/>  By using the Services or posting/reviewing Content, you agree that Auctionopia shall not be responsible or liable for any loss or damage of any sort (including bodily injury or death) incurred as the result of any such dealings. If there is a dispute between participants on this site, or between users and any third party, you understand and agree that Auctionopia is under no obligation to resolve such disputes or become involved in any manner. In the event that you have a dispute with one or more other users, you hereby release Auctionopia, its officers, employees, agents and successors in rights from claims, demands and damages (actual and consequential) of every kind or nature, known or unknown, suspected and unsuspected, disclosed and undisclosed, arising out of or in any way related to such disputes and/or our Services.  By using the Services or posting/reviewing Content, you further expressly waive any right or defense contained in any federal or state statute which provides that a general waiver does not extend to the waiver claims that are unknown at the time the release is executed.
                <br/>
                <br/>  7.  PAID POSTINGS
                <br/>Auctionopia may charge a fee to make Content available through the Services. The fee is an access fee permitting Content to be posted in a designated area. All fees paid will be non-refundable in the event that Content is removed from the Services for violating the Terms of Use.  
                <br/>
                <br/>8.  PRIVACY AND INFORMATION DISCLOSURE
                <br/>Auctionopia has established a Privacy Policy to explain to users how their information is collected and used. Your use of the Auctionopia website or the Services signifies acknowledgement of and agreement to our Privacy Policy. The Privacy Policy is located at the following web address:
                <br/>  
                <br/>[web address to Auctionopia 
                <a href="privacypolicy.php" style="color:#FF9900;">Privacy Policy</a>]
                <br/>
                <br/>9.  �HOW TO� PAGE
                <br/>Auctionopia has established a �How To� page to explain to users how the Auctionopia website works and how the Services are provided. Your use of the Auctionopia website or the Services signifies acknowledgement of and agreement to the terms and conditions contained on the �How To� page. The �How To� page is located at the following web address: 
                <br/>
                <br/>[web address to the 
                <a href="howto.php" style="color:#FF9900;">How To</a> page]
                <br/>
                <br/>10.  EMAIL COMMUNICATIONS
                <br/>By submitting your email address and using the Auctionopia website or the Services, you agree to receive emails from Auctionopia regarding invoicing (or other billing matters), website updates, maintenance reminders or marketing materials.  If you do not wish to receive these emails, you may opt-out at any time by following the instructions contained at the bottom of the email correspondence. Additional fees may apply if you elect not to receive invoices via email. IT IS YOUR RESPONSIBILITY TO NOTIFY AUCTIONOPIA OF ANY CHANGE IN YOUR EMAIL ADDRESS.
                <br/>
                <br/>  11.	CONDUCT
                <br/>By using the Auctionopia website or the Services, you agree not to post, email, or otherwise make available Content:
                <br/>
                <br/>a) that harasses, degrades, intimidates, threatens or is otherwise hateful or abusive toward an individual or group of individuals on the basis of religion, gender, sexual orientation, race, ethnicity, age, or disability; is otherwise harmful, threatening, abusive, harassing, defamatory, libelous, or invasive of another�s privacy; is false, deceptive, misleading, deceitful; or is harmful to minors in any way;
                <br/>b) that is pornographic or depicts a human being engaged in actual sexual conduct including but not limited to (i) sexual intercourse, including genital-genital, oral-genital, anal-genital, or oral-anal, whether between persons of the same or opposite sex, or (ii) bestiality, or (iii) masturbation, or (iv) sadistic or masochistic abuse, or (v) lascivious exhibition of the genitals or pubic area of any person;
                <br/>c) that violates federal, state, or local laws, statutes, rules, ordinances, regulations, codes, licenses, authorizations, decisions, injunctions, interpretations, orders or decrees of any court or Governmental Authority having jurisdiction as may be in effect from time to time;
                <br/>d) that impersonates any person or entity or falsely states or otherwise misrepresents your affiliation with a person or entity (this provision does not apply to Content that constitutes lawful non-deceptive parody of public figures);
                <br/>e) that includes personal or identifying information about another person without that person�s explicit consent;
                <br/>f) that infringes upon any patent, trademark, trade secret, copyright or other proprietary rights of any party, or any Content that you do not have a right to make available;
                <br/>g) that constitutes or contains  �affiliate marketing,� �chain letters,� �junk mail,� �link referral code,� �pyramid schemes,� �spam,� or unsolicited commercial advertisement; 
                <br/>h) that constitutes or contains any form of advertising or solicitation if: posted in areas of the Auctionopia sites which are not designated for such purposes; or emailed to Auctionopia users who have not indicated in writing that it is ok to contact them about other services, products or commercial interests;
                <br/>i) that advertises any illegal item or service or offers for sale any items prohibited or restricted by law, including without limitation the following items and services: 
                <br/>    &bull;	Adult Services (i.e erotic massages, escort services, exotic dancing).
                <br/>&bull;	Alcohol.
                <br/>&bull;	Blood, bodily fluids or body parts.
                <br/>&bull;	Bulk email or mailing lists that containing personal identifying information.
                <br/>&bull;	Burglary tools (i.e., lock-picks, tension bars, slim jims, master keys, etc.)
                <br/>&bull;	Controlled substances or other illegal drugs; and substances and items used to manufacture controlled substances.
                <br/>&bull;	Counterfeit currency, coins and stamps, tickets, and equipment designed to make such counterfeit items.
                <br/>&bull;	Counterfeit, replica, or knock-off brand name goods.
                <br/>&bull;	Coupons or gift cards that restrict transfer, or which you are not authorized to sell.
                <br/>&bull;	Criminal Activity (i.e. murder for hire, kidnapping or any type of physical attack.)
                <br/>&bull;	Drug paraphernalia.
                <br/>&bull;	Explosives of any kind or other destructive devices (including instructions to build any explosive device.)
                <br/>&bull;	Fireworks, including but not limited to �safe and sane� fireworks (i.e. sparklers, smokers, snakes.)
                <br/>&bull;	Gambling items, including but not limited to lottery tickets, raffle tickets, sweepstakes entries or slot machines.
                <br/>&bull;	False identification cards, items with police insignia, citizenship documents, or birth certificates.
                <br/>&bull;	Items issued to United States Armed Forces that have not been disposed of in accordance with Department of Defense demilitarization policies.
                <br/>&bull;	Material that infringes copyright, including but not limited to software or other digital goods you are not authorized to sell.
                <br/>&bull;	Non-packaged food items or adulterated food.
                <br/>&bull;	Non-prescription drugs that make false or misleading treatment claims or treatment claims that require FDA approval.
                <br/>&bull;	Obscene material or child pornography. 
                <br/>&bull;	Offer or solicitation of illegal prostitution. 
                <br/>&bull;	Pesticides or hazardous substances, or items containing hazardous substances including but not limited to contaminated toys, or art or craft material containing toxic substances without a warning label.
                <br/>&bull;	Pets of any kind including but not limited to dogs, cats, primates, cage birds, rodents, reptiles, amphibians, fish.
                <br/>&bull;	Pet parts, blood, or fluids - including but not limited to stud/breeding service.
                <br/>&bull;	Prescription drugs and medical devices, including, but not limited to, prescription or contact lenses, defibrillators, hypodermic needles or hearing aids.
                <br/>&bull;	Prostitution (i.e. engaging in a sexual act in exchange for money, drugs,  property or any other item of value.)
                <br/>&bull;	Restricted or regulated plants and insects, including, but not limited to, noxious weeds, endangered plant species, or live insects or pests.
                <br/>&bull;	Stolen property or property with serial number removed or altered.
                <br/>&bull;	Surveillance Equipment (i.e. telephone bugs, wiretapping devices; miniature transmitters, surveillance microphones, etc.) or other illegal telecommunications equipment (i.e. access cards, password sniffers, access card programmers and unloopers, or cable descramblers.) 
                <br/>&bull;	Tickets that restrict transfer (airline; train; cruise, etc.), and tickets of any kind which you are not authorized to sell.
                <br/>&bull;	Tobacco or tobacco products.
                <br/>&bull;	Weapons and related items, including but not limited to firearms (including parts and accessories (i.e. scopes, silencers, ammunition, ammunition magazines)), body armor, knives, brass knuckles, martial arts weapons (except faux or practice items, such as foam nunchaku or plastic throwing stars, etc.), BB/pellet guns, mace/pepper spray, tear gas or stun guns/tazers.
                <br/>j) that contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer software or hardware or telecommunications equipment; 
                <br/>k) that disrupts the normal flow of dialogue with an excessive amount of Content (flooding attack) to the Services, or that otherwise negatively affects other users� ability to use the Services; 
                <br/>l) that contain misleading email addresses, or forged headers or otherwise manipulated identifiers in order to disguise the origin of Content transmitted through the Services.
                <br/>
                <br/>Additionally, you agree not to:
                <br/>m) make any postings of any kind if you are under the age of thirteen (13).
                <br/>n)  contact anyone who has asked not to be contacted, or make unsolicited contact with anyone for any commercial purpose;
                <br/>o) �stalk� or otherwise harass anyone;
                <br/>p) collect personal data about other users for commercial or unlawful purposes;
                <br/>q) post non-local or otherwise irrelevant Content, repeatedly post the same or similar Content or otherwise impose an unreasonable or disproportionately large load on our infrastructure;
                <br/>r) attempt to gain unauthorized access to Auctionopia�s computer systems or engage in any activity that disrupts, diminishes the quality of, interferes with the performance of, or impairs the functionality of, the Services or the Auctionopia website;
                <br/>s) use any automated device or computer program to download data from the Services or to submit postings or other Content - unless expressly permitted by Auctionopia;
                <br/>t) use any form of automated device or computer program (�flagging tool�) that enables the use of Auctionopia�s �flagging system� or other community moderation systems without each flag being manually entered by the person that initiates the flag (an �automated flagging device�), or use the flagging tool to remove posts of competitors, or to remove posts without a good faith belief that the post being flagged violates these Terms of Use.
                <br/>
                <br/>12.  BIDDING
                <br/>By using the Auctionopia website or the Services, you agree that any bid submitted during an auction shall be considered an offer to enter into a binding contract with a seller of a good or service in the event that such bid is the winning bid. The buyer of such good or service shall be obligated, to the fullest extent of the law, to purchase the auctioned good or service from the seller pursuant to the terms of the sale; however, a seller may, in the seller�s sole and absolute discretion, allow a buyer to cancel a sale after the auction has ended.
                <br/>
                <br/>  13.  PAYMENTS TO AUCTIONOPIA
                <br/>By using the Auctionopia website or the Services, you agree to pay all amounts due and owing to Auctionopia as and when due and without any right to set-off. You must notify Auctionopia within 30 days of the date on an invoice if you dispute any discounts, credits, or fees that were applied to your account. Except as determined in Auctionopia�s sole and absolute discretion, any dispute relating to an invoice if resolved in your favor will result in a credit to your account. In the event that Auctionopia elects to resolve such a dispute in a manner other than crediting your account, such resolution will not be binding or effective unless it is made in writing and signed by both parties.
                <br/>
                <br/>14.  POSTING AGENTS
                <br/>Use of �Posting Agents� is prohibited.  A �Posting Agent� is a third-party agent, service, or intermediary that offers to post Content to the Services on behalf of others. 
                <br/>
                <br/>15.  ACCESS TO THE SERVICES
                <br/>Auctionopia grants you a limited, revocable, nonexclusive license to access the Services for your own personal use which, Auctionopia may revoke at any time in its sole and absolute discretion.  
                <br/>
                <br/>16.  LIMITATIONS ON SERVICES
                <br/>You acknowledge that Auctionopia may establish limits concerning use of the Services, including the maximum number of days that Content will be retained by the Services, the maximum number and size of postings, email messages, or other Content that may be transmitted or stored by the Services, and the frequency with which you may access the Services. You agree that Auctionopia has no responsibility or liability for the deletion or failure to store any Content maintained or transmitted by the Services. You acknowledge that Auctionopia reserves the right at any time to modify or discontinue the Services (or any part thereof), with or without notice, and that Auctionopia shall not be liable to you or to any third party for any modification, suspension or discontinuance of the Services.
                <br/>
                <br/>17.  TERMINATION OF SERVICES
                <br/>You agree that Auctionopia, in its sole discretion, has the right (but not the obligation) to delete or deactivate your account, block your email or IP address, or otherwise terminate your access to or use of the Services (or any part thereof), immediately and without notice, and remove and discard any Content within the Services, for any reason, including, without limitation, if Auctionopia believes that you have violated these Terms of Use or otherwise acted inconsistently with the letter or spirit of these Terms of Use. Further, you agree that Auctionopia shall not be liable to you or any third-party for any termination of your access to the Services.  Further, you agree not to attempt to use the Services after said termination.  Sections 2, 3, 5, 7, 8, 12, 13, 15-18, 20 and 21 shall survive termination of the Terms of Use.
                <br/>
                <br/>18.  PROPRIETARY RIGHTS
                <br/>The Services are protected to the maximum extent permitted by copyright laws and international treaties. Content displayed on or through the Services is protected by copyright as a collective work and/or compilation, pursuant to copyrights laws, and international conventions. Any reproduction, modification, creation of derivative works from, or redistribution of, the site or the collective work, and/or copying or reproducing the sites, or any portion thereof, to any other server or location for further reproduction or redistribution is prohibited without the express written consent of Auctionopia. You further agree not to reproduce, duplicate or copy Content from the Services without the express written consent of Auctionopia, and agree to abide by any and all copyright notices displayed on the Services. You may not decompile or disassemble, reverse engineer or otherwise attempt to discover any source code contained in the Services. Without limiting the foregoing, you agree not to reproduce, duplicate, copy, sell, resell or exploit for any commercial purposes, any aspect of the Services. 
                <br/>Although Auctionopia does not claim ownership of content that its users post, by posting Content to any public area of the Services, you automatically grant, and you represent and warrant that you have the right to grant, to Auctionopia an irrevocable, perpetual, non-exclusive, fully paid, worldwide license to use, copy, perform, display, and distribute said Content and to prepare derivative works of, or incorporate into other works, said Content, and to grant and authorize sublicenses (through multiple tiers) of the foregoing. Furthermore, by posting Content to any public area of the Services, you automatically grant Auctionopia all rights necessary to prohibit any subsequent aggregation, display, copying, duplication, reproduction, or exploitation of the Content on the Services by any party for any purpose.
                <br/>
                <br/>19.  NOTIFICATION OF CLAIMS OF INFRINGEMENT
                <br/>If you believe that your work has been copied in a way that constitutes copyright infringement, or your intellectual property rights have been otherwise violated, please notify Auctionopia immediately: support@auctionopia.com
                <br/>  Please provide the following information:
                <br/>a) The identity and location of material that you claim is infringing on your intellectual property;
                <br/>b) A statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law;
                <br/>c) A statement by you declaring under penalty of perjury that (1) the above information is accurate, and (2) that you are the owner of the intellectual property or that you are authorized to act on behalf of that owner (if you claim you are acting on behalf the owner, you must provide the owner�s name and contact information);
                <br/>d) Your address, telephone number, and email address; and
                <br/>e) Your electronic signature.
                <br/>
                <br/>20. DISCLAIMER OF WARRANTIES
                <br/>YOU AGREE THAT USE OF THE AUCTIONOPIA SITE AND THE SERVICES IS ENTIRELY AT YOUR OWN RISK. THE AUCTIONOPIA SITE AND THE SERVICES ARE PROVIDED ON AN �AS IS� OR �AS AVAILABLE� BASIS, WITHOUT ANY WARRANTIES OF ANY KIND.  ALL EXPRESS AND IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT OF PROPRIETARY RIGHTS ARE EXPRESSLY DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW.  TO THE FULLEST EXTENT PERMITTED BY LAW, AUCTIONOPIA DISCLAIMS ANY WARRANTIES FOR THE SECURITY, RELIABILITY, TIMELINESS, ACCURACY, AND PERFORMANCE OF THE AUCTIONOPIA SITE AND THE SERVICES.  TO THE FULLEST EXTENT PERMITTED BY LAW, AUCTIONOPIA DISCLAIMS ANY WARRANTIES FOR OTHER SERVICES OR GOODS RECEIVED THROUGH OR ADVERTISED ON THE AUCTIONOPIA SITE OR THE SITES OR SERVICES, OR ACCESSED THROUGH ANY LINKS ON THE AUCTIONOPIA SITE.  TO THE FULLEST EXTENT PERMITTED BY LAW, AUCTIONOPIA DISCLAIMS ANY WARRANTIES FOR VIRUSES OR OTHER HARMFUL COMPONENTS IN CONNECTION WITH THE AUCTIONOPIA SITE OR THE SERVICES.  Some jurisdictions do not allow the disclaimer of implied warranties.  In such jurisdictions, some of the foregoing disclaimers may not apply to you insofar as they relate to implied warranties.
                <br/>
                <br/>21. LIMITATIONS OF LIABILITY
                <br/>UNDER NO CIRCUMSTANCES SHALL AUCTIONOPIA BE LIABLE FOR DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES (EVEN IF AUCTIONOPIA HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES), RESULTING FROM ANY ASPECT OF YOUR USE OF THE AUCTIONOPIA SITE OR THE SERVICES, WHETHER THE DAMAGES ARISE FROM USE OR MISUSE OF THE AUCTIONOPIA SITE OR THE SERVICES, FROM INABILITY TO USE THE AUCTIONOPIA SITE OR THE SERVICES, OR THE INTERRUPTION, SUSPENSION, MODIFICATION, ALTERATION, OR TERMINATION OF THE AUCTIONOPIA SITE OR THE SERVICES.  SUCH LIMITATION SHALL ALSO APPLY WITH RESPECT TO DAMAGES INCURRED BY REASON OF OTHER SERVICES OR PRODUCTS RECEIVED THROUGH, OR ADVERTISED IN, CONNECTION WITH THE AUCTIONOPIA SITE OR THE SERVICES OR ANY LINKS ON THE AUCTIONOPIA SITE, AS WELL AS BY REASON OF ANY INFORMATION OR ADVICE RECEIVED THROUGH OR ADVERTISED IN CONNECTION WITH THE AUCTIONOPIA SITE OR THE SERVICES OR ANY LINKS ON THE AUCTIONOPIA SITE.  THESE LIMITATIONS SHALL APPLY TO THE FULLEST EXTENT PERMITTED BY LAW. In some jurisdictions, limitations of liability are not permitted.  In such jurisdictions, some of the foregoing limitation may not apply to you.
                <br/>
                <br/>22. INDEMNITY
                <br/>You agree to indemnify and hold Auctionopia, its officers, subsidiaries, affiliates, successors, assigns, directors, officers, agents, service providers, suppliers and employees, harmless from any claim or demand, including reasonable attorney fees and court costs, made by any third party  due to or arising out of Content you submit, post or make available through the Services, your use of the Services, your violation of the Terms of Use, your breach of any of the representations and warranties herein, or your violation of any rights of another.
                <br/>
                <br/>23. GENERAL INFORMATION
                <br/>The Terms of Use constitute the entire agreement between you and Auctionopia and govern your use of the Services, superseding any prior agreements between you and Auctionopia. The Terms of Use and the relationship between you and Auctionopia shall be governed by the laws of Maryland without regard to its conflict of law provisions. You and Auctionopia agree to submit to the personal and exclusive jurisdiction of the courts located within Baltimore County, Maryland. The failure of Auctionopia to exercise or enforce any right or provision of the Terms of Use shall not constitute a waiver of such right or provision. In case any provision (or any part of any provision) contained in these Terms of Use shall for any reason be held to be invalid, illegal or unenforceable in any respect, such invalidity, illegality or unenforceability shall not affect any other provision (or remaining part of the affected provision) of these Terms of Use, but these Terms of Use shall be construed as if such invalid, illegal or unenforceable provision (or part thereof) had never been contained herein but only to the extent it is invalid, illegal or unenforceable. You agree that regardless of any statute or law to the contrary, any claim or cause of action arising out of or related to use of the Services or the Terms of Use must be filed within one (1) year after such claim or cause of action arose or be forever barred. 
              </label>
            </span>
          </div>			
          <form name="step" id="step" method="post" action="registrationconfirm.php">
            <input type="hidden" name='valueOfsteps' value="step3" />				
            <div style="padding:none;">					
              <div style="float:left; padding:none;">	
                <input type="radio" name="c_AGREE" style="float:left;"  value="1" checked="checked" />	
                <font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;&nbsp;&nbsp; I Agree
                </font>					
              </div>					
              <div style="float:left; padding:none; display:none;">
                <font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif">I Agree
                </font>					
              </div>				
            </div>				
            <div style="clear:both; padding:none;">					
              <div style="float:left; padding:none;">	
                <input type="radio" name="c_AGREE" value="2" style="float:left;"  />	 	
                <font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;&nbsp;&nbsp;I Do Not Agree	
                </font>	 		 		
              </div>					
              <div style="float:left; padding:none; display:none;">
                <label style="float:left;  margin-left:5px;">I Do Not Agree
                </label>					
              </div>				
            </div>				<br style="clear:both;" /><br style="clear:both;" />				
          </form>				
          <div style="float:left; width:100%;">					
            <div class="nav-page" style="float:left;">
              <a href="registrationsteptwo.php">Back</a>
            </div>					
            <div class="nav-page">
              <a href="javascript:formsubmit()">Next</a>
            </div>				
          </div>				 				
        </div>			
      </div>		
    </div>	
  </div>
<? //John Harre 9/13/2010 - disabled due to FF bombing. The file is backed up and all contents have been cleared out.
  include('footer.php');?>