$(document).ready(function(){
	//show 10 feedbacks of a seller
	$('#show_more_s').click(function(){
		var limit = $('#limit_s').val();
		var user_id = $('#user_id').val();
		var total = $('#total_seller').val();
		$.ajax({
			url : '/ebay/ajaxFeedback.php',
			type: 'POST',
			data: 'limit='+limit+'&user_id='+user_id+'&role=Seller&all=0',
			success: function(result){
				$('#more_s').append(result);

				total = total - 10;
				$('#total_seller').val(total);

				limit = limit * 1 + 10;
				$('#limit_s').val(limit);

				if(total <= 0){
					$('#show_more_s').fadeOut(500, function(){$(this).remove();})
					$('#show_all_s').fadeOut(500, function(){$(this).remove();})
				}
			}
		});
	});
	//show all feedbacks of a seller
	$('#show_all_s').click(function(){
		//$('loader').append('<img src="ebay/img/ajax-loader.gif" alt="Preloader"/>');
		var user_id = $('#user_id').val();
		var limit = $('#limit_s').val();
		$.ajax({
			url : '/ebay/ajaxFeedback.php',
			type: 'POST',
			data: 'user_id='+user_id+'&role=Seller'+'&limit='+limit+'&all=1',
			success: function(result){
				$('#more_s').fadeIn(500, function(){$(this).append(result);});
				$('#show_more_s').fadeOut(500, function(){$(this).remove();})
				$('#show_all_s').fadeOut(500, function(){$(this).remove();})
			}
		});
	});

	//show 10 feedbacks of a buyer
	$('#show_more_b').click(function(){
		var limit = $('#limit_b').val();
		var user_id = $('#user_id').val();
		var total = $('#total_buyer').val();
		$.ajax({
			url : '/ebay/ajaxFeedback.php',
			type: 'POST',
			data: 'limit='+limit+'&user_id='+user_id+'&role=Buyer&all=0',
			success: function(result){
				$('#more_b').append(result);
				
				total = total - 10;
				$('#total_buyer').val(total);

				limit = limit * 1 + 10;
				$('#limit_b').val(limit);
				
				if(total <= 0){
					$('#show_more_b').fadeOut(500, function(){$(this).remove();})
					$('#show_all_b').fadeOut(500, function(){$(this).remove();})
				}
			}
		});
	});
	
	//show all feedbacks of a buyer
	$('#show_all_b').click(function(){
		var user_id = $('#user_id').val();
		var limit = $('#limit_b').val();
		$.ajax({
			url : '/ebay/ajaxFeedback.php',
			type: 'POST',
			data: 'user_id='+user_id+'&role=Buyer'+'&limit='+limit+'&all=1',
			success: function(result){
				$('#more_b').append(result);
				$('#show_more_b').fadeOut(500, function(){$(this).remove();})
				$('#show_all_b').fadeOut(500, function(){$(this).remove();})
			}
		});
	});

	//show 10 feedbacks of a seller
	$('#show_more_s_admin').click(function(){
		var limit = $('#limit_s').val();
		var user_id = $('#user_id').val();
		var total = $('#total_seller').val();
		$.ajax({
			url : '/ebay/ajaxFeedback.php',
			type: 'POST',
			data: 'limit='+limit+'&user_id='+user_id+'&role=Seller&all=0&admin=1',
			success: function(result){
				$('#more_s').append(result);

				total = total - 10;
				$('#total_seller').val(total);

				limit = limit * 1 + 10;
				$('#limit_s').val(limit);

				if(total <= 0){
					$('#show_more_s_admin').fadeOut(500, function(){$(this).remove();})
					$('#show_all_s_admin').fadeOut(500, function(){$(this).remove();})
				}
			}
		});
	});
	//show all feedbacks of a seller
	$('#show_all_s_admin').click(function(){
		//$('loader').append('<img src="ebay/img/ajax-loader.gif" alt="Preloader"/>');
		var user_id = $('#user_id').val();
		var limit = $('#limit_s').val();
		$.ajax({
			url : '/ebay/ajaxFeedback.php',
			type: 'POST',
			data: 'user_id='+user_id+'&role=Buyer'+'&limit='+limit+'&all=1&admin=1',
			success: function(result){
				$('#more_s').fadeIn(500, function(){$(this).append(result);});
				$('#show_more_s_admin').fadeOut(500, function(){$(this).remove();})
				$('#show_all_s_admin').fadeOut(500, function(){$(this).remove();})
			}
		});
	});

	//show 10 feedbacks of a buyer
	$('#show_more_b_admin').click(function(){
		var limit = $('#limit_b').val();
		var user_id = $('#user_id').val();
		var total = $('#total_buyer').val();
		$.ajax({
			url : '/ebay/ajaxFeedback.php',
			type: 'POST',
			data: 'limit='+limit+'&user_id='+user_id+'&role=Seller&all=0&admin=1',
			success: function(result){
				$('#more_b').append(result);

				total = total - 10;
				$('#total_buyer').val(total);

				limit = limit * 1 + 10;
				$('#limit_b').val(limit);

				if(total <= 0){
					$('#show_more_b_admin').fadeOut(500, function(){$(this).remove();})
					$('#show_all_b_admin').fadeOut(500, function(){$(this).remove();})
				}
			}
		});
	});

	//show all feedbacks of a buyer
	$('#show_all_b_admin').click(function(){
		var user_id = $('#user_id').val();
		var limit = $('#limit_b').val();
		$.ajax({
			url : '/ebay/ajaxFeedback.php',
			type: 'POST',
			data: 'user_id='+user_id+'&role=Seller'+'&limit='+limit+'&all=1&admin=1',
			success: function(result){
				$('#more_b').append(result);
				$('#show_more_b_admin').fadeOut(500, function(){$(this).remove();})
				$('#show_all_b_admin').fadeOut(500, function(){$(this).remove();})
			}
		});
	});

});