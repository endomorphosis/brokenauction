<?php
class eBay {
	protected $_tradingEbayUrlProduction = 'https://api.ebay.com/ws/api.dll';
	protected $_tradingEbayUrlSundbox = 'https://api.sandbox.ebay.com/ws/api.dll';

	protected $_developerIdProduction = '39431c0c-55b0-4d96-af90-277d80c6bc30';
	protected $_appIdProduction = 'Home1d45a-cb7e-41fb-89ee-8e477873314';
	protected $_certIdProduction = '5e410d24-38fa-4a14-8776-53f0950d5543';
	protected $_productionRuname = 'Home-Home1d45a-cb7e--titigwow';

	protected $_developerIdSundbox = '39431c0c-55b0-4d96-af90-277d80c6bc30';
	protected $_appIdSundbox = 'Home32c02-a131-4721-812a-24a8cd7afe6';
	protected $_certIdSundbox = '6c002b24-9371-4542-a88c-f964c7de305b';
	protected $_sandboxRuname = 'Home-Home32c02-a131--cgskrbs';

	protected $_mode = 'production';

	protected $_eBayCompatibility = 635;

	protected $_getSessionId = '
				<?xml version="1.0" encoding="utf-8"?>
				<GetSessionIDRequest xmlns="urn:ebay:apis:eBLBaseComponents">
					<Version>619</Version>
						<RequesterCredentials>
						 <DevId>%s</DevId>
						 <AppId>%s</AppId>
						 <AuthCert>%s</AuthCert>
					   </RequesterCredentials>
					<RuName>%s</RuName>
				</GetSessionIDRequest>
	';

	protected $_getFeedBack = '
				<?xml version="1.0" encoding="utf-8"?>
					<GetFeedbackRequest xmlns="urn:ebay:apis:eBLBaseComponents">
					  <Version>%s</Version>
						<RequesterCredentials>
							<eBayAuthToken>%s</eBayAuthToken>
						</RequesterCredentials>
						<UserID>%s</UserID>
						<DetailLevel>ReturnAll</DetailLevel>
					</GetFeedbackRequest>
	';


	protected $_fetchToken = '
				<?xml version="1.0" encoding="utf-8"?>
					<FetchToken xmlns="urn:ebay:apis:eBLBaseComponents">
						<SecretID>%s</SecretID>
						<RequesterCredentials>
							<Username>%s</Username>
						</RequesterCredentials>						
					</FetchToken>
	';

	public function __construct(){
		
	}

	public function getSessionId(){
		$callName = 'GetSessionID';
		$this->_eBayCompatibility = 619;
		if ($this->_mode == 'production' ){
			$request = $this->_getSessionId;
			$request = sprintf($request, $this->_developerIdProduction, $this->_appIdProduction, $this->_certIdProduction, $this->_productionRuname);
			$result = $this->_callRequest($this->_tradingEbayUrlProduction, $callName, $request);
			$sessionId = $this->_parseSessionId($result);
			return $sessionId;
		}
		else{
			$request = $this->_getSessionId;
			$request = sprintf($request, $this->_developerIdSundbox, $this->_appIdSundbox, $this->_certIdSundbox, $this->_sandboxRuname);
			$result = $this->_callRequest($this->_tradingEbayUrlSundbox, $callName, $request);
			$sessionId = $this->_parseSessionId($result);
			return $sessionId;
		}
	}

	protected function _parseSessionId($xml){
		$sessionId = simplexml_load_string($xml);
		return (string)$sessionId->SessionID;
	}

	protected function _setHeaders($callName){
		if ($this->_mode == 'production'){
			$eBayHeaders = array (
			'X-EBAY-API-COMPATIBILITY-LEVEL: ' . $this->_eBayCompatibility,
			'X-EBAY-API-SESSION-CERTIFICATE: ' . $this->_developerIdProduction . ';' . $this->_appIdProduction . ';' . $this->_certIdProduction,
			'X-EBAY-API-DEV-NAME: ' . $this->_developerIdProduction,
			'X-EBAY-API-APP-NAME: ' . $this->_appIdProduction,
			'X-EBAY-API-CERT-NAME:' . $this->_certIdProduction,
			'X-EBAY-API-CALL-NAME: '. $callName,
			'X-EBAY-API-SITEID: 0',
			'X-EBAY-API-DETAIL-LEVEL: 0'
			);
		}
		else{
			$eBayHeaders = array (
			'X-EBAY-API-COMPATIBILITY-LEVEL: ' . $this->_eBayCompatibility,
			'X-EBAY-API-SESSION-CERTIFICATE: ' . $this->_developerIdSundbox . ';' . $this->_appIdSundbox . ';' . $this->_certIdSundbox,
			'X-EBAY-API-DEV-NAME: ' . $this->_developerIdSundbox,
			'X-EBAY-API-APP-NAME: ' . $this->_appIdSundbox,
			'X-EBAY-API-CERT-NAME:' . $this->_certIdSundbox,
			'X-EBAY-API-CALL-NAME: '. $callName,
			'X-EBAY-API-SITEID: 0',
			'X-EBAY-API-DETAIL-LEVEL: 0'
			);
		}
		return $eBayHeaders;
	}

	protected function _callRequest($url, $callName, $request){
		$APIConnection = curl_init();
		curl_setopt( $APIConnection, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt( $APIConnection, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt( $APIConnection, CURLOPT_HTTPHEADER, $this->_setHeaders($callName)); // Set the eBay Headers
		curl_setopt( $APIConnection, CURLOPT_URL, $url);
		curl_setopt( $APIConnection, CURLOPT_POST, 1 );
		curl_setopt( $APIConnection, CURLOPT_POSTFIELDS, $request);
		curl_setopt( $APIConnection, CURLOPT_RETURNTRANSFER, 1 );
		$response = curl_exec( $APIConnection );
		curl_close( $APIConnection );
		return $response;
	}

	public function callAutorize(){
//		$sessionId = $this->getSessionId();
		$secretId = md5(uniqid());
		if($this->_mode == 'production'){
			$url = 'https://signin.ebay.com/ws/eBayISAPI.dll?SignIn&runame=' . $this->_productionRuname . '&sid=' . $secretId . '&ruparams=SecretID=' . $secretId;
		}
		else{
			$url = 'https://signin.sandbox.ebay.com/ws/eBayISAPI.dll?SignIn&runame=' . $this->_sandboxRuname . '&SessID=' . $sessionId;
		}
		return $url;
	}

	public function fetchToken($secretId, $userName){
		$this->_eBayCompatibility = 639;
		$fetchToket = sprintf($this->_fetchToken, $secretId, $userName);
		$result = $this->_callRequest($this->_tradingEbayUrlProduction, 'FetchToken', $fetchToket);
		$token = simplexml_load_string($result);
		return (string)$token->eBayAuthToken;
	}

	public function getFeedback($username, $token){
		$callName = 'GetFeedback';
		$this->_eBayCompatibility = 639;
		$request = sprintf($this->_getFeedBack, $this->_eBayCompatibility, $token, $username);
		if ($this->_mode == 'production'){
			$result = $this->_callRequest($this->_tradingEbayUrlProduction, $callName, $request);
		}
		else{
			$result = $this->_callRequest($this->_tradingEbayUrlSundbox, $callName, $request);
		}
		return $result;
	}

	public function fetchFeedBack($feedBacks){
		$result = array();
		$feddBackObject = simplexml_load_string($feedBacks);
		foreach ($feddBackObject->FeedbackDetailArray as $feedBackArray){
			$result['FeedBacks'] = $this->_prepeareFeedbacks($feedBackArray);
		}
		foreach ($feddBackObject->FeedbackSummary as $k=>$v){
			foreach($v as $key=>$value){
				if ($key == 'SellerRatingSummaryArray'){
					$result['Ratings'] = $this->_prepeareRating($value->AverageRatingSummary[0]);
				}
			}
		}
		return $result;
	}

	protected function _prepeareFeedbacks($feedBackArray){
		$positiveCountSeller = 0;
		$negativeCountSeller = 0;

		$positiveCountBuyer = 0;
		$negativeCountBuyer = 0;

		$user_id = $_SESSION['m_info']['user_id'];
		$feedBacks = array();
		
		$feedBacks['count']['buyer']['positive'] = 0;
		$feedBacks['count']['buyer']['negative'] = 0;
		$feedBacks['count']['seller']['positive'] = 0;
		$feedBacks['count']['seller']['negative'] = 0;
		
		foreach($feedBackArray as $feedBackKey=>$feedbackContent){
			$checkQuery = 'SELECT `id`
							FROM `ebay_feedback`
							WHERE
						`transaction` = "' . (string)$feedbackContent->TransactionID .
					'" AND `from` = "' . (string)$feedbackContent->CommentingUser . '"' ;
			$check = mysql_query($checkQuery);
			$result = mysql_fetch_array($check);
			if (!$result){
				$query = 'INSERT INTO `ebay_feedback_temp`
						VALUES
						(null, ' . $user_id . ',
						 "'. (string)$feedbackContent->CommentingUser .'",
						 "' . (string)$feedbackContent->CommentText . '",
						 "' . (string)$feedbackContent->CommentType . '",
						 '. (string)$feedbackContent->TransactionID .',
						 "'. (string)$feedbackContent->Role .'")
				';
				
				mysql_query($query);
			}

			if ((string)$feedbackContent->CommentType == 'Positive' && (string)$feedbackContent->Role == 'Seller' ){
					++$positiveCountSeller;
					$feedBacks['count']['buyer']['positive'] = $positiveCountSeller;
 			}
			elseif((string)$feedbackContent->CommentType == 'Negative' && (string)$feedbackContent->Role == 'Seller'){
					++$negativeCountSeller;
					$feedBacks['count']['buyer']['negative'] = $negativeCountSeller;
			}
			elseif((string)$feedbackContent->CommentType == 'Positive' && (string)$feedbackContent->Role == 'Buyer'){
				++$positiveCountBuyer;
				$feedBacks['count']['seller']['positive'] = $positiveCountBuyer;

			}
			elseif((string)$feedbackContent->CommentType == 'Negative' && (string)$feedbackContent->Role == 'Buyer'){
				++$negativeCountBuyer;
				$feedBacks['count']['seller']['negative'] = $negativeCountBuyer;
			}
			$feedBacks[] = array((string)$feedbackContent->CommentingUser, (string)$feedbackContent->CommentText, (string)$feedbackContent->CommentType, (string)$feedbackContent->Role);
		}
//		$feedBacks['count']['positive']
		return $feedBacks;
	}

	protected function _prepeareRating($rating){
		$user_id = $_SESSION['m_info']['user_id'];
		$userRatings = array();
		$i = 0;
		foreach ($rating as $ratingDetails){
			if($ratingDetails->RatingDetail){

				//$check = 'SELECT `id` FROM `ebay_rating` WHERE `user_id` = ' . $user_id . ' AND `r_name` = "' . (string)$ratingDetails->RatingDetail . '"';
				//$test = mysql_query($check);
				//$result = mysql_fetch_object($test);
				$query = 'INSERT INTO `ebay_rating_temp`
								VALUES (null,
									' . $user_id . ',
									"' . (string)$ratingDetails->RatingDetail . '",
									"' . (string)$ratingDetails->Rating . '",
									' . (string)$ratingDetails->RatingCount . '
					)';
				mysql_query($query);

//				if (!$result){
//					$query = 'INSERT INTO `ebay_rating`
//								VALUES (null,
//									' . $user_id . ',
//									"' . (string)$ratingDetails->RatingDetail . '",
//									"' . (string)$ratingDetails->Rating . '",
//									' . (string)$ratingDetails->RatingCount . '
//					)';
//					mysql_query($query);
//				}
//				else{
//					$query = 'UPDATE `ebay_rating`
//								SET
//									`average` = "' . (string)$ratingDetails->Rating . '",
//									`r_numbers` = ' . (string)$ratingDetails->RatingCount . '
//								WHERE
//									`id` = ' . $result->id . '
//					';
//					mysql_query($query);
//				}
				foreach($ratingDetails as $name=>$value){
					$userRatings[$i][(string)$name] = (string)$value;
				}
			}
			$i++;
		}
		return $userRatings;
	}

	public function getButtons($userId=0, $sessId=0){
		$buttons='
		<div align="center" style="padding: 10px;">
			<a class="eBaylinks" style="color: black" href="' . $sessId . '">Transfer your ratings from eBay</a>
			<a class="eBaylinks delete" style="color: black" href="ebayfeedback.php?seller_id=' . $userId . '&id=' . $userId . '">
				Delete all information from eBay
			</a>
		</div>';
		return $buttons;
	}

	public function saveEbayInfo($user_id){
//		$user_id = $_SESSION['m_info']['user_id'];
		$queryFeedbacks = 'INSERT INTO `ebay_feedback` SELECT * FROM `ebay_feedback_temp` WHERE `user_id` = ' . $user_id;
		$result_feedback = mysql_query($queryFeedbacks);
		
		$queryDelete = 'DELETE * FROM `ebay_rating` WHERE `user_id` = ' . $user_id;
		mysql_query($queryDelete);
		
		$queryRatings = 'INSERT INTO `ebay_rating` (SELECT * FROM `ebay_rating_temp` WHERE `user_id` = ' . $user_id . ')';
		$result_rating = mysql_query($queryRatings);

		mysql_query("DELETE FROM `ebay_feedback_temp` WHERE `user_id` = $user_id");
		mysql_query("DELETE FROM `ebay_rating_temp` WHERE `user_id` = $user_id");

		if ($result_rating && $result_feedback){
			return true;
		}
		return false;
	}

	public function deleteTemp(){
		$user_id = $_SESSION['m_info']['user_id'];
		$res1 = mysql_query("DELETE FROM `ebay_feedback_temp` WHERE `user_id` = $user_id");
		$res2 = mysql_query("DELETE FROM `ebay_rating_temp` WHERE `user_id` = $user_id");
		if ($res1 && $res2){
			return true;
		}
		return false;
	}

}