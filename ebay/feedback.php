<?php
class FeedBack{ 

	const DELETE_MESSAGE = 'Your information from eBay was deleted';
	const SELLER = 'Seller';
	const BUYER = 'Buyer';

	const POSITIVE = 'Positive';
	const NEGATIVE = 'Negative';

	const LIMIT = 10;

	protected $_userId;

	public function  __construct($userId = null) {
		$this->_userId = $userId;
	}

	public function setUserId($userId){
		$this->_userId = $userId;
	}

	public function deleteAll(){
		if ($this->_userId){
			mysql_query('DELETE FROM `ebay_rating` WHERE `user_id` = ' . $this->_userId);
			mysql_query('DELETE FROM `ebay_feedback` WHERE `user_id` = ' . $this->_userId);
			$message = '<div align="center" style="padding: 10px;">';
			$message .= self::DELETE_MESSAGE;
			$message .= '</div>';
			return $message;
		}
		else{
			return false;
		}
	}

	public function getEbayInformation(){
		$detailInformation = array();
		
		$detailInformation['FeedBack']['Seller'] = $this->_getAllFeddBacks(self::BUYER, self::LIMIT);;
		$detailInformation['FeedBack']['Buyer'] = $this->_getAllFeddBacks(self::SELLER, self::LIMIT);
		
		$detailInformation['Rating'] = $this->_getRating();
		//positive seller
		$positeveFeedBackSeller = $this->_getCountFeedBack(self::BUYER, self::POSITIVE);
		//negative seller
		$negativeFeedbackSeller = $this->_getCountFeedBack(self::BUYER, self::NEGATIVE);
		//positive buyer
		$positeveFeedBackBuyer = $this->_getCountFeedBack(self::SELLER, self::POSITIVE);
		//negative buyer
		$negativeFeedbackBuyer = $this->_getCountFeedBack(self::SELLER, self::NEGATIVE);
		
		$persentSeller = $this->_getPercent((int)$positeveFeedBackSeller->num, (int)$negativeFeedbackSeller->num);
		$summ_seller = ((int)$positeveFeedBackSeller->num) + ((int)$negativeFeedbackSeller->num);

		$persentBuyer = $this->_getPercent((int)$positeveFeedBackBuyer->num, (int)$negativeFeedbackBuyer->num);
		$summ_buyer = (int)$positeveFeedBackBuyer->num + (int)$negativeFeedbackBuyer->num;

		$detailInformation['count']['total']['seller'] = $summ_seller;
		$detailInformation['count']['total']['buyer'] = $summ_buyer;

		$detailInformation['count']['percent']['seller'] = (int)$persentSeller;
		$detailInformation['count']['percent']['buyer'] = (int)$persentBuyer;

		return $detailInformation;
	}

	protected function _getAllFeddBacks($role, $limit=0){		
		$feedBacksQuery = 'SELECT * FROM `ebay_feedback` WHERE `user_id` = ' .  $this->_userId . ' AND `role` = "' . $role . '"';
		$feedBacksQuery .= 'LIMIT 0, ' . $limit;
		$result = mysql_query($feedBacksQuery);
		return $result;
	}

	protected function _getRating(){
		$ratingQuery = 'SELECT * FROM `ebay_rating` WHERE `user_id` = ' . $this->_userId;
		$result = mysql_query($ratingQuery);
		return $result;
	}

	protected function _getCountFeedBack($role, $type){
		$countQuery = 'SELECT COUNT(*) num FROM `ebay_feedback`
											WHERE `user_id` = ' . $this->_userId . '
												AND `type` = "' . $type . '" AND `role` = "' . $role . '"';
		$result = mysql_query($countQuery);
		return 	@mysql_fetch_object($result);
	}

	protected function _getPercent($positive = 0, $negative = 0){
		$summ_feedBack = (int)$positive + (int)$negative;
		if (!$summ_feedBack){
			$percent = 0;
			return $percent;			
		}
		$percent = ((int)$positive/$summ_feedBack) * 100;
		return $percent;
	}

	public function getFeedBacks($role, $limit=0, $all=0){
		$this->_dbConnect();
		$feedBacksQuery = 'SELECT * FROM `ebay_feedback` WHERE `user_id` = ' .  $this->_userId . ' AND `role` = "' . $role . '"';
		if ($limit && !$all){
			$feedBacksQuery .= ' LIMIT ' . $limit . ', ' . self::LIMIT . ';';
		}
		else{
			$feedBacksQuery .= ' LIMIT ' . $limit . ', 1000;';
		}

		$resultQuery = mysql_query($feedBacksQuery);
		$testResult = mysql_fetch_object($resultQuery);
		if ($testResult->from){
			return $resultQuery;
		}
		else{
			return false;
		}
	}

	protected function _dbConnect(){

		$database="brokenauction"; //database name

		$mysql_user = "barberb_barberb"; //database user name

		$mysql_password = "F9extous"; //database user password

		$mysql_host = "localhost";

		$success = mysql_pconnect($mysql_host, $mysql_user, $mysql_password);

		if (!$success)

			die ("<b>Cannot connect to database, check if username, password and host are correct.</b>");

		$success = mysql_select_db($database);

	}
}
?>
