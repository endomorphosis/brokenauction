<?php 
if (isset($_GET['username']) && !empty($_GET['username'])){
	require_once('ebay.php');
	$username = $_GET['username'];
	$secretId = $_GET['SecretID'];
	$eBay = new eBay();
	$token = $eBay->fetchToken($secretId,$username);
	$requestResult = $eBay->getFeedback($username, $token);
	$ratings = $eBay->fetchFeedBack($requestResult);

	$summerSeller = $ratings['FeedBacks']['count']['buyer']['positive'] + $ratings['FeedBacks']['count']['buyer']['negative'];
	$sellerPersent = ($summerSeller > 0 ) ? (($ratings['FeedBacks']['count']['buyer']['positive']/$summerSeller)*100) : 0;

	$summerBuyer = $ratings['FeedBacks']['count']['seller']['positive'] + $ratings['FeedBacks']['count']['seller']['negative'];
	$buyerPersent = ($summerBuyer > 0 ) ? (($ratings['FeedBacks']['count']['seller']['positive']/$summerBuyer)*100) : 0;
?>
<div id="center-column" class="single" >
<?php echo $message; ?>
<!-- FeedBack header -->
  <div class="feddback-header">
	  Feedback Center
  </div>
	<div style="clear:both;"></div>
	<div class="subtitle" style="padding-top:10px;"></div>
	<div id="table-row">
	  <div class="med1" style="font-weight: bold; font-size: 14px;">Seller Feedback</div>
	  <div class="big"></div>
	  <div class="td-clear"></div>
	</div>
	<div id="table-row">
	  <div class="med1" style="width: 155px;">Positive Feedback</div>
	  <div class="big"><?php echo $sellerPersent; ?> %</div>
	  <div class="td-clear"></div>
	</div>
	<div id="table-row">
		<div class="med1" style="width: 155px;" >Total Number Of Feedback: </div>
		<div class="big" ><?php echo $summerSeller; ?></div>
		<div class="big">
		<!--Archived Feedback-->
		</div>
		<div class="td-clear"></div>
	</div>
	<div id="table-row">
	  <div class="med1"></div>
	  <div class="big"></div>
	  <div class="td-clear"></div>
	</div>
	<div id="table-row">
	  <div class="med1" style="font-weight: bold; font-size: 14px;">Buyer Feedback</div>
	  <div class="big"></div>
	  <div class="td-clear"></div>
	</div>
	<div id="table-row">
	  <div class="med1" style="width: 155px;">Positive Feedback:</div>
	  <div class="big">
			<?php echo ($buyerPersent); ?> %
	  </div>
	  <div class="td-clear"></div>
	</div>
	<div id="table-row">
	<div class="med1" style="width: 155px;" >Total Number Of Feedback: </div>
	  <div class="big" ><?php echo ($summerBuyer); ?></div>
	  <div class="big">
		<!--Archived Feedback-->
	  </div>
	  <div class="td-clear"></div>
	</div>
</div>
<br style="clear:both;" />
<br style="clear:both;" />
	<!-- When transfer -->
	<div align="center">Seller feedbacks</div>
	<table  class="sellerRating" border="0" style="width: 100%">
		<tr>
			<th>UserID</th>
			<th>Comment</th>
			<th>Type</th>
		</tr>
		<?php for($i=0, $n=count($ratings['FeedBacks']); $i<$n; $i++):?>
				<?php if ($ratings['FeedBacks'][$i][3] == 'Buyer') continue;?>
				<?php ($ratings['FeedBacks'][$i][2] == 'Positive') ? $color = 'style="color: #99CC00;"' : $color = 'style="color: red;"'; ?>
				<tr <?php echo $color;?>>
					<?php if ($sellerPersent): ?>
						<td><?php echo $ratings['FeedBacks'][$i][0] ?></td>
						<td><?php echo $ratings['FeedBacks'][$i][1] ?></td>
						<td><?php echo $ratings['FeedBacks'][$i][2] ?></td>
					<?php else: ?>
						<td colspan="3" align="center">
							No feedbacks for buyer
						</td>
					<?php endif; ?>
				</tr>
			<?php endfor; ?>
	</table>
	<br /><br />
	<div align="center">Buyer feedbacks</div>
		<table  class="sellerRating" border="0" style="width: 100%">
			<tr>
				<th>UserID</th>
				<th>Comment</th>
				<th>Type</th>
			</tr>
			<?php for($i=0, $n=count($ratings['FeedBacks']); $i<$n; $i++):?>
			<?php if ($ratings['FeedBacks'][$i][3] == 'Seller') {continue;}?>
			<?php ($ratings['FeedBacks'][$i][2] == 'Positive') ? $color = 'style="color: #99CC00;"' : $color = 'style="color: red;"'; ?>
		<tr <?php echo $color;?>>
			<?php if ($buyerPersent): ?>
				<td><?php echo $ratings['FeedBacks'][$i][0] ?></td>
				<td><?php echo $ratings['FeedBacks'][$i][1] ?></td>
				<td><?php echo $ratings['FeedBacks'][$i][2] ?></td>
			<?php else: ?>
				<td colspan="3" align="center">
					No feedbacks for seller
				</td>
			<?php endif; ?>
		</tr>
		<?php endfor; ?>
		</table>
		<br />
		<!-- Ratings when transfer -->
		<table class="sellerRating" border="1">
			<tr>
				<th colspan="3">
					Deteils seller ratings
				</th>
			</tr>
			<tr>
				<td>Criteria</td>
				<td>Average rating</td>
				<td>Number of ratings</td>
			</tr>
			<?php for($i=1, $n=count($ratings['Ratings'])+1; $i<$n; $i++):?>
			<tr>
				<td><?php if ($i==1){ echo 'Item as described';}elseif($i==4){ echo 'Shipping and handling charges';} else {echo $ratings['Ratings'][$i]['RatingDetail'];} ?></td>
				<td>
					<?php
						if((int)$rating->average < 1)
						echo "<img src='images/palm-0.gif'>";
						else if((int)$ratings['Ratings'][$i]['Rating'] > 1 )
						echo "<img src='images/palm-1.gif'>";
						else if((int)$ratings['Ratings'][$i]['Rating'] > 2)
						echo "<img src='images/palm-2.gif'>";
						else if((int)$ratings['Ratings'][$i]['Rating'] > 3 )
						echo "<img src='images/palm-3.gif'>";
						else if((int)$ratings['Ratings'][$i]['Rating'] > 4)
						echo "<img src='images/palm-4.gif'>";
						else if((int)$ratings['Ratings'][$i]['Rating'] >= 5)
						echo "<img src='images/palm-5.gif'>";
					?>
					<?php //echo $ratings['Ratings'][$i]['Rating']; ?>
				</td>
				<td><?php echo $ratings['Ratings'][$i]['RatingCount']; ?></td>
			</tr>
			<?php endfor;?>
		</table>
		<!-- Accept buttons -->
		<div style="text-align: center; margin-top: 10px;">
			<a class="eBaylinks" href="ebayfeedback.php?message=save&seller_id=<?php echo $_SESSION['m_info']['user_id']; ?>">
				Accept and save transfered information from eBay
			</a>
			<a class="eBaylinks delete"
			   href="ebayfeedback.php?seller_id=<?php echo $_SESSION['m_info']['user_id']; ?>&decline=<?php echo $_SESSION['m_info']['user_id']; ?>">
				Decline and delete transfered information from eBay
			</a>
		</div>
<?php
}
?>
