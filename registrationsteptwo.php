<? 
  include('header.php');
  if($_POST['valueOfsteps']=='step1'){
  $_SESSION['register'][$_POST['valueOfsteps']]=$_POST;
  }
  $pageFormsValue=$_SESSION['register']['step2'];
?>
  <div id="main-column">	 		
    <div class="clear">&nbsp;
    </div>		 		
    <div>			
      <div class="h3">				
        <div style="float:left; color:#dc983a;">Add Personal Information
        </div>				
        <div style="float:right; padding-right:10px;">
          <img src="/images/spacer.gif" class="progress-bar" style="background-position:0 -30px;" />
        </div>			
      </div>			
      <div style="padding:14px 10px; position:relative;">				
        <div class="user-register">				
        <script language="javascript">
        function formsubmit(){
        var submitForm=formvalidation(); if(submitForm){ document.step.submit(); }
        }
        function formvalidation(){
          if (emptyvalidation('c_Salutation',"The Salutation is empty. Please type it.")==false) { return false;}
          if (emptyvalidation('c_First_Name',"The First Name is empty. Please type it.")==false) { return false;}
          if (emptyvalidation('c_Last_Name',"The Last Name is empty. Please type it.")==false) { return false;}
          return true;
        }
        </script>	 				
        <? 
          function selectmyselect($val1, $val2){
    				if($val1==$val2){
    				return "selected";
    				}					
  				}
        ?> 				 				 				 				
          <form name="step" id="step" method="post" action="registrationstepthree.php" style="width:740px;">
            <input type="hidden" name='valueOfsteps' value="step2" />				  <h3>Personal Information</h3>				  
            <div> 					
              <span>
                <label for="txtusername*">Salutation *
                </label>
              </span> 									
              <select name="c_Salutation" id="c_Salutation">					
                <option value="Mr" <?=selectmyselect("Mr", $pageFormsValue['c_Salutation'])?>>Mr
                </option>					
                <option value="Mrs" <?=selectmyselect("Mrs", $pageFormsValue['c_Salutation'])?>>Mrs
                </option>					
                <option value="Ms" <?=selectmyselect("Ms", $pageFormsValue['c_Salutation'])?>>Ms
                </option>					
              </select>					  					
              <span class="help-text">Mr? Mrs? Ms?
              </span>				  
            </div> 					  					
            <div> 					
              <span>
                <label for="txtpassword*">First Name *
                </label>
              </span> 					
              <span>
                <input type="text" name="c_First_Name" id="c_First_Name" value="<? echo $pageFormsValue['c_First_Name'] ?>" />
              </span>
              <span class="help-text">
              </span> 					
            </div> 					
            <div> 					
              <span>
                <label for="txtconfirmpassword">Last Name *
                </label>
              </span> 					
              <span> 
                <input type="text" id="c_Last_Name" name="c_Last_Name" value="<? echo $pageFormsValue['c_Last_Name'] ?>" />
              </span>
              <span class="help-text">
              </span> 					
            </div>					<br style="clear: both;" />				  
            <div> 					
              <span>
                <label for="txtsecretquestion*">Address 1 
                </label>
              </span> 					
              <input type="text" id="c_Address_1" name="c_Address_1" value="<? echo $pageFormsValue['c_Address_1'] ?>" /> 				
              <span class="help-text">
              </span> 					
            </div> 					
            <div> 					
              <span>
                <label for="txtanswer*">Address 2
                </label>
              </span>					
              <span>
                <input type="text" id="c_Address_2" name="c_Address_2" value="<? echo $pageFormsValue['c_Address_2'] ?>" />
              </span>
              <span class="help-text">Apartment/Unit number?
              </span> 					
            </div>				     					
            <!-- Country Start --> 					 					 					
            <div> 					 					
              <span>
                <label for="txtconfirmanswer*	">Country 
                </label>
              </span> 					
              <select id="c_Country" name="c_Country" onblur="javascript:setstate(this.options[this.selectedIndex].text);">					
                <option value="" <?=selectmyselect("", $pageFormsValue['c_Country'])?>> Select Country
                </option>					
            <? 					
  					$sqlcountry = "select * from countrylist";
  						$sqlselq = mysql_query($sqlcountry);
  						while($sqlfetchcon  = mysql_fetch_object($sqlselq))
  						{
  						echo "<option value='$sqlfetchcon->Name' ".selectmyselect($sqlfetchcon->Name, $pageFormsValue['c_Country']).">$sqlfetchcon->Name</option>";
  						}					
              ?>					
              </select>			 					 										
            </div>					
            <script>
  					function setstate(val)
  					{
  					//alert(val);
  					if(val=="United States"){
  					
  				var bodystate = '<select id="c_State_Province" name="c_State_Province" ><option value="AL">Alabama</option><option value="AK">Alaska</option><option value="AZ">Arizona</option><option value="AR">Arkansas</option><option value="CA">California</option><option value="CO">Colorado</option><option value="CT">Connecticut</option><option value="DE">Delaware</option><option value="DC">District of Columbia</option><option value="FL">Florida</option><option value="GA">Georgia</option><option value="HI">Hawaii</option><option value="ID">Idaho</option><option value="IL">Illinois</option><option value="IN">Indiana</option>	<option value="IA">Iowa</option><option value="KS">Kansas</option><option value="KY">Kentucky</option><option value="LA">Louisiana</option><option value="ME">Maine</option><option value="MD">Maryland</option><option value="MA">Massachusetts</option><option value="MI">Michigan</option><option value="MN">Minnesota</option><option value="MS">Mississippi</option><option value="MO">Missouri</option><option value="MT">Montana</option><option value="NE">Nebraska</option><option value="NV">Nevada</option><option value="NH">New Hampshire</option><option value="NJ">New Jersey</option><option value="NM">New Mexico</option><option value="NY">New York</option><option value="NC">North Carolina</option><option value="ND">North Dakota</option><option value="OH">Ohio</option>	<option value="OK">Oklahoma</option><option value="OR">Oregon</option><option value="PA">Pennsylvania</option><option value="RI">Rhode Island</option><option value="SC">South Carolina</option><option value="SD">South Dakota</option><option value="TN">Tennessee</option><option value="TX">Texas</option><option value="UT">Utah</option><option value="VT">Vermont</option><option value="VA">Virginia</option><option value="WA">Washington</option><option value="WV">West Virginia</option><option value="WI">Wisconsin</option><option value="WY">Wyoming</option></select>';
  				//alert(bodystate);
  					document.getElementById('mystate').innerHTML= bodystate;
  					}
  					else
  					{
  					document.getElementById('mystate').innerHTML= '<input type="text" id="c_State_Province" name="c_State_Province"/>';
  					}
  					
  					}
  					</script>					
            <!-- Country End -->					
            <div> 					
              <span>
                <label for="txtconfirmanswer*	">City 
                </label>
              </span> 					
              <span>
                <input type="text" id="c_City" name="c_City" value="<? echo $pageFormsValue['c_City'] ?>" />
              </span>					
              <span class="help-text">
              </span> 					
            </div>				  
            <div> 					
              <span>
                <label for="txtconfirmanswer*	">State / Province 
                </label>
              </span> 				
              <span id="mystate" style="display:block">				
                <input type="text" id="c_State_Province" name="c_State_Province" value="<? echo $pageFormsValue['c_State_Province'] ?>" />			
              </span>				
              <span class="help-text">
              </span>					
            </div>					
            <div> 					
              <span>
                <label for="txtconfirmanswer*	">Zip / Postal Code 
                </label>
              </span> 					
              <span> 
                <input type="text" name="c_Zip" id="c_Zip"  value="<? echo $pageFormsValue['c_Zip'] ?>" />
              </span>					
              <span class="help-text">
              </span> 					
            </div>					<br style="clear: both;" />					<h3>Optional Information</h3> 					
            <div> 					
              <span>
                <label for="txtemailaddress*">Home Phone Number
                </label>
              </span> 					
              <span>
                <input type="text" name="c_Home_Phone_Number" id="c_Home_Phone_Number" value="<? echo $pageFormsValue['c_Home_Phone_Number'] ?>" />
              </span>					
              <span class="help-text">
              </span> 					
            </div>					
            <div> 					
              <span>
                <label for="txtconfirmemailadd">Work Phone Number
                </label>
              </span> 					
              <span>
                <input type="text" name="c_Work_Phone_Number" id="c_Work_Phone_Number" value="<? echo $pageFormsValue['c_Work_Phone_Number'] ?>" />
              </span>					
              <span class="help-text">
              </span> 					
            </div>					
            <div> 					
              <span>
                <label for="txtconfirmemailadd">Mobile Phone Number
                </label>
              </span> 					
              <span>
                <input type="text" name="c_Mobile_Phone_Number" id="c_Mobile_Phone_Number" value="<? echo $pageFormsValue['c_Mobile_Phone_Number'] ?>" />
              </span>					
              <span class="help-text">
              </span> 					
            </div>					
            <div> 					
              <span>
                <label for="txtconfirmemailadd">Fax Number
                </label>
              </span> 					
              <span>
                <input type="text" name="c_Fax_Number" id="c_Fax_Number" value="<? echo $pageFormsValue['c_Fax_Number'] ?>" /><br />
              </span>
              <span class="help-text">
              </span>                                					
            </div>					
            <div> 					
              <span>
                <label for="txtconfirmemailadd" style="color:#FF9900; font-size:10px; font-weight: normal;">* Required Fields
                </label>
              </span>					
            </div>					<br style="clear: both;" />				
          </form>				 				 				 				 				 				 				 				
          <div style="float:left; width:100%;">					
            <div class="nav-page" style="float:left;">
              <a href="registrationstepone.php">Previous</a>
            </div>					
            <div class="nav-page">
              <a href="javascript:formsubmit()">Next</a>
            </div>				
          </div>				 				
        </div>			
      </div>		
    </div>	
  </div>
<? //John Harre 9/13/2010 - disabled due to FF bombing. The file is backed up and all contents have been cleared out.
  include('footer.php');?>