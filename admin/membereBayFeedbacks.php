<?php
session_start();
error_reporting(0);
 
include("../cms.php");
include("../ebay/feedback.php");
include("includes/config.php");
//login check
require_once("logincheck.php");
require_once("includes/header.php");

$page_name = 'membereBayFeedbacks.php';
$message = '<script text="type/javascript"> alert ("Information was deleted") </script>';
if (isset($_POST['delete_selected_seller'])){
	$role = 'Buyer';
	$ids = implode(',', $_POST['s_feed']);
	$user_id = $_POST['user_id'];
	deleteSelected($role, $user_id, $ids);
	echo $message;
}
elseif (isset($_POST['delete_all_seller'])){
	$user_id = $_GET['member_id'];
	$role = 'Buyer';
	if (deleteAll($user_id, $role)){
		echo $message;
	}
}
elseif (isset($_POST['delete_selected_buyer'])){
	$role = 'Seller';
	$ids = implode(',', $_POST['b_feed']);
	$user_id = $_POST['user_id'];
	deleteSelected($role, $user_id, $ids);
	echo $message;
}
elseif(isset($_POST['delete_all_buyer'])){
	$user_id = $_GET['member_id'];
	$role = 'Seller';
	if (deleteAll($user_id, $role)){
		echo $message;
	}
}
elseif (isset($_POST['delete_ratings'])){
	$user_id = $_GET['member_id'];
	$query='DELETE FROM `ebay_rating` WHERE `user_id` = ' . $user_id;
	mysql_query($query);
	echo $message;
}

function deleteAll($user_id, $role){
	$query = 'DELETE FROM `ebay_feedback` WHERE `user_id` = ' . $user_id . ' AND `role` = "' . $role . '"';
	mysql_query($query);
	return true;
}

function deleteSelected($role, $user_id, $ids){
	$query = 'DELETE FROM `ebay_feedback` WHERE `user_id` = ' . $user_id . ' AND `role` = "' . $role . '" AND `id` IN (' . $ids . ')' ;
	mysql_query($query);
	return true;
}
?>
<style type="text/css">
	table.sellerRating{
		font-size: 12px;
		padding: 15px;
	}
	table.sellerRating tr td{
		text-align: center;
		padding: 5px;
	}
	table.sellerRating tr th{
		padding: 10px;
		border: 1px solid #fff;
	}
	table.sellerRating{
		margin: 0 auto;
	}
	table.sellerRating tr td.rating{
		text-align: left;
	}
	div.show_feedback{
		text-align: center;
		padding: 10px;
	}
	table.show_feedback{
		width: 300px;
		margin: 0 auto;
	}

	table.show_feedback input{
		cursor: pointer;
	}

	table.show_feedback td{
		text-align: center;
	}
	table.show_feedback a{
		display: inline-block;
		width: 200px;
		background: #4BAAD5;
		color: #fff;
		text-decoration: none;
		margin: 0 auto;
		padding: 5px 0px;
	}
	a.eBaylinks{
		display: inline-block;
		width: 250px;
		background: #4BAAD5;
		color: #fff;
		text-decoration: none;
		margin: 0 auto;
		padding: 5px 0px;
	}

	a.delete{
		background: #FF8517;
	}
	div.show_feedback a:hover{
		color: #000;
	}
	div.feddback-header{
		padding:5px;
		font: bold 13px Arial;
		border:1px solid #ff8e2f;
		background: #ff8e2f url(images/inner-title-bg.gif) repeat-x top;
	}
</style>
<div id="wrapper">
	<div id="page">
		<?php if (isset($_GET['member_id'])): ?>
			<?php
				$feedbacks = new FeedBack($_GET['member_id']);
				$detailInformation = $feedbacks->getEbayInformation();
			?>
		<form name="adminForm" method="post" action="<?php echo $page_name; ?>?member_id=<?php echo $_GET['member_id'];?>">
				<table class="sellerRating" width="100%" cellpadding="0" border="0">
					<tr>
						<th colspan="4">Seller feedbacks</th>
					</tr>
					<tr>
						<th>ID</th>
						<th>UserID</th>
						<th>Comment</th>
						<th>Type</th>
					</tr>
				<?php while ($feedback = @mysql_fetch_object($detailInformation['FeedBack']['Seller'])){ ?>
					<tr>
						<td><input type="checkbox" value="<?php echo $feedback->id; ?>" name="s_feed[]" /></td>
						<td><?php echo $feedback->from; ?></td>
						<td><?php echo $feedback->comment; ?></td>
						<td><?php echo $feedback->type; ?></td>
					</tr>
				<?php }?>
				</table>
				<script type="text/javascript" src="../ebay/js/jquery.js"></script>
				<script type="text/javascript" src="../ebay/js/feedbacks.js"></script>
				<div id="more_s"></div>
				<table class="show_feedback">
					<tr>
						<td><a href="javascript:void(0)" id="show_more_s_admin" title="show more seller feedbacks">Show more</a></td>
						<td><a href="javascript:void(0)" id="show_all_s_admin" title="show all seller feedbacks" onclick="">Show all seller feedback</a></td>
					</tr>
					<tr>
						<td><input type="submit" name="delete_selected_seller" value="Delete selected" /></td>
						<td><input type="submit" name="delete_all_seller" value="Delete all" /></td>
					</tr>
				</table>
				
				<input type="hidden" value="<?php echo ($detailInformation['count']['total']['seller'] - 10); ?>" id="total_seller" />
				<input type="hidden" value="10" id="limit_s" name="limit_s"/>
				<input type="hidden" value="<?php echo $_GET['member_id']; ?>" id="user_id" name="user_id"/>

				<table class="sellerRating" width="100%" cellpadding="0" border="0">
					<tr>
						<th colspan="4">Buyer feedbacks</th>
					</tr>
					<tr>
						<th>ID</th>
						<th>UserID</th>
						<th>Comment</th>
						<th>Type</th>
					</tr>
				<?php while ($feedback = @mysql_fetch_object($detailInformation['FeedBack']['Buyer'])){ ?>
					<tr>
						<td><input type="checkbox" value="<?php echo $feedback->id; ?>" name="b_feed[]" /></td>
						<td><?php echo $feedback->from; ?></td>
						<td><?php echo $feedback->comment; ?></td>
						<td><?php echo $feedback->type; ?></td>
					</tr>
				<?php
				}
				?>
				</table>
				<div id="more_b"></div>
				<table class="show_feedback">
					<tr>
						<td><a href="javascript:void(0)" id="show_more_b_admin" title="show more buyer feedbacks">Show more</a></td>
						<td><a href="javascript:void(0)" id="show_all_b_admin" title="show all buyer feedback">Show all buyer feedback</a></td>
					</tr>
					<tr>
						<td><input type="submit" name="delete_selected_buyer" value="Delete selected" /></td>
						<td><input type="submit" name="delete_all_buyer" value="Delete all" /></td>
					</tr>
				</table>
					
				<input type="hidden" value="10" id="limit_b" name="limit_b" />
				<input type="hidden" value="<?php echo ($detailInformation['count']['total']['buyer'] - 10); ?>" id="total_buyer" />

				<table class="sellerRating" border="0" style="margin-top: 30px;">
					<tr>
						<th colspan="3">
							Details seller ratings
						</th>
					</tr>
					<tr>
						<td>Criteria</td>
						<td>Average rating</td>
						<td>Number of ratings</td>
					</tr>
					<?php while ($rating = @mysql_fetch_object($detailInformation['Rating'])){ ?>
					<tr>
						<td class="rating">
							<?php
							if ($rating->r_name == 'ItemAsDescribed'){
								echo 'Item as described';
							}
							elseif($rating->r_name == 'ShippingAndHandlingCharges'){
								echo 'Shipping and handling charges';
							}
							else{
								echo $rating->r_name;
							}
						?>
						</td>
						<td>
							 <?
							if((int)$rating->average < 1.0)
							echo "<img src='../images/palm-0.gif'>";
							else if((int)$rating->average > 1.0 )
							echo "<img src='../images/palm-1.gif'>";
							else if((int)$rating->average > 2.0)
							echo "<img src='../images/palm-2.gif'>";
							else if((int)$rating->average > 3.0 )
							echo "<img src='../images/palm-3.gif'>";
							else if((int)$rating->average > 4.0)
							echo "<img src='../images/palm-4.gif'>";
							else if((int)$rating->average >= 5.0)
							echo "<img src='../images/palm-5.gif'>";
							?>
							<?php //echo $rating->average; ?>
						</td>
						<td><?php echo $rating->r_numbers; ?></td>
					</tr>					
					<?php }?>
					<tr>
						<td colspan="3"><input type="submit" name="delete_ratings" value="Delete ratings" /></td>
					</tr>
				</table>
			</form>
		<?php endif; ?>
	</div>
</div>
<?php
//include footer
require_once("includes/footer.php");
?>
