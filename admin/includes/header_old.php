<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Auctionopia Admin</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="default.css" rel="stylesheet" type="text/css" />
</head>
<body>

<div id="logo-wrap">
<div id="logo"><img src="images/web-logo.gif" alt="" width="239" height="81" /></div>
</div>

<!-- start header -->
<div id="header">
	<? if(isset($_SESSION['adminuser'])) { ?>
	<div id="menu">
		<ul>
			<li class="current_page_item"><a href="#">Users</a></li>
		  	<li><a href="#">Categories</a></li>
		  	<li><a href="#">Auction Items</a></li>
		  	<li><a href="content.php">Edit Content</a></li>
          	<li><a href="#">Auction Enhancement</a></li>
			<li><a href="change_password.php">Password</a></li>
          	<li class="last" style="float:right;"><a href="logout.php">Logout</a></li>
		</ul>
  </div>
  <? } ?>
</div>
<!-- end header -->