<?
//======================================================================================================================
// database.inc.php
//======================================================================================================================
//---change log-------------------------------------------------------------------------------------------------------b-
// 05/27/10 - fixed notifcation errors
//---change log-------------------------------------------------------------------------------------------------------e-
	include_once(dirname(__FILE__) . "/db_config.inc.php");

	class database{
		var $check_where = true;

		function database($db = ""){
			$this->break_on_err = BREAK_ON_ERR;

			$this->db = ($db == "") ? DEFAULT_DB : $db;
			$this->error = false;

			$link = mysql_connect(DB_SERVER, DB_USER, DB_PASS);

//			$result = mysql_list_tables($this->db, $link);
      $sql1='SHOW TABLES FROM '.$this->db;
			$result = mysql_query($sql1, $link);
			if(!$result) exit("Unable to open DB: $db_name");
			$i = 0;
			while ($i < mysql_num_rows($result)) {
				$this->table[$i] = mysql_tablename($result, $i);
				$i++;
			}
		}

		function listFields($tableName) {
			$this->query("show columns from " . $tableName);
			foreach($this->row as $r){
				$fld[] = $r["Field"];
			}

			Return $fld;
		}

		function set_db($db_name){
			$this->db = $db_name;
		}

		function execute($sql, $print_sql = false){
			unset($this->row);
			$this->sql = $sql;

			$sqlp = SqlParser::ParseString($sql);
			$sqlq = $sqlp->query;
			if($this->check_where){
				if(!isset($sqlq['where']) && key_exists("update", $sqlq)){
					$this->error = "There is no 'where' clause in the provided SQL.";
					$this->output_error();
				}
			}

			if($print_sql){ print "<pre>$sql</pre><hr>"; }
			$link = mysql_connect(DB_SERVER, DB_USER, DB_PASS);
//			$this->results = mysql_db_query($this->db, $sql, $link);
			$this->results = mysql_query($sql, $link);
			if(!$this->results){
				$this->error = mysql_error();
				$this->output_error();
			}
			$pos = stristr($sql, "insert");
			if($pos !== false){
				$this->insert_id = mysql_insert_id();
			}
		}

		function query($sql, $print_sql = false){
			$this->row = array();
			$this->sql = $sql;
			if($print_sql){ print "<pre>$sql</pre><hr>"; }
			$link = mysql_connect(DB_SERVER, DB_USER, DB_PASS);
//			$this->results = mysql_db_query($this->db, $sql, $link);
			$this->results = mysql_query($sql, $link);
			if($this->results){
				if($this->num_rows = mysql_num_rows($this->results)){
					while($r = mysql_fetch_array($this->results)){
						$cur_r = array();
						foreach($r as $k=>$v){
							if(!is_numeric($k)) $cur_r[$k] = $v;
						}
						$this->row[] = $cur_r;
					}
				}
			} else {
				$this->error = mysql_error();
				$this->output_error();
			}
		}

		function output_error(){
			if(!$this->break_on_err) return false;
			die("Error: " . $this->error . "<hr />" . $this->sql);
		}
	}

/**
 * This is a simple sql tokenizer / parser.
 * I needed code to get the total count of a paged recordset, so I wrote this.
 * It seems to handle sane mssql/mysql queries.
 *
 * PROTOTYPE
 *
 * @author Justin Carlson <justin.carlson@gmail.com>
 * @license LGPL
 * @version 0.0.3
 */


class SqlParser {

    var $handle = null;
    public static $querysections = array('alter', 'create', 'drop', 'select', 'delete', 'insert', 'update','from','where','limit','order','group');
    public static $operators = array('=', '<>', '<', '<=', '>', '>=', 'like', 'clike', 'slike', 'not', 'is', 'in', 'between');
    public static $types = array('character', 'char', 'varchar', 'nchar', 'bit', 'numeric', 'decimal', 'dec', 'integer', 'int', 'smallint', 'float', 'real', 'double', 'date', 'datetime', 'time', 'timestamp', 'interval', 'bool', 'boolean', 'set', 'enum', 'text');
    public static $conjuctions = array('by', 'as', 'on', 'into', 'from', 'where', 'with');
    public static $funcitons = array('avg', 'count', 'max', 'min', 'sum', 'nextval', 'currval', 'concat');
    public static $reserved = array('absolute', 'action', 'add', 'all', 'allocate', 'and', 'any', 'are', 'asc', 'ascending', 'assertion', 'at', 'authorization', 'begin', 'bit_length', 'both', 'cascade', 'cascaded', 'case', 'cast', 'catalog', 'char_length', 'character_length', 'check', 'close', 'coalesce', 'collate', 'collation', 'column', 'commit', 'connect', 'connection', 'constraint', 'constraints', 'continue', 'convert', 'corresponding', 'cross', 'current', 'current_date', 'current_time', 'current_timestamp', 'current_user', 'cursor', 'day', 'deallocate', 'declare', 'default', 'deferrable', 'deferred', 'desc', 'descending', 'describe', 'descriptor', 'diagnostics', 'disconnect', 'distinct', 'domain', 'else', 'end', 'end-exec', 'escape', 'except', 'exception', 'exec', 'execute', 'exists', 'external', 'extract', 'false', 'fetch', 'first', 'for', 'foreign', 'found', 'full', 'get', 'global', 'go', 'goto', 'grant', 'group', 'having', 'hour', 'identity', 'immediate', 'indicator', 'initially', 'inner', 'input', 'insensitive', 'intersect', 'isolation', 'join', 'key', 'language', 'last', 'leading', 'left', 'level', 'limit', 'local', 'lower', 'match', 'minute', 'module', 'month', 'names', 'national', 'natural', 'next', 'no', 'null', 'nullif', 'octet_length', 'of', 'only', 'open', 'option', 'or', 'order', 'outer', 'output', 'overlaps', 'pad', 'partial', 'position', 'precision', 'prepare', 'preserve', 'primary', 'prior', 'privileges', 'procedure', 'public', 'read', 'references', 'relative', 'restrict', 'revoke', 'right', 'rollback', 'rows', 'schema', 'scroll', 'second', 'section', 'session', 'session_user', 'size', 'some', 'space', 'sql', 'sqlcode', 'sqlerror', 'sqlstate', 'substring', 'system_user', 'table', 'temporary', 'then', 'timezone_hour', 'timezone_minute', 'to', 'trailing', 'transaction', 'translate', 'translation', 'trim', 'true', 'union', 'unique', 'unknown', 'upper', 'usage', 'user', 'using', 'value', 'values', 'varying', 'view', 'when', 'whenever', 'work', 'write', 'year', 'zone', 'eoc');
    public static $startparens = array('{', '(');
    public static $endparens = array('}', ')');
    public static $tokens = array(',', ' ');
    public $query = '';
    public function __construct() { }

    /**
     * Simple SQL Tokenizer
     *
     * @author Justin Carlson <justin.carlson@gmail.com>
     * @license GPL
     * @param string $sqlQuery
     * @return token array
     */
    public static function Tokenize($sqlQuery,$cleanWhitespace = true) {

        /**
         * Strip extra whitespace from the query
         */
        if($cleanWhitespace) {
         $sqlQuery = ltrim(preg_replace('/[\\s]{2,}/',' ',$sqlQuery));
        }

        /**
         * Regular expression based on SQL::Tokenizer's Tokenizer.pm by Igor Sutton Lopes
         **/
        $regex = '('; # begin group
        $regex .= '(?:--|\\#)[\\ \\t\\S]*'; # inline comments
        $regex .= '|(?:<>|<=>|>=|<=|==|=|!=|!|<<|>>|<|>|\\|\\||\\||&&|&|-|\\+|\\*(?!\/)|\/(?!\\*)|\\%|~|\\^|\\?)'; # logical operators
        $regex .= '|[\\[\\]\\(\\),;`]|\\\'\\\'(?!\\\')|\\"\\"(?!\\"")'; # empty single/double quotes
        $regex .= '|".*?(?:(?:""){1,}"|(?<!["\\\\])"(?!")|\\\\"{2})|\'.*?(?:(?:\'\'){1,}\'|(?<![\'\\\\])\'(?!\')|\\\\\'{2})'; # quoted strings
        $regex .= '|\/\\*[\\ \\t\\n\\S]*?\\*\/'; # c style comments
        $regex .= '|(?:[\\w:@]+(?:\\.(?:\\w+|\\*)?)*)'; # words, placeholders, database.table.column strings
        $regex .= '|[\t\ ]+';
        $regex .= '|[\.]'; #period

        $regex .= ')'; # end group

        // get global match
        preg_match_all( '/' . $regex . '/smx', $sqlQuery, $result );

        // return tokens
        return $result[0];

    }

    /**
     * Simple SQL Parser
     *
     * @author Justin Carlson <justin.carlson@gmail.com>
     * @license LGPL
     * @param string $sqlQuery
     * @param bool optional $cleanup
     * @return SqlParser Object
     */
    public static function ParseString($sqlQuery) {

        // returns a SqlParser object
        if (! isset( $this )) {
            $handle = new SqlParser();
        } else {
            $handle = $this;
        }

        // copy and cut the query
        $tokens = self::Tokenize( $sqlQuery );
        $tokenCount = count( $tokens );
        $queryParts = array();
        $section = $tokens[0];

        // parse the tokens
        for ($t = 0; $t < $tokenCount; $t ++) {
            if (in_array( $tokens[$t], self::$startparens )) {

                $sub = $handle->readsub( $tokens, $t );
                $handle->query[$section].= $sub;

            } else {

                if(in_array(strtolower($tokens[$t]),self::$querysections) && !isset($handle->query[$tokens[$t]])) {
                    $section = strtolower($tokens[$t]);
                }

                // rebuild the query in sections
                if (!isset($handle->query[$section])) $handle->query[$section] = '';
//                if ($handle->query[$section]=='' ) $handle->query[$section] = '';
                $handle->query[$section] .= $tokens[$t];

            }

        }

        return $handle;

    }

     /**
     * Parses a section of a query ( usually a sub-query or where clause )
     *
     * @param array $tokens
     * @param int $position
     * @return string section
     */
    private function readsub($tokens, &$position) {

        $sub = $tokens[$position];
        $tokenCount = count( $tokens );
        $position ++;
        while ( ! in_array( $tokens[$position], self::$endparens ) && $position < $tokenCount ) {

            if (in_array( $tokens[$position], self::$startparens )) {
//                $sub.= $this->readsub( $tokens, &$position );
                $sub.= $this->readsub( $tokens, $position );
                $subs++;
            } else {
                $sub.= $tokens[$position];
            }
            $position ++;
        }
        $sub.= $tokens[$position];
        return $sub;
    }


    /**
     * Returns manipulated sql to get the number of rows in the query.
     *
     * @author Justin Carlson <justin.carlson@gmail.com>
     * @license LGPL
     * @return string sql
     */
    public function getCountQuery() {

        $this->query['select'] = 'select count(*) as `count` ';
        unset($this->query['limit']);
        #die(implode('',$this->query));
        return implode('',$this->query);

    }

    /**
     * Returns manipulated sql to get the unlimited number of rows in the query.
     *
     * @author Justin Carlson <justin.carlson@gmail.com>
     * @license LGPL
     * @return string sql
     */
    public function getLimitedCountQuery() {

        $this->query['select'] = 'select count(*) as `count` ';
        return implode('',$this->query);

    }

    /**
     * Returns the select section of the query.
     *
     * @author Justin Carlson <justin.carlson@gmail.com>
     * @license LGPL
     * @return string sql
     */
    public function getSelectStatement() {

        return $this->query['select'];

    }

    /**
     * Returns the from section of the query.
     *
     * @author Justin Carlson <justin.carlson@gmail.com>
     * @license LGPL
     * @return string sql
     */
    public function getFromStatement() {

        return $this->query['from'];

    }

    /**
     * Returns the where section of the query.
     *
     * @author Justin Carlson <justin.carlson@gmail.com>
     * @license LGPL
     * @return string sql
     */
    public function getWhereStatement() {

        return $this->query['where'];

    }

    /**
     * Returns the limit section of the query.
     *
     * @author Justin Carlson <justin.carlson@gmail.com>
     * @license LGPL
     * @return string sql
     */
    public function getLimitStatement() {

        return $this->query['limit'];

    }

    /**
     * Returns the where section of the query.
     *
     * @author Justin Carlson <justin.carlson@gmail.com>
     * @license LGPL
     * @return string sql
     */
    public function get($which) {

        if(!isset($this->query[$which])) return false;
        return $this->query[$which];

    }

    /**
     * Returns the where section of the query.
     *
     * @author Justin Carlson <justin.carlson@gmail.com>
     * @license LGPL
     * @return string sql
     */
    public function getArray( ) {

        return $this->query;

    }
}


//Handle offline chat
	function sendOfflineChat($chat_id, $sender){

$fp = fopen(dirname(__FILE__) . "/mc_test.txt", "a");
fwrite($fp, date("Y-m-d H:i:s") . " :: sendOfflineChat :: " . $sender . "\n\n");
fclose($fp);

		$msg_text = array();
		$db = new database;
		$db->query("select * from chat_main where chat_id = '" . $chat_id . "'");
		$row = $db->row;
		foreach($row as $r){
			if(stristr($r['text'], "The user is offline. They will get this message in their inbox.")){
				$senderid = $r['fromclient'];
				$db->query("select username from auction_users where user_id = '" . $senderid . "'");
				$sendername = $db->row[0]['username'];
				$j = json_decode($r['text']);
				foreach($j as $i){
					if($i->user == $sendername) $msg_text[] = $i->message;
				}
			} else {
				$receverid = $r['fromclient'];
			}
		}
/*
$fp = fopen(dirname(__FILE__) . "/mc_test.txt", "a");
fwrite($fp, "Chat ID: " . $chat_id . "\nReceiver ID: " . $receverid . "\nSender ID: " . $senderid . "\nMessage: " . implode(" ", $msg_text) . "\n\n================\n\n");
fclose($fp);
 */
		if(count($msg_text) > 0){
			$subject = "Offline Message from " . $sendername;
			$msg = implode(" ", $msg_text);

			$db->execute("insert into messagehistory (
									receverid,
									senderid,
									subject,
									msg,
									datetime
						) values (
								'" . $receverid . "',
								'" . $senderid . "',
								'" . $subject . "',
								'" . $msg . "',
								'" . date("Y-m-d H:i:s") . "')"
						);
		}
	}
?>