<?php

include('connect.php');

$username = $_POST['username'];
$userId = $_POST['user_id'];
$contactId = $_POST['contact_id'];
$productId = $_POST['product_id'];
$userStatus = $_POST['user_status'];
$timestamp = date('U');

if ( $productId > 0 ){

	$query = "SELECT chat_id FROM chat_main WHERE product_id={$productId}";
	$result = mysql_query($query) or die(mysql_error());
	if ( mysql_num_rows($result) > 0) {
		//CHAT EXISTS--------------------------------------------------------------------------------------------------------------------------------------------------------
		$id = mysql_fetch_assoc($result);

		$chatId = $id['chat_id'];
		//GET THE USERS IN THE CURRENT CHAT

		$message = ucfirst($username) . ' has just entered the room';
		$newMessageArr = array( 0 => array('user' => 'admin', 'message' => $message , 'color' => '#ffaa07'));
		$initialMessage = json_encode($newMessageArr);
		$newMessage = ',{"user": "admin","message" : "'.$message.'" , "color" : "#ffaa07"}]';

		$query = "SELECT users FROM chat_users WHERE chat_id={$chatId}";
		$result = mysql_query($query) or die(mysql_error());
		$users = mysql_fetch_assoc($result);

		$JSONUsers = json_decode($users['users'], true);

		foreach ($JSONUsers as $uId){

			$getQuery = "SELECT text FROM chat_main WHERE fromclient='{$uId}' AND product_id='{$productId}'";
			$getResult = mysql_query($getQuery) or die(mysql_error());

			$text = mysql_fetch_array($getResult);
			$cutText = substr($text[0], 0 , -1);
			$new = $cutText . $newMessage;
			//$oldText = json_decode($text[0],true);
			//array_push($oldText, $newMessage);
			//$new = json_encode($oldText);

			if ( $uId != $userId){
				$updateQuery = "UPDATE chat_main SET text='{$new}', status='new' WHERE fromclient='{$uId}' AND product_id='{$productId}'";
				$updateResult = mysql_query($updateQuery) or die(mysql_error());
			} else {
				$updateQuery = "UPDATE chat_main SET text='{$new}' WHERE fromclient='{$uId}' AND product_id='{$productId}'";
				$updateResult = mysql_query($updateQuery) or die(mysql_error());
			}

				
		}


		//ADD THIS USER TO THE USER LIST
		$usersList = substr($users['users'], 0 , -1);

		$newUsers = $usersList . ',"'.$userId.'"]';

		//UPDATE THE ENTRY
		$query = "UPDATE chat_users SET users='{$newUsers}' WHERE chat_id={$id['chat_id']}";
		$result = mysql_query($query) or die(mysql_error());


		$query = "INSERT INTO chat_main (chat_id, user_name, fromclient, product_id, time, text, status) VALUES ($chatId, '$username', '$userId', '$productId', $timestamp, '$initialMessage', 'normal')";
		$result = mysql_query($query) or die(mysql_error());

		//GET THE INFO TO CREATE THE TAB
		$query = "SELECT bidamount FROM bid WHERE pid={$productId} ORDER BY bidamount ASC LIMIT 1";
		$bidResult = mysql_query($query) or die(mysql_error());

		if ($bid = mysql_num_rows($bidResult) > 0){
			$bidArr = mysql_fetch_assoc ( $bidResult );
			$bid = $bidArr['bidamount'];
		} else {
			$bid = '';
		}
		$query = "SELECT start_price, auction_duration_end, title, image_1, AuctionType FROM products WHERE product_id={$productId} LIMIT 1";
		$productResult = mysql_query($query) or die (mysql_error());

		$productInfo = mysql_fetch_assoc ( $productResult );
		
		//$cleanTitle = str_replace(array("'", '"'), '', )
		$productTitle = str_replace(array("'", '"'), '',$productInfo['title'] );
		$productDivId = getId($productInfo['title']).'-'.$chatId;
		
		$json = array('start_price' => $productInfo['start_price'], 'auction_duration_end' => $productInfo['auction_duration_end'], 'title' => $productTitle, 'bidamount' => $bid, 'product_id' => $productId, 'tab_id'=>$productDivId, 'message' => $initialMessage, 'chat_id'=>$chatId, 'image' => $productInfo['image_1'], 'auction_type' => $productInfo['AuctionType'] );


		$j = json_encode($json);
		echo $j;

	} else { // NUM ROWS ELSE
		// START BRAND NEW CHAT -------------------------------------------------------------------------------------------------------------------------------------------------
		$query = "SELECT username FROM auction_users WHERE user_id={$contactId}";
		$result = mysql_query($query) or die(mysql_error());
		$rowName = mysql_fetch_assoc($result);
		$contactUsername = $rowName['username'];

		$message1 = ucfirst($contactUsername) . ' has just entered the room';
		$message = ucfirst($username) . ' has just entered the room';

		//GET THE CHAT ID OF CHAT THAT IS BEING CREATED
		$query = "SELECT id FROM chat_main ORDER BY id DESC LIMIT 1";
		$result = mysql_query($query) or die(mysql_error());
		$id = mysql_fetch_assoc($result);
		$chatId = $id['id'] + 1;

		//INSERT THE NEW CHAT INFO


		$newJSONUsers = json_encode(array($userId));


		if ( $userId != $contactId){
			$messageArr = array(0 => array('user' => 'admin', 'message' => $message, 'color' => '#ffaa07'), 1=> array('user' => 'admin', 'message' => $message1, 'color' => '#ffaa07'));
			$initialMessage = json_encode($messageArr);
			$newJSONUsers = json_encode(array($contactId, $userId));

			//INIT THE CONTACT CHAT AS WELL
			$query = "INSERT INTO chat_main (chat_id, user_name, fromclient, product_id, time, text, status, pinged) VALUES ($chatId, '$contactUsername', '$contactId', '$productId', $timestamp, '$initialMessage', 'alert', '0')";
			$result = mysql_query($query) or die(mysql_error());

			$query = "INSERT INTO chat_main (chat_id, user_name, fromclient, product_id, time, text, status) VALUES ($chatId, '$username', '$userId', '$productId', $timestamp, '$initialMessage', 'normal')";
			$result = mysql_query($query) or die(mysql_error());

			$query = "INSERT INTO chat_users (chat_id, users) VALUES ('$chatId', '$newJSONUsers')";
			$result = mysql_query($query) or die(mysql_error());
		} else {
			$messageArr = array(0 => array('user' => 'admin', 'message' => $message , 'color' => '#ffaa07'));
			$initialMessage = json_encode($messageArr);
			$newJSONUsers = json_encode(array($userId));

			//INIT THE CONTACT CHAT AS WELL
			$query = "INSERT INTO chat_main (chat_id, user_name, fromclient, product_id, time, text, status) VALUES ($chatId, '$contactUsername', '$contactId', '$productId', $timestamp, '$initialMessage', 'normal')";
			$result = mysql_query($query) or die(mysql_error());

			$query = "INSERT INTO chat_users (chat_id, users) VALUES ($chatId, '$newJSONUsers')";
			$result = mysql_query($query) or die(mysql_error());
		}

		//INSERT THE USERS OF THE NEW CHAT


		//CREATE NEW CHAT
		//GET INFO FOR NEW CHAT

		$query = "SELECT bidamount FROM bid WHERE pid={$productId} ORDER BY bidamount DESC LIMIT 1";
		$bidResult = mysql_query($query) or die(mysql_error());

		if ($bid = mysql_num_rows($bidResult) > 0){
			$bidArr = mysql_fetch_assoc ( $bidResult );
			$bid = $bidArr['bidamount'];
		} else {
			$bid = '';
		}
		$query = "SELECT start_price, auction_duration_end, title, image_1, AuctionType FROM products WHERE product_id={$productId} LIMIT 1";
		$productResult = mysql_query($query) or die (mysql_error());
		$productInfo = mysql_fetch_assoc ( $productResult );
			
		$productTitle = str_replace(array(',', '-', '.', '/'), ' ', $productInfo['title']);
		
		$productImage = str_replace(' ', '%20', $productInfo['image_1']);
		
		$productTitle = str_replace(array("'", '"'), '',$productInfo['title'] );
		$productDivId = getId($productInfo['title']).'-'.$chatId;
		
		$is_seller = ($userId == $contactId) ? 1 : 0;
		$json = array('is_seller' => $is_seller, 'start_price' => $productInfo['start_price'], 'auction_duration_end' => $productInfo['auction_duration_end'], 'title' => $productTitle, 'bidamount' => $bid, 'product_id' => $productId, 'message' => $initialMessage, 'tab_id' => $productDivId, 'chat_id'=>$chatId, 'image' => $productImage, 'auction_type' => strtolower($productInfo['AuctionType']) );

		$j = json_encode($json);
		echo $j;

	}
} else {
	// ONE ON ONE CHAT
	$query = "SELECT username, image FROM auction_users WHERE user_id={$contactId}";
	$result = mysql_query($query) or die(mysql_error());
	$rowName = mysql_fetch_assoc($result);
	$contactUsername = $rowName['username'];
	
	$userImage = $rowName['image'];
	
	$message1 = ucfirst($contactUsername) . ' has just entered the room';
	$message = ucfirst($username) . ' has just entered the room';

		$q1 = "SELECT * FROM chat_main WHERE `fromclient` = '$userId' AND `tochatclient` = '$contactId'";
		$r1 = mysql_query($q1) or die(mysql_error());
		$q2 = "SELECT * FROM chat_main WHERE `tochatclient` = '$userId' AND `fromclient` = '$contactId'";
		$r2 = mysql_query($q2) or die(mysql_error());
		if(mysql_num_rows($r1) > 0 && mysql_num_rows($r2) > 0){
		  if(!$r1){ $channel = $r2['private_channel'];}
		  else{ $channel = $r1['private_channel']; }
		$chatId = $channel;
		}
		else{


		$query = "SELECT chat_id FROM chat_main WHERE `chat_id` >  500000000 ORDER BY `chat_id` DESC LIMIT 0 , 1 " ;
		$result = mysql_query($query) or die(mysql_error());
		     
		if($result['chat_id'] > 500000000){
		$chatId = $result['chat_id'] + 1;
		}
		else {$chatId = 500000000;}

		$messageArr = array(0 => array('user' => 'admin', 'message' => $message, 'color' => '#ffaa07'), 1=> array('user' => 'admin', 'message' => $message1, 'color' => '#ffaa07'));
		
		$initUserArr = array(0 => array('user' => 'admin', 'message' => $message, 'color' => '#ffaa07'), 1=> array('user' => 'admin', 'message' => $message1, 'color' => '#ffaa07'));
		if ( strtolower($userStatus) == 'user_offline'){
			$offlineMessage = 'The user is offline. They will get this message in their inbox.';
			$initUserArr[2] = array('user' => 'admin', 'message' => $offlineMessage, 'color' => '#ffaa07');
		}
		
		$initialMessage = json_encode($messageArr);
		$initialMessageInit = json_encode($initUserArr);
		$newJSONUsers = json_encode(array($contactId, $userId));

		//INIT THE CONTACT CHAT AS WELL
		$query = "INSERT INTO chat_main (chat_id, user_name, fromclient, product_id, time, text, status) VALUES ($chatId, '$contactUsername', '$contactId', '$productId', $timestamp, '$initialMessage', 'new')";
		$result = mysql_query($query) or die(mysql_error());

		$query = "INSERT INTO chat_main (chat_id, user_name, fromclient, tochatclient, product_id, time, text, status) VALUES ($chatId, '$username', '$userId', '$contactId', '$productId', $timestamp, '$initialMessageInit', 'normal')";
		$result = mysql_query($query) or die(mysql_error());
		$channel = mysql_insert_id($result);
		
		$query = "INSERT INTO chat_users (chat_id, users) VALUES ('$chatId', '$newJSONUsers')";
		$result = mysql_query($query) or die(mysql_error());

	      







		}

	//GET THE CHAT ID OF CHAT THAT IS BEING CREATED



		

	//CREATE NEW CHAT
	//GET INFO FOR NEW CHAT

	/*$query = "SELECT bidamount FROM bid WHERE pid={$productId} ORDER BY bidamount ASC LIMIT 1";
	$bidResult = mysql_query($query) or die(mysql_error());

	if ($bid = mysql_num_rows($bidResult) > 0){
		$bidArr = mysql_fetch_assoc ( $bidResult );
		$bid = $bidArr['bidamount'];
	} else {
		$bid = '';
	}
	$query = "SELECT start_price, auction_duration_end, title, image_1 FROM products WHERE product_id={$productId} LIMIT 1";
	$productResult = mysql_query($query) or die (mysql_error());
	$productInfo = mysql_fetch_assoc ( $productResult );
		
	$productTitle = str_replace(array(',', '-', '.', '/'), ' ', $productInfo['title']);
		
	$json = array('start_price' => $productInfo['start_price'], 'auction_duration_end' => $productInfo['auction_duration_end'], 'title' => $productTitle, 'bidamount' => $bid, 'product_id' => $productId, 'message' => $initialMessage, 'chat_id'=>$chatId, 'image' => $productInfo['image_1'] );

		
	$j = json_encode($json);
	echo $j;*/
//	$query = "INSERT INTO  `auctiono_auction`.`notify` ( `user_id` , `room` , `type`, `notification` ) VALUES (  '$contactId',  '$chatId',  'join', '$username' )";
//	$result = mysql_query($query);

	echo json_encode(array('type' => 'one_on_one', 'tab_name' => $contactUsername, 'chat_id' => $chatId, 'message'=>$initialMessage, 'image'=>$userImage));
}


function getId($value){
	$underline = str_replace(array(',', '-', '.', '/', "'", '"'), '_', $value);	
	//$productTitleArr = str_replace(' ', '_', $productTitle);
	
	//$underline = str_replace(' ', '_', $productTitleArr);
	$ID = substr($underline, 0 , 10);
	
	return $ID;
}

mysql_close();

?>