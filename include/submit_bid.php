<?php

include('connect.php');
$userId = $_POST['user_id'];
$productId = $_POST['product_id'];
$bidAmount = $_POST['bid_amount'];
$username = $_POST['username'];
$chatId = $_POST['chat_id'];
$chatType = $_POST['chat_type'];
$expired = false;
//$fullBid = $bidAmount . '.00';

$brokenBid = explode('.', $bidAmount);

if (count($brokenBid) > 1){
	$fullBid = $bidAmount;
} else {
	$fullBid = $bidAmount . '.00';
}

$date = date('U');
$returnArr = array();

$startQuery = "SELECT start_price, user_id FROM products WHERE product_id={$productId}";
$startResult = mysql_query($startQuery) or die(mysql_error());
$startPrice = mysql_fetch_array($startResult);

$query = "SELECT bidamount FROM bid WHERE pid={$productId} ORDER BY bidamount DESC LIMIT 1";
$result = mysql_query($query) or die (mysql_error());

if ( mysql_num_rows($result) > 0){
	//THERE IS A BID ON PRODUCT CHECK IS THE NEW BID IS GREATER
	$currentBid = mysql_fetch_array($result);
	
	if ( $startPrice[0] >= $fullBid ){
		$returnArr = array('response' => 'Please bid higher than the starting price');
	} else if ( $currentBid[0] >= $fullBid){
		$returnArr = array('response' => 'Someone has a higher bid, please try again!');
	} else {
		$bidd = '$'.$fullBid;
		$newMessage = ',{"user": "admin","message" : "'.$username.' has just bid '.$bidd.'", "color":"#ffaa07"}]';
		addBidMessage($chatId, $newMessage );
		$insertQuery = "INSERT INTO bid (pid, uid, bidamount, biddate, status) VALUES ({$productId}, {$userId}, {$fullBid}, {$date}, 1)";
		$insertResult = mysql_query($insertQuery) or die(mysql_error());
		
		//get current time
		$date = date('U');
		$ctQuery = "SELECT auction_duration_end FROM products WHERE product_id={$productId} LIMIT 1";
		$ctResult = mysql_query($ctQuery) or die(mysql_error());
		$ctArr = mysql_fetch_array($ctResult);
		$timeLeft = $ctArr[0] - $date;
		if ( $timeLeft >= 120 ){
			$reset = $date + 120;
			$timeMessage = ',{"user": "admin","message" : "The 2:00 timer has been initiated!", "color":"#ff0000"}]';
			addBidMessage($chatId, $timeMessage);
		} else if ( $timeLeft >= 31 && $timeLeft < 120 ){
			//reset timer to 120 from right now
			$reset = $date + 120;
			//$reset = $date;
			$timeMessage = ',{"user": "admin","message" : "The timer has been reset to 2:00", "color":"#ff0000"}]';
			addBidMessage($chatId, $timeMessage);
		} else if ( $timeLeft > 0 && $timeLeft < 30 ){
			//reset timer to 30
			$reset = $date + 30;
			//$resetQuery = "UPDATE products SET auction_duration_end={$reset} WHERE product_id={$productId}";
			//$resetResult = mysql_query($resetQuery);
			$timeMessage = ',{"user": "admin","message" : "The timer has been reset to 30 seconds hurry up!", "color":"#ff0000"}]';
			addBidMessage($chatId, $timeMessage);
		} else {
			$expired = true;
			
			
			//timer is expired just refresh chat	
		}
		
		
		
		$resetQuery = "UPDATE products SET auction_duration_end={$reset} WHERE product_id={$productId}";
		$resetResult = mysql_query($resetQuery);
		
		if( !$expired ){
			$returnArr = array('response' => 'You have successfully placed a bid');
		} else {
			$returnArr = array('response' => 'Update chat, Auction done');
		}
		
		$returnArr['reset'] = $reset;
		
	}
} else {
	
	//THERE IS NO BID SUBMIT NEW BID
	$bidd = '$'.$fullBid;
	$newMessage = ',{"user": "admin","message" : "'.$username.' has just bid '.$bidd.'", "color":"#ffaa07"}]';
	addBidMessage($chatId, $newMessage );
	$insertQuery = "INSERT INTO bid (pid, uid, bidamount, biddate, status) VALUES ({$productId}, {$userId}, {$fullBid}, {$date}, 1)";
	$insertResult = mysql_query($insertQuery) or die(mysql_error());
	
	$date = date('U');
	$newEnd = $date + 120;
	$timeQuery = "UPDATE products SET auction_duration_end={$newEnd} WHERE product_id={$productId}";
	$timeResult = mysql_query($timeQuery);
	
	$timeMessage = ',{"user": "admin","message" : "The 2:00 timer has been initiated!", "color":"#ffaa07"}]';
	addBidMessage($chatId, $timeMessage);
	
	$returnArr = array('response' => 'You have successfully placed a bid');
}






function addBidMessage($_chatId, $_message){
	$query = "SELECT users FROM chat_users WHERE chat_id={$_chatId} ORDER BY id LIMIT 1";
	$result = mysql_query($query) or die(mysql_error());
	$fetch = mysql_fetch_assoc($result);
	$userArr = json_decode($fetch['users'], true);
	foreach ($userArr as $uId){
	
			$getQuery = "SELECT text FROM chat_main WHERE fromclient={$uId} AND chat_id={$_chatId}";
			$getResult = mysql_query($getQuery) or die(mysql_error());
			
			$text = mysql_fetch_array($getResult);
			$cutText = substr($text[0], 0 , -1);
			$new = $cutText . $_message;
			//$oldText = json_decode($text[0],true);
			//array_push($oldText, $newMessage);
			//$new = json_encode($oldText);
			
				if ( $uId != $_chatId){
					$updateQuery = "UPDATE chat_main SET text='{$new}', status='new' WHERE fromclient='{$uId}' AND chat_id={$_chatId}";
					$updateResult = mysql_query($updateQuery) or die(mysql_error());
				} else {
					$updateQuery = "UPDATE chat_main SET text='{$new}' WHERE fromclient='{$uId}' AND chat_id={$_chatId}";
					$updateResult = mysql_query($updateQuery) or die(mysql_error());
				}
			
		
	}
}




$json = json_encode($returnArr);
echo $json;

mysql_close();

?>