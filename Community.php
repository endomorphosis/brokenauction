<? 
  include('header.php');
?>
  <ul class="breadcrumb"> </ul>
<? 
  include('leftLinks.php');
?>
  <style>	
  .comm-module { padding:5px; margin-bottom:15px;display:block; font-size:12px; width:350px;}	
  .comm-date {font-size:11px; color:#00CC66;}
  </style>
  <div id="center-column" class="single">
    <div style="width:400px; float:left;">
      <div style="padding:5px; width:350px; float:left; font: bold 13px Arial; border:1px solid #ff8e2f; background: #ff8e2f url(/images/inner-title-bg.gif) repeat-x top;">News & Announcements
      </div>
      <div style="clear:both;height:10px;"></div>
      <div>
      <span class="subtitle">Recent News & Announcements</span>
      <?
        //news
        $newssql="select * from news where status ='1' order by newsid desc";
        $news=$cms->getresults($newssql);
        if(count($news)>0){
        foreach($news as $val){
      ?>
      <span class="comm-module">
      <span class="comm-date">
      <? 
        //John Harre - does adate need quotes? It's not an ID.
        $dd=explode("-",$val['adate']);
        //John Harre - these are integers, so they will not get quotes.
        echo $dd[1].".".$dd[2].".".$dd[0]; 
      ?>
      </span> 
      <br />
      <a href="news_details.php?id=<?=$val['newsid']?>">
        <b> 
        <? 
          //John Harre - does title need quotes? It's not an ID. 
          echo $val['title']; 
        ?>
        </b>
      </a>
      <br />
      <? echo substr(nl2br($val['description']),0,100); ?>
      </span>
      <? } } else { ?>
      <span class="comm-module"> 		   No news found.
      </span>
      <? } ?>
      </div>
    </div>
    <div style="width:360px; float:left;">
    <div style="padding:5px; width:350px; float:left; font: bold 13px Arial; border:1px solid #ff8e2f; background: #ff8e2f url(/images/inner-title-bg.gif) repeat-x top;">Blog
    </div>
    <div style="clear:both;height:10px;"></div>
      <div>
      <span class="subtitle">Recent Blog</span>
      <?
        //blog
        $blogsql="select * from blog where status ='1' order by blogid desc";
        $blog=$cms->getresults($blogsql);
        if(count($blog)>0){
        foreach($blog as $val){
      ?>
      <span class="comm-module">
      <span class="comm-date">
      <? 
        $dd=explode("-",$val['adate']);
        echo $dd[1].".".$dd[2].".".$dd[0]; ?>
      </span> 
      <br />
      <a href="blog_details.php?id=<?=$val['blogid']?>"><b>
      <? echo $val['title']; ?></b></a><br />
      <? echo substr(nl2br($val['description']),0,100); ?>
      </span>
      <? } } else { ?>
      <span class="comm-module">No blog found.</span>
      <? } ?>
      </div>
    </div>
  </div>
<? //John Harre 9/13/2010 - disabled due to FF bombing. The file is backed up and all contents have been cleared out.
  include('footer.php');?>