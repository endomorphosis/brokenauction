  <div class="popup-tool-new" id="popup_tool_1" style="top:765px; left:380px; display:none;" onmouseout="ppupHide();">A featured product is shown directly on the homepage to allow maximum exposure for your item. When a user visits Auctionopia, they see all of the featured products and have the ability to scroll through each of them 
  </div>
  <div class="popup-tool-new" id="popup_tool_2" style="top:795px; left:453px; display:none;" onmouseout="ppupHide();">A bolded listing will change your text to more pronounced, bolded orange (instead of a traditional white). It is a very cost affordable and efficient enhancement. 
  </div> 
  <div class="popup-tool-new" id="popup_tool_3" style="top:825px; left:430px; display:none" onmouseout="ppupHide();">A highlighted listing will instantly attract attention to your product. When a user is browsing through a category, they will see a strong orange fill color over your productís information. It is our most eye catching enhancement. 
  </div> 
  <div class="popup-tool-new" id="popup_tool_4" style="top:855px; left:443px; display:none"  onmouseout="ppupHide();">The border enhancement will create a vivid green border around your listing. This is another great differentiator, as it also attracts much attention to your product. 
  </div> 
  <div class="popup-tool-new" id="popup_tool_5" style="top:895px; left:405px; display:none;width:600px;height:400px" onmouseout="ppupHide();"><strong>Traditional vs. IM Bidding</strong>
    <br>
    <br>The main differences between traditional vs. IM bidding are in the timing and the method of communication. In IM Bidding you communicate, bid and negotiate over the instant messenger. Every bid resets the timer of the running auction to 2 minutes. Every bid made after less than 30 seconds remain, resets the timer of the running auction to 30 seconds. 
    <br>
    <br>Traditional bidding is similar to a bidding structure on other sites where you have a longer timeframe. However, the buyer and the seller can still communicate over CHAT.  
    <br>
    <br>
  </div>