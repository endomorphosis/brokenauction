<?
//======================================================================================================================
// footer.php
//======================================================================================================================
//---change log-------------------------------------------------------------------------------------------------------b-
// 05/27/10 - fixed notifcation errors
//---change log-------------------------------------------------------------------------------------------------------e-       
?>
<div id="footer">
  <div style="float: left;">
    <a href="<? echo $cms->HeaderLinks['aboutUs']?>">About Us</a> | 
    <a href="<? echo $cms->HeaderLinks['help']?>">Help &amp; FAQs</a>| 
    <a href="<? echo $cms->HeaderLinks['contactUs']?>">Contact Us</a> | 
    <a 	href="javascript:p1()">Disclaimer</a> | 
    <a 	href="<? echo $cms->HeaderLinks['jobs']?>">Jobs</a> | 
    <a 	href="privacy_policy.php">Privacy Policy</a> | 
    <a 	href="seller_agreement.php">Seller's Agreement</a> | 
    <a 	href="terms_of_use.php">Terms of Use</a>
  </div>
  <div style="float: right;">&copy; 
    <?=date("Y", time()) ?> Auctionopia. All rights reserved.
  </div>
</div>                                                                
<div 	style="clear: both; text-align: center; padding: 0px 0 60px; font: 11px Arial, Helvetica, sans-serif; color: #999999;">
  <img src="images/payicons.gif" /><br />Site Developed by 
  <a href="http://www.cronsystems.net" target="_blank" 	style="color: #FF9900;">Cron Systems Corp.</a>
</div>                                                                                                                     
<? $chat = new chat();
// $chat->main();
  if($cms->loggedUserId!="")
{ ?>         
<script src="js/jquery.color.js" type='text/javascript'>
        </script> 		
<script src="js/colorpicker.js" type='text/javascript'>
        </script>         
<script src="js/colorpicker.js" type='text/javascript'>
        </script>         
<?php  include($_SERVER['DOCUMENT_ROOT'].'/js/chat_main_js.php');  ?>         
<script src="/js/soundmanager2-nodebug-jsmin.js" type='text/javascript'>
        </script>         
<script src="/js/jquery.countdown.js" type='text/javascript'>
        </script>         
<script src="/js/jquery.alert.js" type='text/javascript'>
        </script> 
<div id='chat-box' 	class="chat-chatbox">
</div>
<div class="chat-hellodiv" id='hellodiv'>
  <div class="chat-close" align="right">
    <a href="#" id="closeme" style="font-size: 10px; padding: 3px;">X</a>
  </div>
  <div>
    <iframe class="chat-iframe" id="onlineusers" frameborder="0" allowTransparency="true">
    </iframe>
  </div>
</div>
<!-- Added by Mike Casto on 12/7/09 for vertical display of items -->
<div class="chat-mc_items" id='mc_items'>
  <div class="chat-close" align="right">
    <a href="#" id="mc_closeme" 	style="font-size: 10px; padding: 3px;">X</a>
  </div>
  <div>
    <iframe class="chat-iframe" id="mc_listitems" frameborder="0" allowTransparency="true">
    </iframe>
  </div>
</div>
<!-- **************************** -->
<div id='chat_tool_bar'>            
  <ul id='toolbar_nav'>                
    <li id='toolbar_selling_products' class='sell_popup'>Selling Products</li>             
    <!-- <li id='toolbar_buying_products' class='buy_popup'>Buying Products</li> -->                
    <li id='toolbar_live_chat' class='live_chat_popup'>
      <div class="fade_me">
        <span class='live_chat_text'>Live Chat
        </span>
      </div>
    </li>                
    <li id='toolbar_contacts' class='contacts_popup'>Contacts
    </li>            
  </ul>            
  <div id='sell_popup'>				
    <h2 class='buy_sell_title'>Products I'm Selling</h2>
<?
      include($_SERVER['DOCUMENT_ROOT'].'/admin/mc_db/db_config.inc.php');
			$db_conn = mysql_connect(DB_SERVER, DB_USER, DB_PASS);
      $db_result = mysql_selectdb(DEFAULT_DB,$db_conn);
  		$p1query="SELECT * FROM products WHERE user_id='".$_SESSION['m_info']['user_id']."' AND status='active' AND auction_duration_end > '".time()."'";
      $r1=mysql_query($p1query);
if (mysql_num_rows($r1)==0)
  {
  echo 'No products listed for sale.';
  }
    ?>
    <ul>	
      <li>
      <h2 class='buy_sell_title'>Products I'm Selling</h2>
      </li>	
<?
      /* John Harre not used 10-4-2010
        if(mysql_num_rows($r1)>=1)
        {
        while ($results=mysql_fetch_assoc($r1))
        {
			echo '<li>';
			echo '<a href="selling.php">123'.$results['title'].'</a>';
			echo '</li>';
        }
      } */
      	?>
    </ul>            
  </div>            
  <div id='live_chat_popup'>                
    <div id='live_chat_container'>                    
      <div id='tool_bar_tabs'>                        
        <ul>                        
        </ul>                    
      </div>                    
      <div id='opened_chats'>                        
        <!-- REPLACE 42 WITH SESSION OF USER -->
<?
//<iframe src ="/chat/index.php" width="600" height="300" style="overflow:hidden;" frameborder="0">You do not have iframes.</iframe>
        ?>                        
        <!-- Open Chat Area -->                    
      </div>                
    </div>            
  </div>            
  <!-- REPLACE 42 WITH SESSION OF USER -->            
  <div id='contacts_popup'>            
  </div>     
</div>
<div class="chat-container2" 	id="container">
</div>
<? } ?>
<? $chat->main(); ?>
<script type='text/javascript'>
function trim(str, chars) {
	return ltrim(rtrim(str, chars), chars);
}
function ltrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}
 
function rtrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}
//Added by Mike Casto on 12/7/09 for vertical display of buying/selling items in taskbar
	function mc_show_items(el) {
		var sdiv = $("div.sc_menu"),
			bdiv = $("span.chat-waitmsg div div");
		if(el == "selling"){
			var left = sdiv.offset().left;
			var top = sdiv.offset().top;	
		} else {
			var left = bdiv.offset().left + 110;
			var top = bdiv.offset().top;	
		}
		var xpos = left-10;
		var ypos = top;
		$(".chat-mc_items").css({ 
				left: xpos + "px", 
				bottom: "34px", 
				position: "fixed", 
				display: "block"
			});
		var ouw = $("#mc_listitems").width() * 2;
		$("#mc_listitems").css({ width: ouw + "px" });
		document.getElementById("mc_listitems").src = "contacts_chat.php";
	}
//*********************
function findPos(obj) {
	var curleft = curtop = 0;
	if (obj.offsetParent) {
		curleft = obj.offsetLeft
		curtop = obj.offsetTop
		while (obj = obj.offsetParent) {
			curleft += obj.offsetLeft
			curtop += obj.offsetTop
		}
	}
	return [curleft,curtop];
}
function myshow(id) {
alert(id);
}
function show_users(id) {
var mywork = findPos(document.getElementById('pro_'+id));
var xpos = mywork[0];
var ypos = mywork[1];
//document.getElementById("hellodiv").style.display="none"
var mywork1 = findPos(document.getElementById('waitmsg'));
//alert(mywork1[0]+"--"+xpos);
if(xpos>mywork1[0])
{
xpos =  mywork1[0]-50;
}
//alert(xpos);
xpos = parseInt(xpos);
document.getElementById('hellodiv').style.left= xpos+'px'
document.getElementById('hellodiv').style.bottom = '34px';
document.getElementById('hellodiv').style.position='fixed';
document.getElementById('hellodiv').style.overflow='auto';
document.getElementById("onlineusers").src="bidder.php?id="+id
document.getElementById("hellodiv").style.display="block"
//var myworkx = findPos(document.getElementById('hellodiv'));
//alert(myworkx[0]);
generatediv(0, 1, id, 1);
//alert(xpos);
}
function showcontacts() {
  var mywork = findPos(document.getElementById('contacts_list'));
  var xpos = mywork[0]-10;
  var ypos = mywork[1];
  document.getElementById('hellodiv').style.left= xpos+'px'
  document.getElementById('hellodiv').style.bottom = '34px';
  document.getElementById('hellodiv').style.position='fixed';
  document.getElementById('hellodiv').style.overflow='auto';
  document.getElementById("onlineusers").src="contacts_chat.php";
  document.getElementById("hellodiv").style.display="block"
  }
</script>
<script>
var myidgloab;
var xpos1 = 0;
var ypos1 = 0;
var screenwidth = 0;
function generatediv(myid, type, proid, mytype, stat) {
//alert(mytype+' hello ');
if( mytype == undefined)
{
mytype =0;
}
if( stat== undefined)
{
stat=0;
}
screenwidth = screen.width;
//var screenwidth = document.body.clientWidth
//alert(mytype+' hello ');
myidgloab = myid; 
xmlHttp=GetXmlHttpObject()
if (xmlHttp==null)
 {
 alert ("Browser does not support HTTP Request")
 return
 }
 if(document.getElementById('hellodiv')!=null)
 {
 if (type==1)
 {
 var mywork = findPos(document.getElementById('hellodiv'));
 xpos1 = mywork[0];
 ypos1 = mywork[1];
 //alert(xpos1);
 if(xpos1==0){ 
 var myworkdisposition = findPos(document.getElementById('waitmsg'));
 xpos1=myworkdisposition[0]-42; 
 //xpos1= screenwidth/2; 
 }
 xpos1 = xpos1 + 140;
 }
 if (type==2)
 {
 var mywork = findPos(document.getElementById('pro_'+proid));
 var secpos = findPos(document.getElementById('contacts_list'));
 var x1 = secpos[0]; 
 xpos1 = mywork[0];
 ypos1 = mywork[1]; 
 if( x1 < xpos1+375 )
 {
 xpos1 = x1 - 379;
 } 
 //alert(xpos1+""+x1);
 }
 if(type==0)
 {
 xpos1 = screenwidth/3;
 }
 if(type==3)
 {
 xpos1 = 435;
 }
 }
var url="chatbox.php"
url=url+"?id="+myid
url = url + "&pro_id="+proid
url = url + "&extratype="+mytype
url = url + "&stat="+stat
url=url+"&sid="+Math.random()
//alert(url);
xmlHttp.onreadystatechange=stateChanged3 
xmlHttp.open("GET",url,true)
xmlHttp.send(null)
}
function stateChanged3() { 
if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
 { 
 document.getElementById("chat-box").innerHTML=""
 document.getElementById('chat-box').style.display='block';
 document.getElementById('chat-box').style.position='fixed'; 
  var sc = screenwidth - 310;
 if( xpos1 < sc )
 {
 document.getElementById('chat-box').style.left=xpos1+'px';
 }
 else
 {
 //var mpos = xpos1-200;
 var xxx = xpos1-600;
 var mpos = xpos1-xxx;
 document.getElementById('chat-box').style.left = mpos+'px' ;
 }
 //alert(xpos1);
 document.getElementById("chat-box").innerHTML=xmlHttp.responseText 
 //aftershw(myidgloab);
 } 
}
function close() {
xmlHttp=GetXmlHttpObject()
if (xmlHttp==null)
 {
 alert ("Browser does not support HTTP Request")
 return
 }
document.getElementById('chat-box').style.display='none';
document.getElementById('hellodiv').style.display='none';
showminimizediv(); 
var url="close.php";
xmlHttp.open("GET",url,true)
xmlHttp.send(null)
}
var xmlHttplogin;
function chklogin() {
xmlHttplogin=GetXmlHttpObject()
if (xmlHttp==null)
 {
 alert ("Browser does not support HTTP Request")
 return
 }
var url="chkonline.php";
xmlHttplogin.onreadystatechange=stateonline
xmlHttplogin.open("GET",url,true)
xmlHttplogin.send(null) 
}
function stateonline() {
if (xmlHttplogin.readyState==4 || xmlHttplogin.readyState=="complete")
 {
  var onlinelist = xmlHttplogin.responseText;
 // alert(online);
  var mysplit = onlinelist.split("/");
  var offline = mysplit[0];
  var online = mysplit[1];
  var offlinesplit = offline.split(",");
  var onlinelinesplit = online.split(",");
  var i;
  for(i = 1; i <= offlinesplit.length; i++)
  {
  var id = parseInt(offlinesplit[i]);
  //alert(id);
  if(document.getElementById('pro_'+id)!=null)
	{
	document.getElementById('pro_'+id).style.color= "red";
  	}
  }
  //alert(onlinelinesplit.length);
  for(i = 1; i <= onlinelinesplit.length; i++)
  {
  var id = parseInt(onlinelinesplit[i]);
  //alert(id);
 if(document.getElementById('pro_'+id)!=null)
	{
	document.getElementById('pro_'+id).style.color= "#33FF00";	
  	}
  }
 } 
 }
function chkping() {
xmlHttp=GetXmlHttpObject()
if (xmlHttp==null)
 {
 alert ("Browser does not support HTTP Request")
 return
 }
var url="chkping.php";
xmlHttp.onreadystatechange=stateChangedping
xmlHttp.open("GET",url,true)
xmlHttp.send(null) 
}
function stateChangedping() {
if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
 { 
var myreturnping = xmlHttp.responseText;
//alert(myreturnping);
var mysplit = myreturnping.split("/");
 var users = mysplit[0];
 var pinguser = mysplit[1];
 var buysell = mysplit[2];
 var contact_list_chat=mysplit[3];
  //alert (pinguser);
  //alert (buysell);
if((buysell!="") && (typeof(buysell)!="undefined")) {
  var splitbuysell = buysell.split(",");
  //id2 = parseInt(splitbuysell[1]);
  //id3 = parseInt(splitbuysell[2]);
  id2 = splitbuysell[1];
  var id2_arr=id2.split("-");
  id3 = splitbuysell[2];
  var id3_arr=id3.split("-");
  //alert(id3_arr +" "+id2_arr );
  /* Commented out by Mike Casto - *_pro_XXX is no longer being used
  document.getElementById('buy_pro_XXX').innerHTML="";
  document.getElementById('sell_pro_XXX').style.backgroundImage = "";
  document.getElementById('buy_pro_XXX').style.backgroundImage = "";
  document.getElementById('sell_pro_XXX').innerHTML="";
  */
  //alert("out---"+id2_arr[0]);
  if(id2_arr[0]==1)
  {
  //alert(id2_arr[id2_arr.length-1]);
  if(parseInt(id2_arr[id2_arr.length-1])!=0){
  /* Commented out by Mike Casto - buy_pro_XXX is no longer being used
  document.getElementById('buy_pro_XXX').innerHTML="&nbsp;"+id2_arr[id2_arr.length-1];
  document.getElementById('buy_pro_XXX').style.backgroundImage = "url(note.gif)";
  document.getElementById('buy_pro_XXX').style.backgroundRepeat = "no-repeat";
  document.getElementById('buy_pro_XXX').style.backgroundPosition = "90px 0"
  */
  }
  }
  if(id3_arr[0]==1)
  {
  }
}  
if(users!="" ||  users!=undefined) { 
var splitusers = users.split(",");
	for(i = 1; i < splitusers.length; i++){
	//alert('knock_'+ splitusers[i]);
	var id = trim(splitusers[i], ' ');
	id = parseInt(id);
	if(document.getElementById('knock_'+id)!=null)
	{
	document.getElementById('knock_'+id).innerHTML=" ";
	document.getElementById('knock_'+id).innerHTML=" ";
	document.getElementById('knock_'+id).style.backgroundImage = "";
	//document.getElementById('knock_'+id).style.display = "none";
	}
	}
}
if(pinguser!=undefined || pinguser!="")
{
var splitpinguser = pinguser.split(",");
//alert(id2_arr);
for(i = 1; i < id2_arr.length; i+=2)
{
//var id = trim(id2_arr[i+1], ' ');
var id = id2_arr[i+1];
//alert('knock_'+splitpinguser[i]);
id = parseInt(id);
//if (id!=0){
			if(document.getElementById('knock_'+id)!=null)
			{
			document.getElementById('knock_'+id).innerHTML= "&nbsp;"+id2_arr[i];
			document.getElementById('knock_'+id).style.backgroundImage = "url(images/note.gif)";
			document.getElementById('knock_'+id).style.backgroundRepeat = "no-repeat";
			document.getElementById('knock_'+id).style.backgroundPosition = "right"
			//document.getElementById('knock_'+id).style.display = "block";
			}
			if(document.getElementById('minknock_'+id)!=null)
			{
			//alert('knock_'+id);
			document.getElementById('minknock_'+id).style.color="blue";
			//document.getElementById('knock_'+id).style.display = "block";
			}
		  //}
}
//alert(id3_arr);
for(i = 1; i < id3_arr.length; i+=2)
{
//var id = trim(id3_arr[i+1], ' ');
var id = id3_arr[i+1];
//alert('knock_'+splitpinguser[i]);
//alert(id);
id = parseInt(id);
//if (id!=0){
			if(document.getElementById('knock_'+id)!=null)
			{
			//alert('knock_'+id);
			document.getElementById('knock_'+id).innerHTML= "&nbsp;"+id3_arr[i];
			document.getElementById('knock_'+id).style.backgroundImage = "url(images/note.gif)";
			document.getElementById('knock_'+id).style.backgroundRepeat = "no-repeat";
			//document.getElementById('knock_'+id).style.display = "block";
			}
			if(document.getElementById('minknock_'+id)!=null)
			{
			//alert('knock_'+id);
			document.getElementById('minknock_'+id).style.color="blue";
			//document.getElementById('knock_'+id).style.display = "block";
			}
		  //}
}
if(contact_list_chat>0)
{
if(document.getElementById('knock_-1')!=null)
			{
			//alert('knock_'+id);
			document.getElementById('knock_-1').innerHTML= contact_list_chat;
			document.getElementById('knock_-1').style.backgroundImage = "url(images/note.gif)";
			document.getElementById('knock_-1').style.backgroundRepeat = "no-repeat";
			document.getElementById('knock_-1').style.backgroundPosition = "62px 0"
			//alert(document.getElementById('knock_-1').innerHTML)
			//document.getElementById('knock_'+id).style.display = "block";
			}
			if(document.getElementById('minknock_-1')!=null)
			{
			//alert('knock_'+id);
			document.getElementById('minknock_-1').style.color="blue";
			//document.getElementById('knock_'+id).style.display = "block";
			}
}
}
}
}
</script>
<script>
var xmlHttp_changebid;
function changestartbid() {
xmlHttp_changebid=GetXmlHttpObject()
if (xmlHttp_changebid==null) 
 {
 alert ("Browser does not support HTTP Request")
 return
 }
 var url="changebid.php?action=change_ajax"
url=url+"&pricetochange="+document.getElementById("bidval").value
url = url + "&id="+document.getElementById("mypro").value
url=url+"&sid="+Math.random()
xmlHttp_changebid.onreadystatechange=stateChangedstart
xmlHttp_changebid.open("GET",url,true)
xmlHttp_changebid.send(null) 
}
var xmlHttpins;
function instantbid() {
  //alert ()
  xmlHttpins=GetXmlHttpObject();
  if (xmlHttpins==null) {
   alert ("Browser does not support HTTP Request");
   return
   }
//  var i_val=document.getElementById("bidval").value;
  var i_val=document.getElementById("val").value;
  alert(i_val);
  //alert(document.getElementById("startprice").value + " - " + document.getElementById("bidval").value);
  //return false
  var url="ajaxbid.php";
  url=url+"?val="+i_val;
  url=url+"&pro="+document.getElementById("mypro").value;
  url=url+"&sid="+Math.random();
  xmlHttpins.onreadystatechange=stateChangedbid;
  xmlHttpins.open("GET",url,true);
  xmlHttpins.send(null);
}
function stateChangedbid() { 
if (xmlHttpins.readyState==4 || xmlHttpins.readyState=="complete")
 { 
 document.getElementById("bidval").value=""
 document.getElementById("msgmebid").innerHTML=""
 document.getElementById("msgmebid").innerHTML=xmlHttpins.responseText 
 } 
}
function stateChangedstart() {
if (xmlHttp_changebid.readyState==4 || xmlHttp_changebid.readyState=="complete")
 { 
 document.getElementById("bidval").value=""
 document.getElementById("msgmebid").innerHTML=""
 document.getElementById("msgmebid").innerHTML=xmlHttp_changebid.responseText 
 } 
}
var xmlHttp_send;
function sendofmsg() {
xmlHttp_send=GetXmlHttpObject()
if (xmlHttp_send==null)
 {
 alert ("Browser does not support HTTP Request")
 return
 }
var id_to= document.getElementById("send_user_id").value
var offmsg = document.getElementById("msgtext").value
var subject = document.getElementById("subject").value
subject = trim(subject, ' ');
offmsg = trim(offmsg, ' ');
//alert(subject);
if(subject=='')
{
document.getElementById("return_msg").style.display="block";
document.getElementById("return_msg").innerHTML = " &nbsp; Please Enter Subject."
return false
}
if(offmsg=='')
{
document.getElementById("return_msg").style.display="block";
document.getElementById("return_msg").innerHTML = " &nbsp; Please Enter Message Body. "
return false
}
var url="send_msg_offline.php"
url=url+"?id="+id_to
url = url + "&msg="+offmsg
url = url + "&sub="+subject
url=url+"&sid="+Math.random()
document.getElementById("return_msg").style.display="block";
document.getElementById("return_msg").innerHTML =" &nbsp; Process Running...";
xmlHttp_send.onreadystatechange=stateChangeoffmsg
xmlHttp_send.open("GET",url,true)
xmlHttp_send.send(null)
}
function stateChangeoffmsg() {
if (xmlHttp_send.readyState==4 || xmlHttp_send.readyState=="complete")
 { 
 //alert( xmlHttp_send.responseText );
 document.getElementById("msgtext").value="";
 document.getElementById("subject").value="";
 document.getElementById("return_msg").style.display="block";
 document.getElementById("return_msg").innerHTML = " &nbsp; Your Message Has Been Sent"
 }
}
</script>
<script>
//$(function(){});
function chatnow(item_pro, log_usr, ownerusr) {
if(log_usr != ownerusr)
{
window.location="adtomaychatlist.php?user="+ownerusr+"&proid="+item_pro;
}
}
var xmlHttp_lastbid;
function lastbid() {
xmlHttp_lastbid=GetXmlHttpObject()
if (xmlHttp_lastbid==null)
 {
 alert ("Browser does not support HTTP Request")
 return
 }
 var url="lastbid.php"
/*url=url+"&pricetochange="+document.getElementById("bidval").value
url = url + "&id="+document.getElementById("mypro").value
url=url+"&sid="+Math.random()*/
xmlHttp_lastbid.onreadystatechange=lastbidchangestate
xmlHttp_lastbid.open("GET",url,true)
xmlHttp_lastbid.send(null) 
}
function lastbidchangestate() {
if(document.getElementById("show-last-bid")!= null)
{
//document.getElementById("show-last-bid").innerHTML="";
document.getElementById("show-last-bid").innerHTML=xmlHttp_lastbid.responseText ;
}
}
function removepro(pro) {
var answer = confirm("Are You Sure To Remove This Product From Your Task Bar?")
	if (answer){
		//window.location = "removepro.php?pro="+pro+"&url='"+url+"'";
		//window.location = "removepro.php";
		var frmname = 'from_pro_'+pro;		
		document.getElementById(frmname).submit();
		//document.frmname.submit();
	}
}
var xmlHttp_chkstat;
function chkstat() {
xmlHttp_chkstat=GetXmlHttpObject()
if (xmlHttp_chkstat==null)
 {
 alert ("Browser does not support HTTP Request")
 return
 }
var url="chkstat.php?idx="+Math.random(); 
xmlHttp_chkstat.open("GET",url,true)
xmlHttp_chkstat.send(null)  
}
function relaystat() {
chkstat();
//window.setTimeout("relaystat()", 4000);
}
var xmlHttp_chkme;
function chkme() {
xmlHttp_chkme=GetXmlHttpObject()
if (xmlHttp_chkme==null)
 {
 alert ("Browser does not support HTTP Request")
 return
 }
var url="chkme.php"
xmlHttp_chkme.onreadystatechange=stateChangechkme
xmlHttp_chkme.open("GET",url,true)
xmlHttp_chkme.send(null)
}
function stateChangechkme() {
if (xmlHttp_chkme.readyState==4 || xmlHttp_chkme.readyState=="complete")
 { 
  var chkval =  xmlHttp_chkme.responseText ;
  chkval = parseInt(chkval); 
  if(chkval==1)
  {
  if(document.getElementById("changeme1")!=null)
  {
  document.getElementById("changeme1").style.display="block";  
  }
  if(document.getElementById("changeme")!=null)
  {
  document.getElementById("changeme").style.display="none";  
  }
  }
 }
}
function closemyelf() {
document.getElementById('hellodiv').style.display = "none";
}
var xmlHttp_color;
function setmecolor() {
xmlHttp_color=GetXmlHttpObject()
if (xmlHttp_color==null)
 {
 alert ("Browser does not support HTTP Request")
 return
 }
var url="color.php"
url = url + "?color="+document.ajax.colorset.value
url=url+"&sid="+Math.random()
xmlHttp_color.onreadystatechange=stateChangecolor
xmlHttp_color.open("GET",url,true)
xmlHttp_color.send(null)
//document.getElementById("color-container").style.backgroundcolor = "'#"+document.ajax.colorset.value+"'";
//document.getElementById("color-container").style.className = "green";
//alert("'#"+document.ajax.colorset.value+"'");
}
function stateChangecolor() {
if (xmlHttp_color.readyState==4 || xmlHttp_color.readyState=="complete")
 {
  document.getElementById("mydivtoset").innerHTML = "";
  document.getElementById("mydivtoset").innerHTML = xmlHttp_color.responseText; 
 }
}
var xmlHttp_sound;
function loadPlayerCode() {
xmlHttp_sound=GetXmlHttpObject()
if (xmlHttp_sound==null)
 {
 alert ("Browser does not support HTTP Request")
 return
 }
var url="play1.php?id="+ Math.random()
//url = url + "?color="+document.ajax.colorset.value
//url=url+"&sid="+Math.random()
xmlHttp_sound.onreadystatechange=stateChangesound
xmlHttp_sound.open("GET",url,true)
xmlHttp_sound.send(null)
//document.getElementById("color-container").style.backgroundcolor = "'#"+document.ajax.colorset.value+"'";
//document.getElementById("color-container").style.className = "green";
//alert("'#"+document.ajax.colorset.value+"'");
}
function stateChangesound() {
if (xmlHttp_sound.readyState==4 || xmlHttp_sound.readyState=="complete")
 {
  //document.getElementById("mydivtoset").innerHTML = "";
  var idetifier = xmlHttp_sound.responseText;
 // alert(idetifier);
  if(idetifier == 1)
  { 
  soundManager.play('smash0');
  }
 }
}
</script>
<div id="flashcontent">
</div>
</body>
</html>